﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AutoMartSystem
{
    public partial class CarSalesRecord : Form
    {

        MySqlDataAdapter sqldata;
        DataTable carTable;
        string maxeno;
        public CarSalesRecord()
        {
            InitializeComponent();

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM carrecorddetails", MyConn);
                // sqldata = new MySqlDataAdapter("SELECT staffID,staffName,staffIC,,staffAge,staffGender,staffEmailAdd,datejoined,salary,password,roles" +
                //   " FROM staffdetails INNER JOIN logindetails ON loginID = staffIC", MyConn);
                carTable = new DataTable();

                //DataViewSettingCollection
                sqldata.Fill(carTable);



                carRecord.DataSource = carTable;
                // close connection
                MyConn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void rtn_main_Click(object sender, EventArgs e)
        {
            //on click to return to manager
            this.Close();
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //open report and also print

        }
    }
}
