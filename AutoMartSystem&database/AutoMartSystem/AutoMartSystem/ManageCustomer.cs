﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class ManageCustomer : Form
    {
        //code reuse
        private string EncryptString(string text)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            string encryptedPassword = Convert.ToBase64String(b);
            return encryptedPassword;
            throw new NotImplementedException();
        }

        //code reuse
        private string DecryptString(string encrString)
        {
            byte[] b = Convert.FromBase64String(encrString);
            string decryptedPassword = System.Text.ASCIIEncoding.ASCII.GetString(b);

            return decryptedPassword;
        }

        public ManageCustomer(string custid)
        {
            InitializeComponent();
            addPassword.UseSystemPasswordChar = true;
            this.addId.Text = custid;
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT * FROM customerdetails INNER JOIN logindetails ON custIC = loginID";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while (MyReader.Read())
                {
                    if (this.addId.Text == MyReader.GetString("custID"))
                    {
                        this.addIc.Text = MyReader.GetString("custIC");
                        this.addName.Text = MyReader.GetString("custName");
                        this.addAddress.Text = MyReader.GetString("custAdd");
                        this.addPhone.Text = MyReader.GetString("custPhone").Remove(0,2);
                        this.addGender.Text = MyReader.GetString("custGender");
                        this.addAge.Text = MyReader.GetString("custAge");
                        this.addPassword.Text = DecryptString(MyReader.GetString("password"));
                    }

                }
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void addCustomerBtn_Click(object sender, EventArgs e)
        {
        
            bool check = true,validateic = false;
            string checkic = "";
            Regex email1 = new Regex("[a-z0-9]{2,}.[a-z0-9]{2,}@hotmail.com");
            Regex email2 = new Regex("[a-z0-9]{2,}.[a-z0-9]{2,}@gmail.com");
            //to connect to phpmyadmin database name cardealership
            string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


            //select from the table of staff details
            MySqlConnection MyConn = new MySqlConnection(Conn);

            string Query = "SELECT * FROM customerdetails";

            MyConn = new MySqlConnection(Conn);
            MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
            MySqlDataReader MyReader;
            MyConn.Open();
            MyReader = MyComd.ExecuteReader();

            while (MyReader.Read())
            {
                if (checkic == MyReader.GetString("custIC"))
                {
                    validateic = true;
                    MessageBox.Show("The IC has been registered.", "Please try again.");
                }
            }

            MyConn.Close();

            //validate phone length
            if (addPhone.TextLength > 10)
            {
                MessageBox.Show("Exceeded the numbers in a phone", "Error Message");
                addPhone.Text = "";
                check = false;
            }

         
          

            if (check == false || validateic == true)
            {
                MessageBox.Show("No record to add", "Please try again");
            }
            else
            {
                try
                {

                
                    //insert from the table of car details
                    Query = "UPDATE customerdetails set custIC='" + this.addIc.Text + "',custName='" + this.addName.Text + "',custAdd='" + this.addAddress.Text + "',custPhone='" +
                        "60" + this.addPhone.Text + "',custGender='" + this.addGender.Text + "',custAge='" + int.Parse(this.addAge.Text) + "' where custID='" + this.addId.Text + "';";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                   
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MyConn.Close();

                    //create login account for customer
                    Query = "UPDATE logindetails  set password='" + EncryptString(this.addPassword.Text) + "',loginID='" + this.addIc.Text + "' where loginID='" + this.addIc.Text + "';";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MessageBox.Show("Updated Saved", "Updated Record");
                    MyConn.Close();


                    this.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                
            }

          

        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            addId.Text = "";
            addIc.Text = "";
            addGender.Text = "-Select Gender-";
            addName.Text = "";
            addPhone.Text = "";
            addAddress.Text = "";
            addAge.Text = "";
            addPassword.Text = "";
            passwordBtn.Checked = false;
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        //validation

        private void passwordBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (passwordBtn.Checked)
            {
                addPassword.UseSystemPasswordChar = false;

            }
            else
            {
                addPassword.UseSystemPasswordChar = true;

            }
        }

        private void addIc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addAge_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
