﻿using AutoMartSystem.Observer;
using AutoMartSystem.Subject;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class ManageEmployee : Form
    {
        ///code reuse
        private string EncryptString(string text)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            string encryptedPassword = Convert.ToBase64String(b);
            return encryptedPassword;
            throw new NotImplementedException();
        }

        //code reuse
        private string DecryptString(string encrString)
        {
            byte[] b = Convert.FromBase64String(encrString);
            string decryptedPassword = System.Text.ASCIIEncoding.ASCII.GetString(b);

            return decryptedPassword;
        }

        public ManageEmployee(string employeeid)
        {
            InitializeComponent();
            this.eno.Text = employeeid;

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT * FROM staffdetails INNER JOIN logindetails ON staffIC = loginID";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while (MyReader.Read())
                {
                    if (this.eno.Text == MyReader.GetString("staffID"))
                    {
                        this.ename.Text = MyReader.GetString("staffName");
                        this.eic.Text = MyReader.GetString("staffIC");
                        this.ephone.Text = MyReader.GetString("staffPhone").Remove(0,2);
                        this.eage.Text = MyReader.GetString("staffAge");
                        this.egender.Text = MyReader.GetString("staffGender");
                        this.eemailadd.Text = MyReader.GetString("staffEmailAdd");
                        this.edatejoined.Text = MyReader.GetString("datejoined");
                        this.esalary.Text = MyReader.GetString("salary");
                        this.epassword.Text = DecryptString(MyReader.GetString("password"));
                        this.erole.Text = MyReader.GetString("roles");
                        this.staffStatus.Text = MyReader.GetString("staffStatus");
                    }

                }
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void update_Click(object sender, EventArgs e)
        {
            bool check = true,validateic = false;
            string checkic = "";
            Regex email1 = new Regex("[a-z0-9]{2,}.[a-z0-9]{2,}@hotmail.com");
            Regex email2 = new Regex("[a-z0-9]{2,}.[a-z0-9]{2,}@gmail.com");

            //to connect to phpmyadmin database name cardealership
            string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


            //select from the table of staff details
            MySqlConnection MyConn = new MySqlConnection(Conn);

            string Query = "SELECT * FROM staffdetails";

            MyConn = new MySqlConnection(Conn);
            MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
            MySqlDataReader MyReader;
            MyConn.Open();
            MyReader = MyComd.ExecuteReader();

            while (MyReader.Read())
            {
                if (checkic == MyReader.GetString("staffIC"))
                {
                    validateic = true;
                    MessageBox.Show("The IC has been registered.", "Please try again.");
                }
            }

            MyConn.Close();
            //validate phone  length
            if (ephone.TextLength > 10)
            {
                MessageBox.Show("Exceeded the numbers in a phone", "Error Message");
                ephone.Text = "";
                check = false;
            }
            //validate email
            if (!email1.IsMatch(eemailadd.Text) && !email2.IsMatch(eemailadd.Text))
            {
                MessageBox.Show("Email Address must be like this example@hotmail.com or example@gmail.com", "Error Message");
                check = false;
            }
            if (eno.Text == "" || check == false || validateic == true)
            {
                MessageBox.Show("No record to update", "Records");
            }
            else
            {
             
                try
                {

                   Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                    Query = "UPDATE staffdetails set staffName='" + this.ename.Text + "',staffIC='" + this.eic.Text +
                        "',staffPhone='" + "60" + this.ephone.Text + "',staffAge='" + this.eage.Text + "',staffGender='" + this.egender.Text + "',staffEmailAdd='" + this.eemailadd.Text +
                         "',datejoined='" + this.edatejoined.Text + "',salary='" + this.esalary.Text + "',staffStatus='" + this.staffStatus.Text + "' where staffID='" + this.eno.Text + "';";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MyConn.Close();


                    Query = "UPDATE logindetails set password='" + EncryptString(this.epassword.Text) + "',roles='" 
                        + this.erole.Text + "' where loginID ='" + this.eic.Text + "';";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MessageBox.Show("Data Updated", "Records");

                    MyConn.Close();//Connection closed here  

                    //Observer Design Pattern implementation
                    Employed employ = new Employed();


                    if (this.erole.Text == "Admin")
                    {
                        Observer.Staff admin = new ObserverAdmin();
                        employ.Add_Employee(admin);
                    }
                    else
                    {
                        Observer.Staff salesperson = new ObserverSalesperson();
                        employ.Add_Employee(salesperson);
                    }

                    employ.NotifyManager(); //notify the addition of employees
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

               
            }
        }

     

        private void clear_Click(object sender, EventArgs e)
        {
            //Just Refresh the Form
           
            ename.Text = "";
            eic.Text = "";
            ephone.Text = "";
            eage.Text = "";
            egender.Text = "-Select-";
            eemailadd.Text = "";
            edatejoined.Text = "";
            esalary.Text = "";
            epassword.Text = "";
            erole.Text = "-Select-";
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void eic_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void ephone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void eage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void esalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
