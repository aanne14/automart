﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AutoMartSystem
{
    public partial class EmployeeHistory : Form
    {


        //default constructor
        public EmployeeHistory()
        {
            MySqlDataAdapter sqldata;
            DataTable employeeTable;

            InitializeComponent();

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM commissiondetails", MyConn);
                employeeTable = new DataTable();

                //DataViewSettingCollection
                sqldata.Fill(employeeTable);



                employeeDetails.DataSource = employeeTable;

                // close connection
                MyConn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



            //finding the best employee


            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT Name, FinalSalary, TotalSales FROM commissiondetails WHERE FinalSalary=MAX", MyConn);
                //  employeeTable = new DataTable();

                //DataViewSettingCollection
                //   sqldata.Fill(employeeTable);



                //   employeeDetails.DataSource = employeeTable;

                // close connection
                MyConn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //close this form
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //lead to page to calculate commmission
            MessageBox.Show("Calculating Commision for This Week","Commission");

        }
    }
}
