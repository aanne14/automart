﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace AutoMartSystem
{
    public partial class EmployeeDetails : Form
    {


        MySqlDataAdapter sqldata;
        DataTable employeetable;

        public EmployeeDetails()
        {
            InitializeComponent();

         //   this.Controls.Add(this.reportViewer1);
        }

        private void EmployeeDetails_Load(object sender, EventArgs e)
        {

        }

        public EmployeeDetails (string name, string staffid, string salary, string commision, string carsSold, string addComments)
        {
            InitializeComponent();
            //name, id, status, date joined, salary, commision, total cars sold ,additional comments 
            eName.Text = name;
            eID.Text = staffid;
            eStatus.Text = "Active";
            eSalary.Text = salary;
            eCommision.Text = commision;
            eTotalCarsSold.Text = carsSold;
            eAddComment.Text = addComments;

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM staffdetails", MyConn);
                // sqldata = new MySqlDataAdapter("SELECT staffID,staffName,staffIC,,staffAge,staffGender,staffEmailAdd,datejoined,salary,password,roles" +
                //   " FROM staffdetails INNER JOIN logindetails ON loginID = staffIC", MyConn);
                employeetable = new DataTable();

                //DataViewSettingCollection
                sqldata.Fill(employeetable);

                

               // employeeTable.DataSource = employeetable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
