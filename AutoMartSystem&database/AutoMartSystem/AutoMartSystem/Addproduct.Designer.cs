﻿namespace AutoMartSystem
{
    partial class Addproduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add_productbtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pBrand = new System.Windows.Forms.TextBox();
            this.pid = new System.Windows.Forms.TextBox();
            this.pProductPic = new System.Windows.Forms.PictureBox();
            this.btnOpenPic = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pProductPic)).BeginInit();
            this.SuspendLayout();
            // 
            // Add_productbtn
            // 
            this.Add_productbtn.Location = new System.Drawing.Point(88, 435);
            this.Add_productbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_productbtn.Name = "Add_productbtn";
            this.Add_productbtn.Size = new System.Drawing.Size(172, 31);
            this.Add_productbtn.TabIndex = 0;
            this.Add_productbtn.Text = "Confirm";
            this.Add_productbtn.UseVisualStyleBackColor = true;
            this.Add_productbtn.Click += new System.EventHandler(this.Add_productbtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(316, 435);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(172, 31);
            this.clearbtn.TabIndex = 1;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(571, 435);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(172, 31);
            this.exitBtn.TabIndex = 2;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(214, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Product ID: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(214, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Brand Name: ";
            // 
            // pBrand
            // 
            this.pBrand.Location = new System.Drawing.Point(399, 47);
            this.pBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pBrand.Name = "pBrand";
            this.pBrand.Size = new System.Drawing.Size(196, 22);
            this.pBrand.TabIndex = 21;
            // 
            // pid
            // 
            this.pid.Enabled = false;
            this.pid.Location = new System.Drawing.Point(399, 11);
            this.pid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pid.Name = "pid";
            this.pid.Size = new System.Drawing.Size(118, 22);
            this.pid.TabIndex = 44;
            // 
            // pProductPic
            // 
            this.pProductPic.Location = new System.Drawing.Point(147, 96);
            this.pProductPic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pProductPic.Name = "pProductPic";
            this.pProductPic.Size = new System.Drawing.Size(527, 242);
            this.pProductPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pProductPic.TabIndex = 53;
            this.pProductPic.TabStop = false;
            // 
            // btnOpenPic
            // 
            this.btnOpenPic.Location = new System.Drawing.Point(316, 354);
            this.btnOpenPic.Name = "btnOpenPic";
            this.btnOpenPic.Size = new System.Drawing.Size(145, 30);
            this.btnOpenPic.TabIndex = 56;
            this.btnOpenPic.Text = "Open Picture";
            this.btnOpenPic.UseVisualStyleBackColor = true;
            this.btnOpenPic.Click += new System.EventHandler(this.btnOpenPic_Click);
            // 
            // Addproduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 491);
            this.Controls.Add(this.btnOpenPic);
            this.Controls.Add(this.pProductPic);
            this.Controls.Add(this.pid);
            this.Controls.Add(this.pBrand);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.Add_productbtn);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Addproduct";
            this.Text = "Addproduct";
            ((System.ComponentModel.ISupportInitialize)(this.pProductPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Add_productbtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox pBrand;
        private System.Windows.Forms.TextBox pid;
        private System.Windows.Forms.PictureBox pProductPic;
        private System.Windows.Forms.Button btnOpenPic;
    }
}