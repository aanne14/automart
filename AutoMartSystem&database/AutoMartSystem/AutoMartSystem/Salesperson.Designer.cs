﻿namespace AutoMartSystem
{
    partial class Salesperson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Salesperson));
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.details = new System.Windows.Forms.RichTextBox();
            this.productTable = new System.Windows.Forms.DataGridView();
            this.carId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cColour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCarPlateNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cManufactureYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.branchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mEngineCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cMileage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPicture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pProductPic = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.TextBox();
            this.search = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.sID = new System.Windows.Forms.Label();
            this.sName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sEmail = new System.Windows.Forms.Label();
            this.sPhone = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pProductPic)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1189, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Salesperson";
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(1373, 94);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(107, 39);
            this.exitBtn.TabIndex = 1;
            this.exitBtn.Text = "Logout";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.refreshBtn);
            this.tabPage2.Controls.Add(this.details);
            this.tabPage2.Controls.Add(this.productTable);
            this.tabPage2.Controls.Add(this.pProductPic);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.searchButton);
            this.tabPage2.Controls.Add(this.search);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1430, 689);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Car  Details";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // details
            // 
            this.details.Location = new System.Drawing.Point(27, 73);
            this.details.Name = "details";
            this.details.Size = new System.Drawing.Size(774, 154);
            this.details.TabIndex = 14;
            this.details.Text = "";
            // 
            // productTable
            // 
            this.productTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.carId,
            this.pBrand,
            this.mName,
            this.cType,
            this.cColour,
            this.cCarPlateNo,
            this.cManufactureYear,
            this.branchName,
            this.mEngineCC,
            this.cMileage,
            this.quantity,
            this.cDescription,
            this.cPrice,
            this.cPicture,
            this.status});
            this.productTable.Location = new System.Drawing.Point(7, 280);
            this.productTable.Name = "productTable";
            this.productTable.RowHeadersWidth = 51;
            this.productTable.RowTemplate.Height = 24;
            this.productTable.Size = new System.Drawing.Size(1416, 343);
            this.productTable.TabIndex = 13;
            this.productTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.productTable_CellContentClick);
            // 
            // carId
            // 
            this.carId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.carId.DataPropertyName = "carId";
            this.carId.HeaderText = "Car ID";
            this.carId.MinimumWidth = 6;
            this.carId.Name = "carId";
            this.carId.Width = 80;
            // 
            // pBrand
            // 
            this.pBrand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.pBrand.DataPropertyName = "pBrand";
            this.pBrand.HeaderText = "Car Brand";
            this.pBrand.MinimumWidth = 6;
            this.pBrand.Name = "pBrand";
            this.pBrand.Width = 137;
            // 
            // mName
            // 
            this.mName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mName.DataPropertyName = "mName";
            this.mName.HeaderText = "Car Model";
            this.mName.MinimumWidth = 6;
            this.mName.Name = "mName";
            this.mName.Width = 141;
            // 
            // cType
            // 
            this.cType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cType.DataPropertyName = "cType";
            this.cType.HeaderText = "Car Type";
            this.cType.MinimumWidth = 6;
            this.cType.Name = "cType";
            this.cType.Width = 129;
            // 
            // cColour
            // 
            this.cColour.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cColour.DataPropertyName = "cColour";
            this.cColour.HeaderText = "Colour";
            this.cColour.MinimumWidth = 6;
            this.cColour.Name = "cColour";
            this.cColour.Width = 114;
            // 
            // cCarPlateNo
            // 
            this.cCarPlateNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cCarPlateNo.DataPropertyName = "cCarPlateNo";
            this.cCarPlateNo.HeaderText = "Car Plate No";
            this.cCarPlateNo.MinimumWidth = 6;
            this.cCarPlateNo.Name = "cCarPlateNo";
            this.cCarPlateNo.Width = 135;
            // 
            // cManufactureYear
            // 
            this.cManufactureYear.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cManufactureYear.DataPropertyName = "cManufactureYear";
            this.cManufactureYear.HeaderText = "Production Year";
            this.cManufactureYear.MinimumWidth = 6;
            this.cManufactureYear.Name = "cManufactureYear";
            this.cManufactureYear.Width = 196;
            // 
            // branchName
            // 
            this.branchName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.branchName.DataPropertyName = "branchName";
            this.branchName.HeaderText = "Branch Name";
            this.branchName.MinimumWidth = 6;
            this.branchName.Name = "branchName";
            this.branchName.Width = 172;
            // 
            // mEngineCC
            // 
            this.mEngineCC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mEngineCC.DataPropertyName = "mEngineCC";
            this.mEngineCC.HeaderText = "Engine CC";
            this.mEngineCC.MinimumWidth = 6;
            this.mEngineCC.Name = "mEngineCC";
            this.mEngineCC.Width = 145;
            // 
            // cMileage
            // 
            this.cMileage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cMileage.DataPropertyName = "cMileage";
            this.cMileage.HeaderText = "Mileage";
            this.cMileage.MinimumWidth = 6;
            this.cMileage.Name = "cMileage";
            this.cMileage.Width = 129;
            // 
            // quantity
            // 
            this.quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.quantity.DataPropertyName = "quantity";
            this.quantity.HeaderText = "Quantity";
            this.quantity.MinimumWidth = 6;
            this.quantity.Name = "quantity";
            this.quantity.Width = 129;
            // 
            // cDescription
            // 
            this.cDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cDescription.DataPropertyName = "cDescription";
            this.cDescription.HeaderText = "Description";
            this.cDescription.MinimumWidth = 6;
            this.cDescription.Name = "cDescription";
            this.cDescription.Width = 164;
            // 
            // cPrice
            // 
            this.cPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cPrice.DataPropertyName = "cPrice";
            this.cPrice.HeaderText = "Price";
            this.cPrice.MinimumWidth = 6;
            this.cPrice.Name = "cPrice";
            this.cPrice.Width = 98;
            // 
            // cPicture
            // 
            this.cPicture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cPicture.DataPropertyName = "cPicture";
            this.cPicture.HeaderText = "Picture URL";
            this.cPicture.MinimumWidth = 6;
            this.cPicture.Name = "cPicture";
            this.cPicture.Width = 155;
            // 
            // status
            // 
            this.status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "Status";
            this.status.MinimumWidth = 6;
            this.status.Name = "status";
            this.status.Width = 108;
            // 
            // pProductPic
            // 
            this.pProductPic.Location = new System.Drawing.Point(1052, 11);
            this.pProductPic.Name = "pProductPic";
            this.pProductPic.Size = new System.Drawing.Size(371, 263);
            this.pProductPic.TabIndex = 12;
            this.pProductPic.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 29);
            this.label7.TabIndex = 11;
            this.label7.Text = "Search:";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(123, 25);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(206, 34);
            this.searchButton.TabIndex = 10;
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(350, 11);
            this.search.Margin = new System.Windows.Forms.Padding(4);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(145, 48);
            this.search.TabIndex = 7;
            this.search.Text = "Search";
            this.search.UseVisualStyleBackColor = true;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 29);
            this.label6.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 38);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1430, 689);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Home";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(43, 279);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(205, 50);
            this.button1.TabIndex = 21;
            this.button1.Text = "View Salary";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.sID);
            this.groupBox1.Controls.Add(this.sName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.sEmail);
            this.groupBox1.Controls.Add(this.sPhone);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(43, 32);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(445, 214);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Salesperson Details:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 29);
            this.label2.TabIndex = 12;
            this.label2.Text = "Name:";
            // 
            // sID
            // 
            this.sID.AutoSize = true;
            this.sID.Location = new System.Drawing.Point(137, 130);
            this.sID.Name = "sID";
            this.sID.Size = new System.Drawing.Size(79, 29);
            this.sID.TabIndex = 19;
            this.sID.Text = "label4";
            // 
            // sName
            // 
            this.sName.AutoSize = true;
            this.sName.Location = new System.Drawing.Point(137, 41);
            this.sName.Name = "sName";
            this.sName.Size = new System.Drawing.Size(79, 29);
            this.sName.TabIndex = 14;
            this.sName.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 29);
            this.label5.TabIndex = 18;
            this.label5.Text = "ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 29);
            this.label3.TabIndex = 13;
            this.label3.Text = "Phone:";
            // 
            // sEmail
            // 
            this.sEmail.AutoSize = true;
            this.sEmail.Location = new System.Drawing.Point(137, 101);
            this.sEmail.Name = "sEmail";
            this.sEmail.Size = new System.Drawing.Size(79, 29);
            this.sEmail.TabIndex = 17;
            this.sEmail.Text = "label4";
            // 
            // sPhone
            // 
            this.sPhone.AutoSize = true;
            this.sPhone.Location = new System.Drawing.Point(137, 71);
            this.sPhone.Name = "sPhone";
            this.sPhone.Size = new System.Drawing.Size(79, 29);
            this.sPhone.TabIndex = 15;
            this.sPhone.Text = "label4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 29);
            this.label4.TabIndex = 16;
            this.label4.Text = "Email:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(46, 139);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1438, 731);
            this.tabControl1.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(50, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(110, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(183, 50);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(182, 46);
            this.label18.TabIndex = 26;
            this.label18.Text = "AutoMart";
            // 
            // refreshBtn
            // 
            this.refreshBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshBtn.Location = new System.Drawing.Point(548, 11);
            this.refreshBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(167, 46);
            this.refreshBtn.TabIndex = 33;
            this.refreshBtn.Text = "Refresh";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // Salesperson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1525, 946);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Salesperson";
            this.Text = "Salesperson";
            this.Load += new System.EventHandler(this.Salesperson_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pProductPic)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label sID;
        private System.Windows.Forms.Label sName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label sEmail;
        private System.Windows.Forms.Label sPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.DataGridView productTable;
        private System.Windows.Forms.PictureBox pProductPic;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox searchButton;
        private System.Windows.Forms.RichTextBox details;
        private System.Windows.Forms.DataGridViewTextBoxColumn carId;
        private System.Windows.Forms.DataGridViewTextBoxColumn pBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn mName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cType;
        private System.Windows.Forms.DataGridViewTextBoxColumn cColour;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCarPlateNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cManufactureYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn branchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn mEngineCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMileage;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPicture;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button refreshBtn;
    }
}