﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem.Observer
{
    public class ObserverSalesperson: Staff
    {
        public void NotifyEmployeeChange()
        {
            MessageBox.Show("Salesperson List updated");
        }
    }
}
