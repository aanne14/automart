﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem.Observer
{
    public class ObserverAdmin: Staff
    {
        public void NotifyEmployeeChange()
        {
            MessageBox.Show("Admin List updated");
        }
    }
}
