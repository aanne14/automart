﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMartSystem.Observer
{
    public interface Staff
    {
        void NotifyEmployeeChange();  
    }
}
