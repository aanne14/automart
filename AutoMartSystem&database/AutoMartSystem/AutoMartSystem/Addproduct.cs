﻿using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class Addproduct : Form
    {
        Image File;
        string correctFilename;
        MySqlConnection cn = new MySqlConnection();
        MySqlCommand cmd = new MySqlCommand();
        MySqlDataReader dr;
        MySqlParameter picture;
        public Addproduct()
        {
            InitializeComponent();
            //auto-generate product id
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT MAX(pId) FROM producttable";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    
                    pid.Text =  "P" + (int.Parse(MyReader.GetString("MAX(pId)").Remove(0,1)) + 1).ToString();
                 }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

   

  
       
        //clear all add product fields
        private void clearbtn_Click(object sender, EventArgs e)
        {
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT MAX(pId) FROM producttable";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    pid.Text = "P" + (int.Parse(MyReader.GetString("MAX(pId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            pProductPic.Image = null;
            pBrand.Text = "";
        }

     
        ///insert product data into the cartable database table
        private void Add_productbtn_Click(object sender, EventArgs e)
        {
            try
            {
              
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";
               
               
             
                //insert from the table of car details
                string Query = "INSERT INTO producttable (pId,pBrand,pPicture) " +
                    "VALUES ('" + this.pid.Text + "','"  + this.pBrand.Text + "','" + correctFilename   +"');";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();
         
                MessageBox.Show("Record Saved", "Insert Record");
                MyConn.Close();

                Query = "SELECT MAX(pId) FROM producttable";

                MyConn = new MySqlConnection(Conn);
                MyComd = new MySqlCommand(Query, MyConn);
              
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    pid.Text = "P" + (int.Parse(MyReader.GetString("MAX(pId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
            pProductPic.Image = null;
            pBrand.Text = "";


        }

        //exit this page
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    

        //select photo from database base on pid(product id)
        private void selectPic_Click(object sender, EventArgs e)
        {
        
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog =cardealership;username=root;password=;Integrated Security=True");

            MySqlDataAdapter sda = new MySqlDataAdapter("SELECT pPicture FROM producttable WHERE pId='" + this.pid.Text + "'", connection);
            
            DataTable table = new DataTable();
            sda.Fill(table);
            string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
            pProductPic.Image = Image.FromFile(paths + "\\pictures\\" + table.Rows[0]["pPicture"].ToString());
        }

        //select picture from c:\\
        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    pProductPic.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }
    }
}
