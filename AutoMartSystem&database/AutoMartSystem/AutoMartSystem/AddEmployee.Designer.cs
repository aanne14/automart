﻿namespace AutoMartSystem
{
    partial class AddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addEid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.exitbtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.Add_productbtn = new System.Windows.Forms.Button();
            this.addName = new System.Windows.Forms.TextBox();
            this.addAge = new System.Windows.Forms.TextBox();
            this.addEmail = new System.Windows.Forms.TextBox();
            this.addIc = new System.Windows.Forms.TextBox();
            this.addPhone = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.addDateJoined = new System.Windows.Forms.DateTimePicker();
            this.addGender = new System.Windows.Forms.ComboBox();
            this.addPassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.addRole = new System.Windows.Forms.ComboBox();
            this.addSalary = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.passwordBtn = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // addEid
            // 
            this.addEid.Enabled = false;
            this.addEid.Location = new System.Drawing.Point(183, 23);
            this.addEid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addEid.Name = "addEid";
            this.addEid.Size = new System.Drawing.Size(157, 22);
            this.addEid.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 26;
            this.label1.Text = "Employee ID: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 20);
            this.label2.TabIndex = 29;
            this.label2.Text = "Name: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 20);
            this.label3.TabIndex = 30;
            this.label3.Text = "Phone: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 20);
            this.label4.TabIndex = 31;
            this.label4.Text = "IC No: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 20);
            this.label6.TabIndex = 33;
            this.label6.Text = "Email Address: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 34;
            this.label7.Text = "Age: ";
            // 
            // exitbtn
            // 
            this.exitbtn.Location = new System.Drawing.Point(436, 425);
            this.exitbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitbtn.Name = "exitbtn";
            this.exitbtn.Size = new System.Drawing.Size(145, 31);
            this.exitbtn.TabIndex = 37;
            this.exitbtn.Text = "Exit";
            this.exitbtn.UseVisualStyleBackColor = true;
            this.exitbtn.Click += new System.EventHandler(this.exitbtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(235, 425);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(145, 31);
            this.clearbtn.TabIndex = 36;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // Add_productbtn
            // 
            this.Add_productbtn.Location = new System.Drawing.Point(23, 425);
            this.Add_productbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_productbtn.Name = "Add_productbtn";
            this.Add_productbtn.Size = new System.Drawing.Size(145, 31);
            this.Add_productbtn.TabIndex = 35;
            this.Add_productbtn.Text = "Confirm";
            this.Add_productbtn.UseVisualStyleBackColor = true;
            this.Add_productbtn.Click += new System.EventHandler(this.Add_productbtn_Click);
            // 
            // addName
            // 
            this.addName.Location = new System.Drawing.Point(183, 66);
            this.addName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addName.Name = "addName";
            this.addName.Size = new System.Drawing.Size(243, 22);
            this.addName.TabIndex = 38;
            // 
            // addAge
            // 
            this.addAge.Location = new System.Drawing.Point(184, 263);
            this.addAge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addAge.Name = "addAge";
            this.addAge.Size = new System.Drawing.Size(53, 22);
            this.addAge.TabIndex = 39;
            this.addAge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addAge_KeyPress);
            // 
            // addEmail
            // 
            this.addEmail.Location = new System.Drawing.Point(183, 226);
            this.addEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addEmail.Name = "addEmail";
            this.addEmail.Size = new System.Drawing.Size(196, 22);
            this.addEmail.TabIndex = 40;
            // 
            // addIc
            // 
            this.addIc.Location = new System.Drawing.Point(184, 144);
            this.addIc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addIc.Name = "addIc";
            this.addIc.Size = new System.Drawing.Size(196, 22);
            this.addIc.TabIndex = 42;
            this.addIc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addIc_KeyPress);
            // 
            // addPhone
            // 
            this.addPhone.Location = new System.Drawing.Point(184, 110);
            this.addPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addPhone.Name = "addPhone";
            this.addPhone.Size = new System.Drawing.Size(196, 22);
            this.addPhone.TabIndex = 43;
            this.addPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addPhone_KeyPress);
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(141, 110);
            this.textBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(37, 22);
            this.textBox8.TabIndex = 60;
            this.textBox8.Text = "+60";
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // addDateJoined
            // 
            this.addDateJoined.Location = new System.Drawing.Point(184, 356);
            this.addDateJoined.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addDateJoined.Name = "addDateJoined";
            this.addDateJoined.Size = new System.Drawing.Size(236, 22);
            this.addDateJoined.TabIndex = 59;
            // 
            // addGender
            // 
            this.addGender.FormattingEnabled = true;
            this.addGender.Items.AddRange(new object[] {
            "Male\t",
            "Female"});
            this.addGender.Location = new System.Drawing.Point(183, 297);
            this.addGender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addGender.Name = "addGender";
            this.addGender.Size = new System.Drawing.Size(121, 24);
            this.addGender.TabIndex = 58;
            this.addGender.Text = "-Select-";
            // 
            // addPassword
            // 
            this.addPassword.Location = new System.Drawing.Point(183, 326);
            this.addPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addPassword.Name = "addPassword";
            this.addPassword.Size = new System.Drawing.Size(243, 22);
            this.addPassword.TabIndex = 57;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 326);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 20);
            this.label14.TabIndex = 56;
            this.label14.Text = "Password:";
            // 
            // addRole
            // 
            this.addRole.FormattingEnabled = true;
            this.addRole.Items.AddRange(new object[] {
            "manager\t",
            "salesperson",
            "admin"});
            this.addRole.Location = new System.Drawing.Point(184, 185);
            this.addRole.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addRole.Name = "addRole";
            this.addRole.Size = new System.Drawing.Size(196, 24);
            this.addRole.TabIndex = 55;
            this.addRole.Text = "-Select-";
            // 
            // addSalary
            // 
            this.addSalary.Location = new System.Drawing.Point(184, 386);
            this.addSalary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addSalary.Name = "addSalary";
            this.addSalary.Size = new System.Drawing.Size(121, 22);
            this.addSalary.TabIndex = 54;
            this.addSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addSalary_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 386);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 20);
            this.label13.TabIndex = 49;
            this.label13.Text = "Salary:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(21, 356);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 20);
            this.label12.TabIndex = 48;
            this.label12.Text = "Date Joined:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 297);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 20);
            this.label10.TabIndex = 46;
            this.label10.Text = "Gender:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Roles: ";
            // 
            // passwordBtn
            // 
            this.passwordBtn.AutoSize = true;
            this.passwordBtn.Location = new System.Drawing.Point(449, 326);
            this.passwordBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.passwordBtn.Name = "passwordBtn";
            this.passwordBtn.Size = new System.Drawing.Size(129, 21);
            this.passwordBtn.TabIndex = 61;
            this.passwordBtn.Text = "Show Password";
            this.passwordBtn.UseVisualStyleBackColor = true;
            this.passwordBtn.CheckedChanged += new System.EventHandler(this.passwordBtn_CheckedChanged);
            // 
            // AddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 512);
            this.Controls.Add(this.passwordBtn);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.addDateJoined);
            this.Controls.Add(this.addGender);
            this.Controls.Add(this.addPassword);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.addRole);
            this.Controls.Add(this.addSalary);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.addPhone);
            this.Controls.Add(this.addIc);
            this.Controls.Add(this.addEmail);
            this.Controls.Add(this.addAge);
            this.Controls.Add(this.addName);
            this.Controls.Add(this.exitbtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.Add_productbtn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.addEid);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AddEmployee";
            this.Text = "AddEmployee";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox addEid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button exitbtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button Add_productbtn;
        private System.Windows.Forms.TextBox addName;
        private System.Windows.Forms.TextBox addAge;
        private System.Windows.Forms.TextBox addEmail;
        private System.Windows.Forms.TextBox addIc;
        private System.Windows.Forms.TextBox addPhone;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.DateTimePicker addDateJoined;
        private System.Windows.Forms.ComboBox addGender;
        private System.Windows.Forms.TextBox addPassword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox addRole;
        private System.Windows.Forms.TextBox addSalary;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox passwordBtn;
    }
}