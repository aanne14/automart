﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
namespace AutoMartSystem
{


    public partial class CarSalesReport : Form
    {

        MySqlDataAdapter sqldata;
        DataTable carTable;

        public CarSalesReport()
        {
            InitializeComponent();


            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM reportdetails", MyConn);
                // sqldata = new MySqlDataAdapter("SELECT staffID,staffName,staffIC,,staffAge,staffGender,staffEmailAdd,datejoined,salary,password,roles" +
                //   " FROM staffdetails INNER JOIN logindetails ON loginID = staffIC", MyConn);
                carTable = new DataTable();

                //DataViewSettingCollection
                sqldata.Fill(carTable);



                carReport.DataSource = carTable;
                // close connection
                MyConn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void rtn_manager_Click(object sender, EventArgs e)
        {
            //on click to return to manager
            this.Close();
           
        }

        private void button1_Click(object sender, EventArgs e)
        { 

        }
    }
}
