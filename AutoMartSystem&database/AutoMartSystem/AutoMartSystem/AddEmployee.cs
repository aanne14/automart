﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class AddEmployee : Form
    {

       
        MySqlDataAdapter sqldata;
        DataTable employeetable;
        public AddEmployee()
        {
            InitializeComponent();
            addPassword.UseSystemPasswordChar = true;
            //auto generate new staff id 
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
             
                string Query = "SELECT MAX(staffID) FROM staffdetails";

                MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    addEid.Text = "E" + (int.Parse(MyReader.GetString("MAX(staffID)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("No record found", "Please try again");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void Add_productbtn_Click(object sender, EventArgs e)
        {
            bool check = true,validateic=false,successful = false;
            
            //to connect to phpmyadmin database name cardealership
            string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


            //select from the table of staff details
            MySqlConnection MyConn = new MySqlConnection(Conn);

            string Query = "SELECT * FROM logindetails";

            MyConn = new MySqlConnection(Conn);
            MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
            MySqlDataReader MyReader;
            MyConn.Open();
            MyReader = MyComd.ExecuteReader();

            while (MyReader.Read())
            {
               if(addIc.Text == MyReader.GetString("loginID"))
                {
                    validateic = true;
                    MessageBox.Show("The IC has been registered.", "Please try again.");
                }
            }
          
            MyConn.Close();

            //validate phone length
            if (addPhone.TextLength > 10)
            {
                MessageBox.Show("Exceeded the numbers in a phone", "Error Message");
                addPhone.Text = "";
                check = false;
            }

            //email validation
            Regex email1 = new Regex("[a-z0-9]{2,}.[a-z0-9]{2,}@hotmail.com");
            Regex email2 = new Regex("[a-z0-9]{2,}.[a-z0-9]{2,}@gmail.com");

            //validate email
            if (!email1.IsMatch(addEmail.Text) && !email2.IsMatch(addEmail.Text))
            {
                MessageBox.Show("Email Address must be like this example@hotmail.com or example@gmail.com", "Error Message");
                check = false;
            }

            if (!email1.IsMatch(addEmail.Text) && !email2.IsMatch(addEmail.Text) || check == false || validateic == true)
            {
                MessageBox.Show("No record to add", "Please try again");
            }
            else
            {
                try
                {
                    //create login account for employee
                    Query = "INSERT INTO logindetails (loginID,password,roles) VALUES ('" + this.addIc.Text + "','" + EncryptString(addPassword.Text) + "','" + this.addRole.Text + "');";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    
                    MyConn.Close();

                    //insert from the table of staff details
                    Query = "INSERT INTO staffdetails (staffID,staffName,staffIC,staffPhone,staffAge,staffGender,staffEmailAdd,datejoined,salary,staffstatus) VALUES ('"
                        + this.addEid.Text + "','" + this.addName.Text + "','" + this.addIc.Text + "','" + "60" + this.addPhone.Text + "','" + int.Parse(this.addAge.Text) +
                        "','" + this.addGender.Text + "','" + this.addEmail.Text + "','" + this.addDateJoined.Text + "','" + int.Parse(this.addSalary.Text) + "','" + "Active" + "');";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MessageBox.Show("Record Saved", "Insert Record");
                    MyConn.Close();

                    ///refresh the new staffid after submitting 
                    Query = "SELECT MAX(staffID) FROM staffdetails";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {
                        addEid.Text = "E" + (int.Parse(MyReader.GetString("MAX(staffID)").Remove(0, 1)) + 1).ToString();
                    }
                    else
                    {
                        MessageBox.Show("No record found", "Please try again");
                    }
                    successful = true;

                    //Observer Pattern
                    Subject.Employed observerPat = new Subject.Employed();


                    if (addRole.SelectedIndex.Equals(1))
                    {
                        Observer.Staff User = new Observer.ObserverSalesperson();
                        observerPat.Add_Employee(User);
                        observerPat.NotifyManager();
                    }
                    else if (addRole.SelectedIndex.Equals(2))
                    {
                        Observer.Staff User = new Observer.ObserverAdmin();
                        observerPat.Add_Employee(User);
                        observerPat.NotifyManager();

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                if(successful == true)
                {
                    //refresh after insert into database
                  
                    addEmail.Text = "";
                    addAge.Text = "";
                    addDateJoined.Text = "";
                    addGender.Text = "-Select-";
                    addIc.Text = "";
                    addName.Text = "";
                    addPassword.Text = "";
                    addRole.Text = "-Select-";
                    addSalary.Text = "";
                    addPhone.Text = "";
                }
              
            }

           
        }

        private string EncryptString(string text)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            string encryptedPassword = Convert.ToBase64String(b);
            return encryptedPassword;
            throw new NotImplementedException();
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            //clear all text field
          
            addEmail.Text = "";
            addAge.Text = "";
            addDateJoined.Text = "";
            addGender.Text = "-Select-";
            addIc.Text = "";
            addName.Text = "";
            addPassword.Text = "";
            addRole.Text = "-Select-";
            addSalary.Text = "";
            addPhone.Text = "";
        }

        //exit this page
        private void exitbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void passwordBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (passwordBtn.Checked)
            {
                addPassword.UseSystemPasswordChar = false;

            }
            else
            {
                addPassword.UseSystemPasswordChar = true;

            }
        }

        private void addPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addIc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addAge_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
