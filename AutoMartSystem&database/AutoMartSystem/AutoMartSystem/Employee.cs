﻿//Singleton Pattern class
using System;
using System.Collections.Generic;

using System.Data;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace AutoMartSystem
{
    public class Employee
    {
       
        private static Employee logger;
        private List<Employee> employeeList = null;
        ///thread safe environment
        private static readonly object mylocklogin = new object();

        //private constructor
        private Employee(){}

        public static Employee GetInstance()
        {
            lock(mylocklogin)
            {
                if (null == logger)
                {
                    logger = new Employee();
                }
            }
            return logger;
        }

        public string getLoginResult(string userId, string password)
        {
            string checkpassword,roles,checkuserid;     
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(password);
            string encryptedPassword = Convert.ToBase64String(b);
          
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                //select from the table og login details
                string Query = "SELECT * FROM logindetails WHERE loginID =" + userId;

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();
                
                if(MyReader.Read())
                {
                    checkuserid = MyReader.GetString("loginID");
                    checkpassword = MyReader.GetString("password");
                    roles = MyReader.GetString("roles");

                    if (userId.CompareTo(checkuserid) == 0 && encryptedPassword.CompareTo(checkpassword) == 0)
                    {
                        return roles;
                    }
                    else
                    {
                        MessageBox.Show("Wrong password / user id", "Please try again");
                    }
                }
                else
                {
                    MessageBox.Show("No record found", "Please try again");
                }
               

            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
            return "false";
        }

        public string eName { set; get; }
        public string eEmail { set; get; }
        public string ePhone { set; get; }
        public string estaffId { set; get; }
    }
}
