﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class ManageModel : Form
    {
        string correctFilename;
        bool checkchangefilename = false;
        public ManageModel(string modelid)
        {
            InitializeComponent();
            this.modelId.Text = modelid;
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT * FROM modeldetails";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while (MyReader.Read())
                {
                    if (this.modelId.Text == MyReader.GetString("modelId"))
                    {
                        this.brandId.Text = MyReader.GetString("brandId");
                        this.modelName.Text = MyReader.GetString("mName");
                        this.engineCC.Text = MyReader.GetString("mEngineCC");
                        this.quantity.Text = MyReader.GetString("mQuantity");
                        this.seatCapacity.Text = MyReader.GetString("mSeats");
                        this.Door.Text = MyReader.GetString("mDoor");
                        this.mDescription.Text = MyReader.GetString("mDescription");
                        this.mPicture.Text = MyReader.GetString("mPicture");

                        ///show picture in update car page
                        string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                        mPicture.Image = Image.FromFile(paths + "\\pictures\\" + MyReader.GetString("mPicture"));
                    }

                }
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void searchProduct_Click(object sender, EventArgs e)
        {
            //search for branch name and status from the pid entered
            try
            {
                string checkproductid;
                bool checkproduct = false;
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string query = "SELECT * FROM producttable WHERE pId='" + this.brandId.Text + "';";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    checkproductid = MyReader.GetString("pId");

                    if (checkproductid == brandId.Text)
                    {
                        checkproduct = true;
                        MessageBox.Show(MyReader.GetString("pBrand"), "Product Name");
                    }

                }
                MyConn.Close();

                //check for product id existence
                if (checkproduct == true)
                {
                    searchProduct.BackColor = Color.Green;
                }
                else
                {
                    searchProduct.BackColor = Color.Red;
                    MessageBox.Show("Product ID doesn't exist. ", "Please try again.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            checkchangefilename = true;
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    mPicture.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void Add_Carbtn_Click(object sender, EventArgs e)
        {
        
            if (searchProduct.BackColor == Color.Green)
            {
                try
                {

                    //to connect to phpmyadmin database name cardealership
                    string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";
                    string Query;

                   if(checkchangefilename == true)
                    {
                        //insert from the table of model details
                        Query = "UPDATE modeldetails set brandId ='" + this.brandId.Text + "',mName='" + this.modelName.Text + "',mEngineCC='" + this.engineCC.Text +
                            "',mSeats='" + this.seatCapacity.Text + "',mDoor='" + this.Door.Text + "',mQuantity='" + this.quantity.Text + "',mDescription='" + this.mDescription.Text +
                             "',mPicture='" + correctFilename + "' WHERE modelId='" + this.modelId.Text + "';";
                    }
                   else
                    {
                        //insert from the table of model details
                        Query = "UPDATE modeldetails set brandId ='" + this.brandId.Text + "',mName='" + this.modelName.Text + "',mEngineCC='" + this.engineCC.Text +
                            "',mSeats='" + this.seatCapacity.Text + "',mDoor='" + this.Door.Text + "',mQuantity='" + this.quantity.Text + "',mDescription='" + this.mDescription.Text +
                             "',mPicture='" + this.mPicture.Text + "' WHERE modelId='" + this.modelId.Text + "';";
                    }
                 

                    MySqlConnection MyConn = new MySqlConnection(Conn);
                    MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MessageBox.Show("Record Updated", "Update Record");
                    MyConn.Close();

                    brandId.Text = "";
                    searchProduct.BackColor = Color.White;
                    engineCC.Text = "";
                    modelName.Text = "";
                    seatCapacity.Text = "";
                    Door.Text = "";

                    quantity.Text = "";
                    mDescription.Text = "";

                    mPicture.Image = null;
                    this.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

              
            }
            else
            {
                MessageBox.Show("Please check the search box for checking process", "Error");
            }
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            brandId.Text = "";
            searchProduct.BackColor = Color.White;
            engineCC.Text = "";
            modelName.Text = "";
            seatCapacity.Text = "";
            Door.Text = "";

            quantity.Text = "";
            mDescription.Text = "";

            mPicture.Image = null;
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void engineCC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void seatCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void Door_TextChanged(object sender, EventArgs e)
        {

        }

        private void Door_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
