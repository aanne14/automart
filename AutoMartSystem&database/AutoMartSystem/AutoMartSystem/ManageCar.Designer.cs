﻿namespace AutoMartSystem
{
    partial class ManageCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchProduct = new System.Windows.Forms.Button();
            this.carId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnOpenPic = new System.Windows.Forms.Button();
            this.cColour = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cPicture = new System.Windows.Forms.PictureBox();
            this.quantity = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cDescription = new System.Windows.Forms.ComboBox();
            this.modelId = new System.Windows.Forms.TextBox();
            this.cPrice = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cLocation = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cProductionYear = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cCarPlateNo = new System.Windows.Forms.TextBox();
            this.cMileage = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.Add_Carbtn = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // searchProduct
            // 
            this.searchProduct.Location = new System.Drawing.Point(435, 62);
            this.searchProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchProduct.Name = "searchProduct";
            this.searchProduct.Size = new System.Drawing.Size(75, 27);
            this.searchProduct.TabIndex = 122;
            this.searchProduct.Text = "Search";
            this.searchProduct.UseVisualStyleBackColor = true;
            this.searchProduct.Click += new System.EventHandler(this.searchProduct_Click);
            // 
            // carId
            // 
            this.carId.Enabled = false;
            this.carId.Location = new System.Drawing.Point(311, 33);
            this.carId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.carId.Name = "carId";
            this.carId.Size = new System.Drawing.Size(119, 22);
            this.carId.TabIndex = 121;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(125, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.TabIndex = 120;
            this.label3.Text = "Car ID: ";
            // 
            // cType
            // 
            this.cType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cType.FormattingEnabled = true;
            this.cType.Items.AddRange(new object[] {
            "Used Car",
            "New Car"});
            this.cType.Location = new System.Drawing.Point(307, 98);
            this.cType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cType.Name = "cType";
            this.cType.Size = new System.Drawing.Size(196, 28);
            this.cType.TabIndex = 119;
            this.cType.Text = "-Select Type-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(123, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 20);
            this.label6.TabIndex = 118;
            this.label6.Text = "Product Type:";
            // 
            // btnOpenPic
            // 
            this.btnOpenPic.Location = new System.Drawing.Point(697, 325);
            this.btnOpenPic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOpenPic.Name = "btnOpenPic";
            this.btnOpenPic.Size = new System.Drawing.Size(129, 30);
            this.btnOpenPic.TabIndex = 117;
            this.btnOpenPic.Text = "Open Picture";
            this.btnOpenPic.UseVisualStyleBackColor = true;
            this.btnOpenPic.Click += new System.EventHandler(this.btnOpenPic_Click);
            // 
            // cColour
            // 
            this.cColour.Location = new System.Drawing.Point(308, 140);
            this.cColour.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cColour.Name = "cColour";
            this.cColour.Size = new System.Drawing.Size(196, 22);
            this.cColour.TabIndex = 116;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(123, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 115;
            this.label5.Text = "Car Colour: ";
            // 
            // cPicture
            // 
            this.cPicture.Location = new System.Drawing.Point(548, 66);
            this.cPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cPicture.Name = "cPicture";
            this.cPicture.Size = new System.Drawing.Size(396, 242);
            this.cPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cPicture.TabIndex = 114;
            this.cPicture.TabStop = false;
            // 
            // quantity
            // 
            this.quantity.Location = new System.Drawing.Point(304, 322);
            this.quantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(87, 22);
            this.quantity.TabIndex = 113;
            this.quantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantity_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(123, 325);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 20);
            this.label14.TabIndex = 112;
            this.label14.Text = "Quantity: ";
            // 
            // cDescription
            // 
            this.cDescription.FormattingEnabled = true;
            this.cDescription.Items.AddRange(new object[] {
            "Sports Car",
            "Recreational Car",
            "Family Car"});
            this.cDescription.Location = new System.Drawing.Point(307, 361);
            this.cDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cDescription.Name = "cDescription";
            this.cDescription.Size = new System.Drawing.Size(197, 24);
            this.cDescription.TabIndex = 110;
            this.cDescription.Text = "-Select-";
            // 
            // modelId
            // 
            this.modelId.Location = new System.Drawing.Point(311, 66);
            this.modelId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.modelId.Name = "modelId";
            this.modelId.Size = new System.Drawing.Size(119, 22);
            this.modelId.TabIndex = 109;
            // 
            // cPrice
            // 
            this.cPrice.Location = new System.Drawing.Point(304, 401);
            this.cPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cPrice.Name = "cPrice";
            this.cPrice.Size = new System.Drawing.Size(196, 22);
            this.cPrice.TabIndex = 108;
            this.cPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cPrice_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(125, 402);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 20);
            this.label15.TabIndex = 107;
            this.label15.Text = "Price(RM): ";
            // 
            // cLocation
            // 
            this.cLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cLocation.FormattingEnabled = true;
            this.cLocation.Items.AddRange(new object[] {
            "Car Dealership Subang Jaya",
            "Car Dealership Glenmarie",
            "Car Dealership KL"});
            this.cLocation.Location = new System.Drawing.Point(307, 178);
            this.cLocation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cLocation.Name = "cLocation";
            this.cLocation.Size = new System.Drawing.Size(235, 28);
            this.cLocation.TabIndex = 106;
            this.cLocation.Text = "-Select Location-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(123, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 20);
            this.label2.TabIndex = 105;
            this.label2.Text = "Branch Location: ";
            // 
            // cProductionYear
            // 
            this.cProductionYear.Location = new System.Drawing.Point(307, 254);
            this.cProductionYear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cProductionYear.Name = "cProductionYear";
            this.cProductionYear.Size = new System.Drawing.Size(196, 22);
            this.cProductionYear.TabIndex = 104;
            this.cProductionYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cProductionYear_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(123, 256);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 20);
            this.label12.TabIndex = 103;
            this.label12.Text = "Production Year:";
            // 
            // cCarPlateNo
            // 
            this.cCarPlateNo.Location = new System.Drawing.Point(307, 222);
            this.cCarPlateNo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cCarPlateNo.Name = "cCarPlateNo";
            this.cCarPlateNo.Size = new System.Drawing.Size(196, 22);
            this.cCarPlateNo.TabIndex = 102;
            // 
            // cMileage
            // 
            this.cMileage.Location = new System.Drawing.Point(307, 286);
            this.cMileage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cMileage.Name = "cMileage";
            this.cMileage.Size = new System.Drawing.Size(196, 22);
            this.cMileage.TabIndex = 101;
            this.cMileage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cMileage_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(123, 366);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 100;
            this.label9.Text = "Description: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(123, 288);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 20);
            this.label8.TabIndex = 99;
            this.label8.Text = "Current Mileage: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(123, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.TabIndex = 98;
            this.label4.Text = "Car Plate No: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(125, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 97;
            this.label1.Text = "Model ID: ";
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(548, 490);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(172, 31);
            this.exitBtn.TabIndex = 96;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(339, 490);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(172, 31);
            this.clearbtn.TabIndex = 95;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // Add_Carbtn
            // 
            this.Add_Carbtn.Location = new System.Drawing.Point(125, 490);
            this.Add_Carbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_Carbtn.Name = "Add_Carbtn";
            this.Add_Carbtn.Size = new System.Drawing.Size(172, 31);
            this.Add_Carbtn.TabIndex = 94;
            this.Add_Carbtn.Text = "Confirm";
            this.Add_Carbtn.UseVisualStyleBackColor = true;
            this.Add_Carbtn.Click += new System.EventHandler(this.Add_Carbtn_Click);
            // 
            // status
            // 
            this.status.Location = new System.Drawing.Point(304, 441);
            this.status.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(196, 22);
            this.status.TabIndex = 124;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(131, 442);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 20);
            this.label7.TabIndex = 123;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(123, 442);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 20);
            this.label10.TabIndex = 125;
            this.label10.Text = "Status: ";
            // 
            // ManageCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.status);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.searchProduct);
            this.Controls.Add(this.carId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnOpenPic);
            this.Controls.Add(this.cColour);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cPicture);
            this.Controls.Add(this.quantity);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cDescription);
            this.Controls.Add(this.modelId);
            this.Controls.Add(this.cPrice);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cLocation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cProductionYear);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cCarPlateNo);
            this.Controls.Add(this.cMileage);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.Add_Carbtn);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ManageCar";
            this.Text = "ManageCar";
            ((System.ComponentModel.ISupportInitialize)(this.cPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchProduct;
        private System.Windows.Forms.TextBox carId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnOpenPic;
        private System.Windows.Forms.TextBox cColour;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox cPicture;
        private System.Windows.Forms.TextBox quantity;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cDescription;
        private System.Windows.Forms.TextBox modelId;
        private System.Windows.Forms.TextBox cPrice;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cProductionYear;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox cCarPlateNo;
        private System.Windows.Forms.TextBox cMileage;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button Add_Carbtn;
        private System.Windows.Forms.TextBox status;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
    }
}