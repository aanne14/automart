﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class addModel : Form
    {
        string correctFilename;
        public addModel()
        {
            InitializeComponent();

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT MAX(modelId) FROM modeldetails";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {

                    modelId.Text = "M" + (int.Parse(MyReader.GetString("MAX(modelId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    mPicture.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void searchProduct_Click(object sender, EventArgs e)
        {
            //search for branch name and status from the pid entered
            try
            {
                string checkproductid;
                bool checkproduct = false;
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string query = "SELECT pId FROM producttable WHERE pId='" + this.brandId.Text + "';";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    checkproductid = MyReader.GetString("pId");

                    if (checkproductid == brandId.Text)
                    {
                        checkproduct = true;
                    }

                }
                MyConn.Close();

                //check for product id existence
                if (checkproduct == true)
                {
                    searchProduct.BackColor = Color.Green;
                }
                else
                {
                    searchProduct.BackColor = Color.Red;
                    MessageBox.Show("Product ID doesn't exist. ", "Please try again.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {

            brandId.Text = "";
            searchProduct.BackColor = Color.White;
            engineCC.Text = "";
            modelName.Text = "";
            seatCapacity.Text = "";
            Door.Text = "";

            quantity.Text = "";
            mDescription.Text = "";

            mPicture.Image = null;
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Add_Carbtn_Click(object sender, EventArgs e)
        {
            if (searchProduct.BackColor == Color.Green)
            {
                try
                {

                    //to connect to phpmyadmin database name cardealership
                    string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";



                    //insert from the table of model details
                    string Query = "INSERT INTO modeldetails (modelId,brandId,mName,mEngineCC,mQuantity,mSeats,mDoor,mDescription,mPicture) " +
                        "VALUES ('" + this.modelId.Text + "','" + this.brandId.Text + "','" + this.modelName.Text + "','" + this.engineCC.Text  +
                        "','" + this.quantity.Text + "','" + this.seatCapacity.Text + "','" + this.Door.Text + "','" + this.mDescription.Text +
                         "','" + correctFilename + "');";

                    MySqlConnection MyConn = new MySqlConnection(Conn);
                    MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MessageBox.Show("Record Saved", "Insert Record");
                    MyConn.Close();

                    Query = "SELECT MAX(modelId) FROM modeldetails";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {
                        modelId.Text = "M" + (int.Parse(MyReader.GetString("MAX(modelId)").Remove(0, 1)) + 1).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Please try again", "Please try again");
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                brandId.Text = "";
                searchProduct.BackColor = Color.White;
                engineCC.Text = "";
                modelName.Text = "";
                seatCapacity.Text = "";
                Door.Text = "";

                quantity.Text = "";
                mDescription.Text = "";

                mPicture.Image = null;
            }
        }

        private void engineCC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void quantity_TextChanged(object sender, EventArgs e)
        {

        }

        private void quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void seatCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void Door_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
