﻿namespace AutoMartSystem
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Admin));
            this.label1 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.updateAccBtn = new System.Windows.Forms.Button();
            this.acPicture = new System.Windows.Forms.PictureBox();
            this.updateAcc = new System.Windows.Forms.Button();
            this.refreshAccBtn = new System.Windows.Forms.Button();
            this.accSearchBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.searchAcc = new System.Windows.Forms.Button();
            this.accTable = new System.Windows.Forms.DataGridView();
            this.accId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accPicture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addAcc = new System.Windows.Forms.Button();
            this.Car = new System.Windows.Forms.TabPage();
            this.manageCarBtn = new System.Windows.Forms.Button();
            this.carPicture = new System.Windows.Forms.PictureBox();
            this.updateCarTable = new System.Windows.Forms.Button();
            this.carRefreshBtn = new System.Windows.Forms.Button();
            this.carSearchBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.carSearch = new System.Windows.Forms.Button();
            this.addCar = new System.Windows.Forms.Button();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.manageCustBtn = new System.Windows.Forms.Button();
            this.searchCustomer = new System.Windows.Forms.Button();
            this.customerTable = new System.Windows.Forms.DataGridView();
            this.updateCustTable = new System.Windows.Forms.Button();
            this.cusRefresh = new System.Windows.Forms.Button();
            this.searchDetails = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.addCustomer = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.transferTable = new System.Windows.Forms.DataGridView();
            this.transfercarid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transfersID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transferfrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transferto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tquantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transferDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updateTransferTable = new System.Windows.Forms.Button();
            this.refreshTransferBtn = new System.Windows.Forms.Button();
            this.searchTransfer = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.searchTransferBtn = new System.Windows.Forms.Button();
            this.createTransfer = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.Add_productbtn = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabpage6 = new System.Windows.Forms.TabPage();
            this.sPromoProduBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.afterdiscount = new System.Windows.Forms.TextBox();
            this.discount = new System.Windows.Forms.TextBox();
            this.promoproduType = new System.Windows.Forms.TextBox();
            this.promoproduBrand = new System.Windows.Forms.TextBox();
            this.promoProduPrice = new System.Windows.Forms.TextBox();
            this.promoproduQuantity = new System.Windows.Forms.TextBox();
            this.promoproduid = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.invoiceNum = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.manageBrandBtn = new System.Windows.Forms.Button();
            this.brandPicture = new System.Windows.Forms.PictureBox();
            this.updateBrand = new System.Windows.Forms.Button();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.sSearch = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.searchBrand = new System.Windows.Forms.Button();
            this.brandTable = new System.Windows.Forms.DataGridView();
            this.productId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pPicture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addBrand = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.aID = new System.Windows.Forms.Label();
            this.aEmail = new System.Windows.Forms.Label();
            this.aPhone = new System.Windows.Forms.Label();
            this.aName = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.updateModelBtn = new System.Windows.Forms.Button();
            this.mPicture = new System.Windows.Forms.PictureBox();
            this.updmodelTable = new System.Windows.Forms.Button();
            this.refreshModelBtn = new System.Windows.Forms.Button();
            this.modelSearchBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.searchModel = new System.Windows.Forms.Button();
            this.modelTable = new System.Windows.Forms.DataGridView();
            this.modId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mEngineCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mSeats = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mDoor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addModel = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custIC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Car_Model = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarDataTable = new System.Windows.Forms.DataGridView();
            this.cartableid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cartablebrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cColour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carBranch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carplateno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManufactureYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mileage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPicture = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.acPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accTable)).BeginInit();
            this.Car.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.carPicture)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerTable)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transferTable)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.tabpage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.brandPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brandTable)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarDataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(1024, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 54);
            this.label1.TabIndex = 1;
            this.label1.Text = "Admin";
            // 
            // logoutBtn
            // 
            this.logoutBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logoutBtn.Location = new System.Drawing.Point(1021, 96);
            this.logoutBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(173, 34);
            this.logoutBtn.TabIndex = 3;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.updateAccBtn);
            this.tabPage9.Controls.Add(this.acPicture);
            this.tabPage9.Controls.Add(this.updateAcc);
            this.tabPage9.Controls.Add(this.refreshAccBtn);
            this.tabPage9.Controls.Add(this.accSearchBox);
            this.tabPage9.Controls.Add(this.label21);
            this.tabPage9.Controls.Add(this.searchAcc);
            this.tabPage9.Controls.Add(this.accTable);
            this.tabPage9.Controls.Add(this.addAcc);
            this.tabPage9.Location = new System.Drawing.Point(4, 42);
            this.tabPage9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage9.Size = new System.Drawing.Size(1151, 510);
            this.tabPage9.TabIndex = 10;
            this.tabPage9.Text = "Accessories";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // updateAccBtn
            // 
            this.updateAccBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateAccBtn.Location = new System.Drawing.Point(17, 116);
            this.updateAccBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateAccBtn.Name = "updateAccBtn";
            this.updateAccBtn.Size = new System.Drawing.Size(226, 34);
            this.updateAccBtn.TabIndex = 35;
            this.updateAccBtn.Text = "Update Accessories";
            this.updateAccBtn.UseVisualStyleBackColor = true;
            this.updateAccBtn.Click += new System.EventHandler(this.updateAccBtn_Click);
            // 
            // acPicture
            // 
            this.acPicture.Location = new System.Drawing.Point(640, 23);
            this.acPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.acPicture.Name = "acPicture";
            this.acPicture.Size = new System.Drawing.Size(421, 178);
            this.acPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.acPicture.TabIndex = 34;
            this.acPicture.TabStop = false;
            // 
            // updateAcc
            // 
            this.updateAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateAcc.Location = new System.Drawing.Point(249, 167);
            this.updateAcc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateAcc.Name = "updateAcc";
            this.updateAcc.Size = new System.Drawing.Size(239, 34);
            this.updateAcc.TabIndex = 33;
            this.updateAcc.Text = "Update Table";
            this.updateAcc.UseVisualStyleBackColor = true;
            this.updateAcc.Click += new System.EventHandler(this.updateAcc_Click);
            // 
            // refreshAccBtn
            // 
            this.refreshAccBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshAccBtn.Location = new System.Drawing.Point(17, 167);
            this.refreshAccBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.refreshAccBtn.Name = "refreshAccBtn";
            this.refreshAccBtn.Size = new System.Drawing.Size(226, 34);
            this.refreshAccBtn.TabIndex = 32;
            this.refreshAccBtn.Text = "Refresh";
            this.refreshAccBtn.UseVisualStyleBackColor = true;
            this.refreshAccBtn.Click += new System.EventHandler(this.refreshAccBtn_Click);
            // 
            // accSearchBox
            // 
            this.accSearchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accSearchBox.Location = new System.Drawing.Point(113, 39);
            this.accSearchBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accSearchBox.Name = "accSearchBox";
            this.accSearchBox.Size = new System.Drawing.Size(177, 30);
            this.accSearchBox.TabIndex = 31;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(12, 44);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 25);
            this.label21.TabIndex = 30;
            this.label21.Text = "Search:";
            // 
            // searchAcc
            // 
            this.searchAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchAcc.Location = new System.Drawing.Point(315, 39);
            this.searchAcc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchAcc.Name = "searchAcc";
            this.searchAcc.Size = new System.Drawing.Size(173, 34);
            this.searchAcc.TabIndex = 29;
            this.searchAcc.Text = "Search";
            this.searchAcc.UseVisualStyleBackColor = true;
            this.searchAcc.Click += new System.EventHandler(this.searchAcc_Click);
            // 
            // accTable
            // 
            this.accTable.AllowUserToOrderColumns = true;
            this.accTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.accId,
            this.accName,
            this.accPrice,
            this.accQuantity,
            this.accPicture});
            this.accTable.Location = new System.Drawing.Point(17, 222);
            this.accTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.accTable.Name = "accTable";
            this.accTable.RowHeadersWidth = 51;
            this.accTable.Size = new System.Drawing.Size(1053, 238);
            this.accTable.TabIndex = 28;
            this.accTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.accTable_CellContentClick);
            // 
            // accId
            // 
            this.accId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.accId.DataPropertyName = "accId";
            this.accId.HeaderText = "Accessories ID";
            this.accId.MinimumWidth = 6;
            this.accId.Name = "accId";
            this.accId.Width = 202;
            // 
            // accName
            // 
            this.accName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.accName.DataPropertyName = "accName";
            this.accName.FillWeight = 20F;
            this.accName.HeaderText = "Name";
            this.accName.MinimumWidth = 6;
            this.accName.Name = "accName";
            this.accName.Width = 107;
            // 
            // accPrice
            // 
            this.accPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.accPrice.DataPropertyName = "accPrice";
            this.accPrice.HeaderText = "Price";
            this.accPrice.MinimumWidth = 6;
            this.accPrice.Name = "accPrice";
            this.accPrice.Width = 98;
            // 
            // accQuantity
            // 
            this.accQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.accQuantity.DataPropertyName = "accQuantity";
            this.accQuantity.HeaderText = "Quantity";
            this.accQuantity.MinimumWidth = 6;
            this.accQuantity.Name = "accQuantity";
            this.accQuantity.Width = 129;
            // 
            // accPicture
            // 
            this.accPicture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.accPicture.DataPropertyName = "accPicture";
            this.accPicture.HeaderText = "Picture URL";
            this.accPicture.MinimumWidth = 6;
            this.accPicture.Name = "accPicture";
            this.accPicture.Width = 170;
            // 
            // addAcc
            // 
            this.addAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAcc.Location = new System.Drawing.Point(249, 116);
            this.addAcc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addAcc.Name = "addAcc";
            this.addAcc.Size = new System.Drawing.Size(239, 34);
            this.addAcc.TabIndex = 27;
            this.addAcc.Text = "Add Accessories";
            this.addAcc.UseVisualStyleBackColor = true;
            this.addAcc.Click += new System.EventHandler(this.addAcc_Click);
            // 
            // Car
            // 
            this.Car.Controls.Add(this.CarDataTable);
            this.Car.Controls.Add(this.manageCarBtn);
            this.Car.Controls.Add(this.carPicture);
            this.Car.Controls.Add(this.updateCarTable);
            this.Car.Controls.Add(this.carRefreshBtn);
            this.Car.Controls.Add(this.carSearchBox);
            this.Car.Controls.Add(this.label15);
            this.Car.Controls.Add(this.carSearch);
            this.Car.Controls.Add(this.addCar);
            this.Car.Location = new System.Drawing.Point(4, 42);
            this.Car.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Car.Name = "Car";
            this.Car.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Car.Size = new System.Drawing.Size(1151, 510);
            this.Car.TabIndex = 9;
            this.Car.Text = "Car";
            this.Car.UseVisualStyleBackColor = true;
            // 
            // manageCarBtn
            // 
            this.manageCarBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageCarBtn.Location = new System.Drawing.Point(16, 110);
            this.manageCarBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.manageCarBtn.Name = "manageCarBtn";
            this.manageCarBtn.Size = new System.Drawing.Size(173, 34);
            this.manageCarBtn.TabIndex = 27;
            this.manageCarBtn.Text = "Manage Car";
            this.manageCarBtn.UseVisualStyleBackColor = true;
            this.manageCarBtn.Click += new System.EventHandler(this.manageCarBtn_Click);
            // 
            // carPicture
            // 
            this.carPicture.Location = new System.Drawing.Point(637, 12);
            this.carPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.carPicture.Name = "carPicture";
            this.carPicture.Size = new System.Drawing.Size(421, 178);
            this.carPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.carPicture.TabIndex = 26;
            this.carPicture.TabStop = false;
            // 
            // updateCarTable
            // 
            this.updateCarTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateCarTable.Location = new System.Drawing.Point(195, 158);
            this.updateCarTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateCarTable.Name = "updateCarTable";
            this.updateCarTable.Size = new System.Drawing.Size(173, 34);
            this.updateCarTable.TabIndex = 25;
            this.updateCarTable.Text = "Update Table";
            this.updateCarTable.UseVisualStyleBackColor = true;
            this.updateCarTable.Click += new System.EventHandler(this.updateCarTable_Click);
            // 
            // carRefreshBtn
            // 
            this.carRefreshBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carRefreshBtn.Location = new System.Drawing.Point(13, 158);
            this.carRefreshBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.carRefreshBtn.Name = "carRefreshBtn";
            this.carRefreshBtn.Size = new System.Drawing.Size(173, 34);
            this.carRefreshBtn.TabIndex = 24;
            this.carRefreshBtn.Text = "Refresh";
            this.carRefreshBtn.UseVisualStyleBackColor = true;
            this.carRefreshBtn.Click += new System.EventHandler(this.carRefreshBtn_Click);
            // 
            // carSearchBox
            // 
            this.carSearchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carSearchBox.Location = new System.Drawing.Point(109, 28);
            this.carSearchBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.carSearchBox.Name = "carSearchBox";
            this.carSearchBox.Size = new System.Drawing.Size(177, 30);
            this.carSearchBox.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 25);
            this.label15.TabIndex = 22;
            this.label15.Text = "Search:";
            // 
            // carSearch
            // 
            this.carSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carSearch.Location = new System.Drawing.Point(311, 28);
            this.carSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.carSearch.Name = "carSearch";
            this.carSearch.Size = new System.Drawing.Size(173, 34);
            this.carSearch.TabIndex = 21;
            this.carSearch.Text = "Search";
            this.carSearch.UseVisualStyleBackColor = true;
            this.carSearch.Click += new System.EventHandler(this.carSearch_Click);
            // 
            // addCar
            // 
            this.addCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addCar.Location = new System.Drawing.Point(195, 110);
            this.addCar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addCar.Name = "addCar";
            this.addCar.Size = new System.Drawing.Size(173, 34);
            this.addCar.TabIndex = 19;
            this.addCar.Text = "Add Car";
            this.addCar.UseVisualStyleBackColor = true;
            this.addCar.Click += new System.EventHandler(this.addCar_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.manageCustBtn);
            this.tabPage8.Controls.Add(this.searchCustomer);
            this.tabPage8.Controls.Add(this.customerTable);
            this.tabPage8.Controls.Add(this.updateCustTable);
            this.tabPage8.Controls.Add(this.cusRefresh);
            this.tabPage8.Controls.Add(this.searchDetails);
            this.tabPage8.Controls.Add(this.label14);
            this.tabPage8.Controls.Add(this.addCustomer);
            this.tabPage8.Location = new System.Drawing.Point(4, 42);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage8.Size = new System.Drawing.Size(1151, 510);
            this.tabPage8.TabIndex = 8;
            this.tabPage8.Text = "Customer";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // manageCustBtn
            // 
            this.manageCustBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageCustBtn.Location = new System.Drawing.Point(475, 112);
            this.manageCustBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.manageCustBtn.Name = "manageCustBtn";
            this.manageCustBtn.Size = new System.Drawing.Size(215, 34);
            this.manageCustBtn.TabIndex = 28;
            this.manageCustBtn.Text = "Manage Customer";
            this.manageCustBtn.UseVisualStyleBackColor = true;
            this.manageCustBtn.Click += new System.EventHandler(this.manageCustBtn_Click);
            // 
            // searchCustomer
            // 
            this.searchCustomer.Location = new System.Drawing.Point(413, 25);
            this.searchCustomer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchCustomer.Name = "searchCustomer";
            this.searchCustomer.Size = new System.Drawing.Size(131, 46);
            this.searchCustomer.TabIndex = 27;
            this.searchCustomer.Text = "Search";
            this.searchCustomer.UseVisualStyleBackColor = true;
            this.searchCustomer.Click += new System.EventHandler(this.searchCustomer_Click);
            // 
            // customerTable
            // 
            this.customerTable.AllowUserToOrderColumns = true;
            this.customerTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.customerTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.custID,
            this.custIC,
            this.custName,
            this.custAdd,
            this.custPhone,
            this.custGender,
            this.custAge,
            this.cstatus});
            this.customerTable.Location = new System.Drawing.Point(5, 164);
            this.customerTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.customerTable.Name = "customerTable";
            this.customerTable.RowHeadersWidth = 51;
            this.customerTable.Size = new System.Drawing.Size(1071, 311);
            this.customerTable.TabIndex = 26;
            this.customerTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.customerTable_CellContentClick);
            // 
            // updateCustTable
            // 
            this.updateCustTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateCustTable.Location = new System.Drawing.Point(875, 112);
            this.updateCustTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateCustTable.Name = "updateCustTable";
            this.updateCustTable.Size = new System.Drawing.Size(173, 34);
            this.updateCustTable.TabIndex = 25;
            this.updateCustTable.Text = "Update Table";
            this.updateCustTable.UseVisualStyleBackColor = true;
            this.updateCustTable.Click += new System.EventHandler(this.updateCustTable_Click);
            // 
            // cusRefresh
            // 
            this.cusRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cusRefresh.Location = new System.Drawing.Point(695, 112);
            this.cusRefresh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cusRefresh.Name = "cusRefresh";
            this.cusRefresh.Size = new System.Drawing.Size(173, 34);
            this.cusRefresh.TabIndex = 24;
            this.cusRefresh.Text = "Refresh";
            this.cusRefresh.UseVisualStyleBackColor = true;
            this.cusRefresh.Click += new System.EventHandler(this.cusRefresh_Click);
            // 
            // searchDetails
            // 
            this.searchDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchDetails.Location = new System.Drawing.Point(211, 36);
            this.searchDetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchDetails.Name = "searchDetails";
            this.searchDetails.Size = new System.Drawing.Size(177, 30);
            this.searchDetails.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(101, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 25);
            this.label14.TabIndex = 22;
            this.label14.Text = "Search:";
            // 
            // addCustomer
            // 
            this.addCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addCustomer.Location = new System.Drawing.Point(875, 34);
            this.addCustomer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addCustomer.Name = "addCustomer";
            this.addCustomer.Size = new System.Drawing.Size(173, 34);
            this.addCustomer.TabIndex = 19;
            this.addCustomer.Text = "Add Customer";
            this.addCustomer.UseVisualStyleBackColor = true;
            this.addCustomer.Click += new System.EventHandler(this.addCustomer_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.transferTable);
            this.tabPage3.Controls.Add(this.updateTransferTable);
            this.tabPage3.Controls.Add(this.refreshTransferBtn);
            this.tabPage3.Controls.Add(this.searchTransfer);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.searchTransferBtn);
            this.tabPage3.Controls.Add(this.createTransfer);
            this.tabPage3.Location = new System.Drawing.Point(4, 42);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage3.Size = new System.Drawing.Size(1151, 510);
            this.tabPage3.TabIndex = 7;
            this.tabPage3.Text = "Transfer";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // transferTable
            // 
            this.transferTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.transferTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.transfercarid,
            this.transfersID,
            this.transferfrom,
            this.transferto,
            this.tquantity,
            this.transferDescription});
            this.transferTable.Location = new System.Drawing.Point(5, 225);
            this.transferTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.transferTable.Name = "transferTable";
            this.transferTable.RowHeadersWidth = 51;
            this.transferTable.RowTemplate.Height = 24;
            this.transferTable.Size = new System.Drawing.Size(1125, 240);
            this.transferTable.TabIndex = 27;
            this.transferTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.transferTable_CellContentClick);
            // 
            // transfercarid
            // 
            this.transfercarid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.transfercarid.DataPropertyName = "productid";
            this.transfercarid.HeaderText = "Car ID";
            this.transfercarid.MinimumWidth = 6;
            this.transfercarid.Name = "transfercarid";
            // 
            // transfersID
            // 
            this.transfersID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.transfersID.DataPropertyName = "transferid";
            this.transfersID.HeaderText = "Transfer ID";
            this.transfersID.MinimumWidth = 6;
            this.transfersID.Name = "transfersID";
            // 
            // transferfrom
            // 
            this.transferfrom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.transferfrom.DataPropertyName = "transferfrom";
            this.transferfrom.HeaderText = "Transfer From";
            this.transferfrom.MinimumWidth = 6;
            this.transferfrom.Name = "transferfrom";
            // 
            // transferto
            // 
            this.transferto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.transferto.DataPropertyName = "transferto";
            this.transferto.HeaderText = "Transfer To";
            this.transferto.MinimumWidth = 6;
            this.transferto.Name = "transferto";
            // 
            // tquantity
            // 
            this.tquantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tquantity.DataPropertyName = "tquantity";
            this.tquantity.HeaderText = "Quantity";
            this.tquantity.MinimumWidth = 6;
            this.tquantity.Name = "tquantity";
            // 
            // transferDescription
            // 
            this.transferDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.transferDescription.DataPropertyName = "tdescription";
            this.transferDescription.HeaderText = "Description";
            this.transferDescription.MinimumWidth = 6;
            this.transferDescription.Name = "transferDescription";
            // 
            // updateTransferTable
            // 
            this.updateTransferTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateTransferTable.Location = new System.Drawing.Point(875, 158);
            this.updateTransferTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateTransferTable.Name = "updateTransferTable";
            this.updateTransferTable.Size = new System.Drawing.Size(173, 34);
            this.updateTransferTable.TabIndex = 25;
            this.updateTransferTable.Text = "Update Table";
            this.updateTransferTable.UseVisualStyleBackColor = true;
            this.updateTransferTable.Click += new System.EventHandler(this.updateTransferTable_Click);
            // 
            // refreshTransferBtn
            // 
            this.refreshTransferBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshTransferBtn.Location = new System.Drawing.Point(693, 158);
            this.refreshTransferBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.refreshTransferBtn.Name = "refreshTransferBtn";
            this.refreshTransferBtn.Size = new System.Drawing.Size(173, 34);
            this.refreshTransferBtn.TabIndex = 24;
            this.refreshTransferBtn.Text = "Refresh";
            this.refreshTransferBtn.UseVisualStyleBackColor = true;
            this.refreshTransferBtn.Click += new System.EventHandler(this.refreshTransferBtn_Click);
            // 
            // searchTransfer
            // 
            this.searchTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTransfer.Location = new System.Drawing.Point(211, 36);
            this.searchTransfer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchTransfer.Name = "searchTransfer";
            this.searchTransfer.Size = new System.Drawing.Size(177, 30);
            this.searchTransfer.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(101, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 25);
            this.label13.TabIndex = 22;
            this.label13.Text = "Search:";
            // 
            // searchTransferBtn
            // 
            this.searchTransferBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTransferBtn.Location = new System.Drawing.Point(408, 33);
            this.searchTransferBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchTransferBtn.Name = "searchTransferBtn";
            this.searchTransferBtn.Size = new System.Drawing.Size(173, 34);
            this.searchTransferBtn.TabIndex = 21;
            this.searchTransferBtn.Text = "Search";
            this.searchTransferBtn.UseVisualStyleBackColor = true;
            this.searchTransferBtn.Click += new System.EventHandler(this.searchTransferBtn_Click);
            // 
            // createTransfer
            // 
            this.createTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createTransfer.Location = new System.Drawing.Point(875, 33);
            this.createTransfer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.createTransfer.Name = "createTransfer";
            this.createTransfer.Size = new System.Drawing.Size(173, 34);
            this.createTransfer.TabIndex = 19;
            this.createTransfer.Text = "Create Transfer ";
            this.createTransfer.UseVisualStyleBackColor = true;
            this.createTransfer.Click += new System.EventHandler(this.createTransfer_Click);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dateTimePicker1);
            this.tabPage7.Controls.Add(this.label41);
            this.tabPage7.Controls.Add(this.textBox12);
            this.tabPage7.Controls.Add(this.textBox11);
            this.tabPage7.Controls.Add(this.textBox10);
            this.tabPage7.Controls.Add(this.textBox9);
            this.tabPage7.Controls.Add(this.label20);
            this.tabPage7.Controls.Add(this.checkBox5);
            this.tabPage7.Controls.Add(this.checkBox4);
            this.tabPage7.Controls.Add(this.checkBox3);
            this.tabPage7.Controls.Add(this.label19);
            this.tabPage7.Controls.Add(this.label18);
            this.tabPage7.Controls.Add(this.button5);
            this.tabPage7.Controls.Add(this.Add_productbtn);
            this.tabPage7.Controls.Add(this.label17);
            this.tabPage7.Controls.Add(this.label16);
            this.tabPage7.Location = new System.Drawing.Point(4, 42);
            this.tabPage7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage7.Size = new System.Drawing.Size(1151, 510);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Coupon";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(701, 59);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(343, 30);
            this.dateTimePicker1.TabIndex = 16;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(499, 57);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(182, 29);
            this.label41.TabIndex = 15;
            this.label41.Text = "Expiration Date:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(360, 268);
            this.textBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(345, 114);
            this.textBox12.TabIndex = 14;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(360, 224);
            this.textBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(141, 34);
            this.textBox11.TabIndex = 8;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(285, 57);
            this.textBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(177, 34);
            this.textBox10.TabIndex = 3;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(285, 114);
            this.textBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(177, 34);
            this.textBox9.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(53, 268);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(256, 29);
            this.label20.TabIndex = 13;
            this.label20.Text = "Terms and Conditions:";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(389, 174);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(136, 33);
            this.checkBox5.TabIndex = 12;
            this.checkBox5.Text = "Used Car";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(571, 174);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(171, 33);
            this.checkBox4.TabIndex = 11;
            this.checkBox4.Text = "Trade-in Car";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(221, 174);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(129, 33);
            this.checkBox3.TabIndex = 10;
            this.checkBox3.Text = "New Car";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(53, 174);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(124, 29);
            this.label19.TabIndex = 9;
            this.label19.Text = "Car Type: ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(53, 224);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(220, 29);
            this.label18.TabIndex = 7;
            this.label18.Text = "Min Product Value: ";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(331, 409);
            this.button5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(208, 47);
            this.button5.TabIndex = 6;
            this.button5.Text = "Clear";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // Add_productbtn
            // 
            this.Add_productbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Add_productbtn.Location = new System.Drawing.Point(61, 409);
            this.Add_productbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_productbtn.Name = "Add_productbtn";
            this.Add_productbtn.Size = new System.Drawing.Size(207, 47);
            this.Add_productbtn.TabIndex = 4;
            this.Add_productbtn.Text = "Confirm";
            this.Add_productbtn.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(53, 57);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(139, 29);
            this.label17.TabIndex = 2;
            this.label17.Text = "Coupon ID: ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(53, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(177, 29);
            this.label16.TabIndex = 0;
            this.label16.Text = "Coupon Value: ";
            // 
            // tabpage6
            // 
            this.tabpage6.Controls.Add(this.sPromoProduBtn);
            this.tabpage6.Controls.Add(this.button3);
            this.tabpage6.Controls.Add(this.button1);
            this.tabpage6.Controls.Add(this.pictureBox2);
            this.tabpage6.Controls.Add(this.afterdiscount);
            this.tabpage6.Controls.Add(this.discount);
            this.tabpage6.Controls.Add(this.promoproduType);
            this.tabpage6.Controls.Add(this.promoproduBrand);
            this.tabpage6.Controls.Add(this.promoProduPrice);
            this.tabpage6.Controls.Add(this.promoproduQuantity);
            this.tabpage6.Controls.Add(this.promoproduid);
            this.tabpage6.Controls.Add(this.label10);
            this.tabpage6.Controls.Add(this.label9);
            this.tabpage6.Controls.Add(this.label8);
            this.tabpage6.Controls.Add(this.label7);
            this.tabpage6.Controls.Add(this.label6);
            this.tabpage6.Controls.Add(this.label5);
            this.tabpage6.Controls.Add(this.label4);
            this.tabpage6.Location = new System.Drawing.Point(4, 42);
            this.tabpage6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabpage6.Name = "tabpage6";
            this.tabpage6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabpage6.Size = new System.Drawing.Size(1151, 510);
            this.tabpage6.TabIndex = 5;
            this.tabpage6.Text = "Promotion";
            this.tabpage6.UseVisualStyleBackColor = true;
            // 
            // sPromoProduBtn
            // 
            this.sPromoProduBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sPromoProduBtn.Location = new System.Drawing.Point(392, 37);
            this.sPromoProduBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sPromoProduBtn.Name = "sPromoProduBtn";
            this.sPromoProduBtn.Size = new System.Drawing.Size(121, 36);
            this.sPromoProduBtn.TabIndex = 17;
            this.sPromoProduBtn.Text = "Search";
            this.sPromoProduBtn.UseVisualStyleBackColor = true;
            this.sPromoProduBtn.Click += new System.EventHandler(this.sPromoProduBtn_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(285, 427);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(211, 43);
            this.button3.TabIndex = 16;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(43, 427);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(212, 43);
            this.button1.TabIndex = 15;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(619, 37);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(301, 252);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // afterdiscount
            // 
            this.afterdiscount.Location = new System.Drawing.Point(285, 363);
            this.afterdiscount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.afterdiscount.Name = "afterdiscount";
            this.afterdiscount.Size = new System.Drawing.Size(180, 34);
            this.afterdiscount.TabIndex = 13;
            // 
            // discount
            // 
            this.discount.Location = new System.Drawing.Point(189, 313);
            this.discount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.discount.Name = "discount";
            this.discount.Size = new System.Drawing.Size(180, 34);
            this.discount.TabIndex = 11;
            // 
            // promoproduType
            // 
            this.promoproduType.Location = new System.Drawing.Point(189, 87);
            this.promoproduType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.promoproduType.Name = "promoproduType";
            this.promoproduType.Size = new System.Drawing.Size(180, 34);
            this.promoproduType.TabIndex = 9;
            // 
            // promoproduBrand
            // 
            this.promoproduBrand.Location = new System.Drawing.Point(189, 133);
            this.promoproduBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.promoproduBrand.Name = "promoproduBrand";
            this.promoproduBrand.Size = new System.Drawing.Size(180, 34);
            this.promoproduBrand.TabIndex = 8;
            // 
            // promoProduPrice
            // 
            this.promoProduPrice.Location = new System.Drawing.Point(189, 187);
            this.promoProduPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.promoProduPrice.Name = "promoProduPrice";
            this.promoProduPrice.Size = new System.Drawing.Size(180, 34);
            this.promoProduPrice.TabIndex = 7;
            this.promoProduPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.promoProduPrice_KeyPress);
            // 
            // promoproduQuantity
            // 
            this.promoproduQuantity.Location = new System.Drawing.Point(189, 236);
            this.promoproduQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.promoproduQuantity.Name = "promoproduQuantity";
            this.promoproduQuantity.Size = new System.Drawing.Size(180, 34);
            this.promoproduQuantity.TabIndex = 6;
            this.promoproduQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.promoproduQuantity_KeyPress);
            // 
            // promoproduid
            // 
            this.promoproduid.Location = new System.Drawing.Point(189, 37);
            this.promoproduid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.promoproduid.Name = "promoproduid";
            this.promoproduid.Size = new System.Drawing.Size(180, 34);
            this.promoproduid.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(37, 366);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(188, 25);
            this.label10.TabIndex = 12;
            this.label10.Text = "Price after discount: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(37, 313);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "Discount: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(37, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 25);
            this.label8.TabIndex = 4;
            this.label8.Text = "Car Quantity: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(37, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 25);
            this.label7.TabIndex = 3;
            this.label7.Text = "Car Price: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(37, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Car Brand: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 25);
            this.label5.TabIndex = 1;
            this.label5.Text = "Car Type:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(37, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Product ID: ";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnGenerate);
            this.tabPage5.Controls.Add(this.invoiceNum);
            this.tabPage5.Controls.Add(this.textBox8);
            this.tabPage5.Controls.Add(this.textBox16);
            this.tabPage5.Controls.Add(this.textBox15);
            this.tabPage5.Controls.Add(this.label40);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Controls.Add(this.btnSearch);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.btnAdd);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.dataGridView2);
            this.tabPage5.Controls.Add(this.button6);
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Location = new System.Drawing.Point(4, 42);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage5.Size = new System.Drawing.Size(1151, 510);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Invoice";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.Location = new System.Drawing.Point(413, 2);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(125, 32);
            this.btnGenerate.TabIndex = 26;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = false;
            // 
            // invoiceNum
            // 
            this.invoiceNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invoiceNum.Location = new System.Drawing.Point(205, 2);
            this.invoiceNum.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.invoiceNum.Name = "invoiceNum";
            this.invoiceNum.Size = new System.Drawing.Size(177, 30);
            this.invoiceNum.TabIndex = 25;
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(205, 112);
            this.textBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(177, 30);
            this.textBox8.TabIndex = 23;
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(205, 78);
            this.textBox16.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(177, 30);
            this.textBox16.TabIndex = 14;
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(205, 39);
            this.textBox15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(177, 30);
            this.textBox15.TabIndex = 11;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(384, 112);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(94, 25);
            this.label40.TabIndex = 24;
            this.label40.Text = "(optional)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(29, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 25);
            this.label11.TabIndex = 22;
            this.label11.Text = "Transfer#: ";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(584, 5);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(477, 212);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search Result";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(461, 181);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(117, 37);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(29, 5);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(96, 25);
            this.label30.TabIndex = 17;
            this.label30.Text = "Invoice#: ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(29, 78);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(157, 25);
            this.label28.TabIndex = 13;
            this.label28.Text = "Salesperson ID: ";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(329, 181);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(127, 37);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(29, 43);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(114, 25);
            this.label27.TabIndex = 10;
            this.label27.Text = "Product ID: ";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn9,
            this.Car_Model,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn11,
            this.Description,
            this.dataGridViewTextBoxColumn10});
            this.dataGridView2.Location = new System.Drawing.Point(5, 239);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.Size = new System.Drawing.Size(1056, 183);
            this.dataGridView2.TabIndex = 9;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(241, 428);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(167, 42);
            this.button6.TabIndex = 1;
            this.button6.Text = "Clear";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(41, 428);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(173, 43);
            this.button4.TabIndex = 0;
            this.button4.Text = "Create";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.manageBrandBtn);
            this.tabPage2.Controls.Add(this.brandPicture);
            this.tabPage2.Controls.Add(this.updateBrand);
            this.tabPage2.Controls.Add(this.refreshBtn);
            this.tabPage2.Controls.Add(this.sSearch);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.searchBrand);
            this.tabPage2.Controls.Add(this.brandTable);
            this.tabPage2.Controls.Add(this.addBrand);
            this.tabPage2.Location = new System.Drawing.Point(4, 42);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(1151, 510);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Brand";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // manageBrandBtn
            // 
            this.manageBrandBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageBrandBtn.Location = new System.Drawing.Point(5, 112);
            this.manageBrandBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.manageBrandBtn.Name = "manageBrandBtn";
            this.manageBrandBtn.Size = new System.Drawing.Size(173, 34);
            this.manageBrandBtn.TabIndex = 20;
            this.manageBrandBtn.Text = "Manage Brand";
            this.manageBrandBtn.UseVisualStyleBackColor = true;
            this.manageBrandBtn.Click += new System.EventHandler(this.manageBrandBtn_Click);
            // 
            // brandPicture
            // 
            this.brandPicture.Location = new System.Drawing.Point(604, 17);
            this.brandPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.brandPicture.Name = "brandPicture";
            this.brandPicture.Size = new System.Drawing.Size(455, 182);
            this.brandPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brandPicture.TabIndex = 19;
            this.brandPicture.TabStop = false;
            // 
            // updateBrand
            // 
            this.updateBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateBrand.Location = new System.Drawing.Point(205, 165);
            this.updateBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateBrand.Name = "updateBrand";
            this.updateBrand.Size = new System.Drawing.Size(173, 34);
            this.updateBrand.TabIndex = 18;
            this.updateBrand.Text = "Update Table";
            this.updateBrand.UseVisualStyleBackColor = true;
            this.updateBrand.Click += new System.EventHandler(this.updateBrand_Click);
            // 
            // refreshBtn
            // 
            this.refreshBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshBtn.Location = new System.Drawing.Point(5, 165);
            this.refreshBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(173, 34);
            this.refreshBtn.TabIndex = 17;
            this.refreshBtn.Text = "Refresh";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // sSearch
            // 
            this.sSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sSearch.Location = new System.Drawing.Point(115, 37);
            this.sSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sSearch.Name = "sSearch";
            this.sSearch.Size = new System.Drawing.Size(177, 30);
            this.sSearch.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 41);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 25);
            this.label12.TabIndex = 15;
            this.label12.Text = "Search:";
            // 
            // searchBrand
            // 
            this.searchBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBrand.Location = new System.Drawing.Point(315, 32);
            this.searchBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchBrand.Name = "searchBrand";
            this.searchBrand.Size = new System.Drawing.Size(173, 34);
            this.searchBrand.TabIndex = 10;
            this.searchBrand.Text = "Search";
            this.searchBrand.UseVisualStyleBackColor = true;
            this.searchBrand.Click += new System.EventHandler(this.searchProduct_Click);
            // 
            // brandTable
            // 
            this.brandTable.AllowUserToOrderColumns = true;
            this.brandTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.brandTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productId,
            this.dataGridViewTextBoxColumn4,
            this.pPicture});
            this.brandTable.Location = new System.Drawing.Point(5, 219);
            this.brandTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.brandTable.Name = "brandTable";
            this.brandTable.RowHeadersWidth = 51;
            this.brandTable.Size = new System.Drawing.Size(1053, 238);
            this.brandTable.TabIndex = 9;
            this.brandTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.productTable_CellClick);
            this.brandTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.brandTable_CellContentClick);
            // 
            // productId
            // 
            this.productId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.productId.DataPropertyName = "pId";
            this.productId.FillWeight = 20F;
            this.productId.HeaderText = "Brand ID";
            this.productId.MinimumWidth = 6;
            this.productId.Name = "productId";
            this.productId.Width = 135;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "pBrand";
            this.dataGridViewTextBoxColumn4.HeaderText = "Brand";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 106;
            // 
            // pPicture
            // 
            this.pPicture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.pPicture.DataPropertyName = "pPicture";
            this.pPicture.HeaderText = "Picture";
            this.pPicture.MinimumWidth = 6;
            this.pPicture.Name = "pPicture";
            this.pPicture.Width = 117;
            // 
            // addBrand
            // 
            this.addBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBrand.Location = new System.Drawing.Point(205, 112);
            this.addBrand.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addBrand.Name = "addBrand";
            this.addBrand.Size = new System.Drawing.Size(173, 34);
            this.addBrand.TabIndex = 2;
            this.addBrand.Text = "Add brand";
            this.addBrand.UseVisualStyleBackColor = true;
            this.addBrand.Click += new System.EventHandler(this.addBrand_Click_1);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 42);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1151, 510);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Home";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.aID);
            this.groupBox1.Controls.Add(this.aEmail);
            this.groupBox1.Controls.Add(this.aPhone);
            this.groupBox1.Controls.Add(this.aName);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(27, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(515, 261);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Admin Details:";
            // 
            // aID
            // 
            this.aID.AutoSize = true;
            this.aID.Location = new System.Drawing.Point(83, 130);
            this.aID.Name = "aID";
            this.aID.Size = new System.Drawing.Size(26, 20);
            this.aID.TabIndex = 22;
            this.aID.Text = "ID";
            // 
            // aEmail
            // 
            this.aEmail.AutoSize = true;
            this.aEmail.Location = new System.Drawing.Point(83, 101);
            this.aEmail.Name = "aEmail";
            this.aEmail.Size = new System.Drawing.Size(103, 20);
            this.aEmail.TabIndex = 21;
            this.aEmail.Text = "Admin Email";
            // 
            // aPhone
            // 
            this.aPhone.AutoSize = true;
            this.aPhone.Location = new System.Drawing.Point(83, 71);
            this.aPhone.Name = "aPhone";
            this.aPhone.Size = new System.Drawing.Size(108, 20);
            this.aPhone.TabIndex = 20;
            this.aPhone.Text = "Admin Phone";
            // 
            // aName
            // 
            this.aName.AutoSize = true;
            this.aName.Location = new System.Drawing.Point(83, 37);
            this.aName.Name = "aName";
            this.aName.Size = new System.Drawing.Size(105, 20);
            this.aName.TabIndex = 19;
            this.aName.Text = "Admin Name";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 20);
            this.label23.TabIndex = 12;
            this.label23.Text = "Name:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 130);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 20);
            this.label24.TabIndex = 18;
            this.label24.Text = "ID:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(5, 71);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(61, 20);
            this.label25.TabIndex = 13;
            this.label25.Text = "Phone:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 101);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 20);
            this.label26.TabIndex = 16;
            this.label26.Text = "Email:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabpage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.Car);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.tabControl1.ItemSize = new System.Drawing.Size(90, 38);
            this.tabControl1.Location = new System.Drawing.Point(41, 177);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1159, 556);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.updateModelBtn);
            this.tabPage10.Controls.Add(this.mPicture);
            this.tabPage10.Controls.Add(this.updmodelTable);
            this.tabPage10.Controls.Add(this.refreshModelBtn);
            this.tabPage10.Controls.Add(this.modelSearchBox);
            this.tabPage10.Controls.Add(this.label22);
            this.tabPage10.Controls.Add(this.searchModel);
            this.tabPage10.Controls.Add(this.modelTable);
            this.tabPage10.Controls.Add(this.addModel);
            this.tabPage10.Location = new System.Drawing.Point(4, 42);
            this.tabPage10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage10.Size = new System.Drawing.Size(1151, 510);
            this.tabPage10.TabIndex = 11;
            this.tabPage10.Text = "Model";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // updateModelBtn
            // 
            this.updateModelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateModelBtn.Location = new System.Drawing.Point(19, 118);
            this.updateModelBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updateModelBtn.Name = "updateModelBtn";
            this.updateModelBtn.Size = new System.Drawing.Size(173, 34);
            this.updateModelBtn.TabIndex = 35;
            this.updateModelBtn.Text = "Update Model";
            this.updateModelBtn.UseVisualStyleBackColor = true;
            this.updateModelBtn.Click += new System.EventHandler(this.updateModelBtn_Click);
            // 
            // mPicture
            // 
            this.mPicture.Location = new System.Drawing.Point(640, 23);
            this.mPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mPicture.Name = "mPicture";
            this.mPicture.Size = new System.Drawing.Size(421, 178);
            this.mPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mPicture.TabIndex = 34;
            this.mPicture.TabStop = false;
            // 
            // updmodelTable
            // 
            this.updmodelTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updmodelTable.Location = new System.Drawing.Point(197, 167);
            this.updmodelTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.updmodelTable.Name = "updmodelTable";
            this.updmodelTable.Size = new System.Drawing.Size(173, 34);
            this.updmodelTable.TabIndex = 33;
            this.updmodelTable.Text = "Update Table";
            this.updmodelTable.UseVisualStyleBackColor = true;
            this.updmodelTable.Click += new System.EventHandler(this.updmodelTable_Click);
            // 
            // refreshModelBtn
            // 
            this.refreshModelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refreshModelBtn.Location = new System.Drawing.Point(17, 167);
            this.refreshModelBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.refreshModelBtn.Name = "refreshModelBtn";
            this.refreshModelBtn.Size = new System.Drawing.Size(173, 34);
            this.refreshModelBtn.TabIndex = 32;
            this.refreshModelBtn.Text = "Refresh";
            this.refreshModelBtn.UseVisualStyleBackColor = true;
            this.refreshModelBtn.Click += new System.EventHandler(this.refreshModelBtn_Click);
            // 
            // modelSearchBox
            // 
            this.modelSearchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelSearchBox.Location = new System.Drawing.Point(113, 39);
            this.modelSearchBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.modelSearchBox.Name = "modelSearchBox";
            this.modelSearchBox.Size = new System.Drawing.Size(177, 30);
            this.modelSearchBox.TabIndex = 31;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(12, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(81, 25);
            this.label22.TabIndex = 30;
            this.label22.Text = "Search:";
            // 
            // searchModel
            // 
            this.searchModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchModel.Location = new System.Drawing.Point(315, 39);
            this.searchModel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchModel.Name = "searchModel";
            this.searchModel.Size = new System.Drawing.Size(173, 34);
            this.searchModel.TabIndex = 29;
            this.searchModel.Text = "Search";
            this.searchModel.UseVisualStyleBackColor = true;
            this.searchModel.Click += new System.EventHandler(this.searchModel_Click);
            // 
            // modelTable
            // 
            this.modelTable.AllowUserToOrderColumns = true;
            this.modelTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.modelTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.modId,
            this.modelBrand,
            this.mName,
            this.mEngineCC,
            this.mQuantity,
            this.mSeats,
            this.mDoor,
            this.mDescription,
            this.pic});
            this.modelTable.Location = new System.Drawing.Point(17, 222);
            this.modelTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.modelTable.Name = "modelTable";
            this.modelTable.RowHeadersWidth = 51;
            this.modelTable.Size = new System.Drawing.Size(1053, 238);
            this.modelTable.TabIndex = 28;
            this.modelTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.modelTable_CellContentClick);
            // 
            // modId
            // 
            this.modId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.modId.DataPropertyName = "modelId";
            this.modId.FillWeight = 20F;
            this.modId.HeaderText = "Model ID";
            this.modId.MinimumWidth = 6;
            this.modId.Name = "modId";
            this.modId.Width = 139;
            // 
            // modelBrand
            // 
            this.modelBrand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.modelBrand.DataPropertyName = "pBrand";
            this.modelBrand.HeaderText = "Brand Name";
            this.modelBrand.MinimumWidth = 6;
            this.modelBrand.Name = "modelBrand";
            this.modelBrand.Width = 177;
            // 
            // mName
            // 
            this.mName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mName.DataPropertyName = "mName";
            this.mName.HeaderText = "Model Name";
            this.mName.MinimumWidth = 6;
            this.mName.Name = "mName";
            this.mName.Width = 181;
            // 
            // mEngineCC
            // 
            this.mEngineCC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mEngineCC.DataPropertyName = "mEngineCC";
            this.mEngineCC.HeaderText = "Engine CC";
            this.mEngineCC.MinimumWidth = 6;
            this.mEngineCC.Name = "mEngineCC";
            this.mEngineCC.Width = 158;
            // 
            // mQuantity
            // 
            this.mQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mQuantity.DataPropertyName = "mQuantity";
            this.mQuantity.HeaderText = "Quantity";
            this.mQuantity.MinimumWidth = 6;
            this.mQuantity.Name = "mQuantity";
            this.mQuantity.Width = 129;
            // 
            // mSeats
            // 
            this.mSeats.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mSeats.DataPropertyName = "mSeats";
            this.mSeats.HeaderText = "Seat Capacity";
            this.mSeats.MinimumWidth = 6;
            this.mSeats.Name = "mSeats";
            this.mSeats.Width = 189;
            // 
            // mDoor
            // 
            this.mDoor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mDoor.DataPropertyName = "mDoor";
            this.mDoor.HeaderText = "No of Door";
            this.mDoor.MinimumWidth = 6;
            this.mDoor.Name = "mDoor";
            this.mDoor.Width = 159;
            // 
            // mDescription
            // 
            this.mDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mDescription.DataPropertyName = "mDescription";
            this.mDescription.HeaderText = "Description";
            this.mDescription.MinimumWidth = 6;
            this.mDescription.Name = "mDescription";
            this.mDescription.Width = 164;
            // 
            // pic
            // 
            this.pic.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.pic.DataPropertyName = "mPicture";
            this.pic.HeaderText = "Picture";
            this.pic.MinimumWidth = 6;
            this.pic.Name = "pic";
            this.pic.Width = 117;
            // 
            // addModel
            // 
            this.addModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addModel.Location = new System.Drawing.Point(197, 118);
            this.addModel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addModel.Name = "addModel";
            this.addModel.Size = new System.Drawing.Size(173, 34);
            this.addModel.TabIndex = 27;
            this.addModel.Text = "Add Model";
            this.addModel.UseVisualStyleBackColor = true;
            this.addModel.Click += new System.EventHandler(this.addModel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(59, 54);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(187, 71);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(182, 46);
            this.label29.TabIndex = 26;
            this.label29.Text = "AutoMart";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "productid";
            this.dataGridViewTextBoxColumn1.FillWeight = 20F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Car ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // carId
            // 
            this.carId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.carId.DataPropertyName = "carId";
            this.carId.HeaderText = "Car ID";
            this.carId.MinimumWidth = 6;
            this.carId.Name = "carId";
            this.carId.Width = 125;
            // 
            // pBrand
            // 
            this.pBrand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.pBrand.DataPropertyName = "pBrand";
            this.pBrand.HeaderText = "Brand Name";
            this.pBrand.MinimumWidth = 6;
            this.pBrand.Name = "pBrand";
            this.pBrand.Width = 125;
            // 
            // custID
            // 
            this.custID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custID.DataPropertyName = "custID";
            this.custID.HeaderText = "Customer ID";
            this.custID.MinimumWidth = 6;
            this.custID.Name = "custID";
            this.custID.Width = 175;
            // 
            // custIC
            // 
            this.custIC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custIC.DataPropertyName = "custIC";
            this.custIC.HeaderText = "IC";
            this.custIC.MinimumWidth = 6;
            this.custIC.Name = "custIC";
            this.custIC.Width = 65;
            // 
            // custName
            // 
            this.custName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custName.DataPropertyName = "custName";
            this.custName.HeaderText = "Name";
            this.custName.MinimumWidth = 6;
            this.custName.Name = "custName";
            this.custName.Width = 107;
            // 
            // custAdd
            // 
            this.custAdd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custAdd.DataPropertyName = "custAdd";
            this.custAdd.HeaderText = "Address";
            this.custAdd.MinimumWidth = 6;
            this.custAdd.Name = "custAdd";
            this.custAdd.Width = 131;
            // 
            // custPhone
            // 
            this.custPhone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custPhone.DataPropertyName = "custPhone";
            this.custPhone.HeaderText = "Phone";
            this.custPhone.MinimumWidth = 6;
            this.custPhone.Name = "custPhone";
            this.custPhone.Width = 112;
            // 
            // custGender
            // 
            this.custGender.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custGender.DataPropertyName = "custGender";
            this.custGender.HeaderText = "Gender";
            this.custGender.MinimumWidth = 6;
            this.custGender.Name = "custGender";
            this.custGender.Width = 123;
            // 
            // custAge
            // 
            this.custAge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.custAge.DataPropertyName = "custAge";
            this.custAge.HeaderText = "Age";
            this.custAge.MinimumWidth = 6;
            this.custAge.Name = "custAge";
            this.custAge.Width = 85;
            // 
            // cstatus
            // 
            this.cstatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cstatus.DataPropertyName = "status";
            this.cstatus.HeaderText = "Status";
            this.cstatus.MinimumWidth = 6;
            this.cstatus.Name = "cstatus";
            this.cstatus.Width = 108;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Number";
            this.dataGridViewTextBoxColumn2.FillWeight = 20F;
            this.dataGridViewTextBoxColumn2.HeaderText = "No";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 74;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Brand";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 125;
            // 
            // Car_Model
            // 
            this.Car_Model.HeaderText = "Car Model";
            this.Car_Model.MinimumWidth = 6;
            this.Car_Model.Name = "Car_Model";
            this.Car_Model.Width = 250;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "cartype";
            this.dataGridViewTextBoxColumn8.HeaderText = "Car type";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 131;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "carYear";
            this.dataGridViewTextBoxColumn11.HeaderText = "Car Year";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 137;
            // 
            // Description
            // 
            this.Description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Description.HeaderText = "Description";
            this.Description.MinimumWidth = 6;
            this.Description.Name = "Description";
            this.Description.Width = 164;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "carPrice";
            this.dataGridViewTextBoxColumn10.HeaderText = "Price";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 98;
            // 
            // CarDataTable
            // 
            this.CarDataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CarDataTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cartableid,
            this.modelName,
            this.cartablebrand,
            this.carType,
            this.cColour,
            this.carBranch,
            this.carplateno,
            this.ManufactureYear,
            this.mileage,
            this.quantity,
            this.cdescription,
            this.cPrice,
            this.cPicture,
            this.status});
            this.CarDataTable.Location = new System.Drawing.Point(6, 212);
            this.CarDataTable.Name = "CarDataTable";
            this.CarDataTable.RowHeadersWidth = 51;
            this.CarDataTable.RowTemplate.Height = 24;
            this.CarDataTable.Size = new System.Drawing.Size(1142, 293);
            this.CarDataTable.TabIndex = 28;
            this.CarDataTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CarDataTable_CellContentClick_1);
            // 
            // cartableid
            // 
            this.cartableid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cartableid.DataPropertyName = "carId";
            this.cartableid.HeaderText = "Car ID";
            this.cartableid.MinimumWidth = 6;
            this.cartableid.Name = "cartableid";
            this.cartableid.Width = 109;
            // 
            // modelName
            // 
            this.modelName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.modelName.DataPropertyName = "mName";
            this.modelName.HeaderText = "Model Name";
            this.modelName.MinimumWidth = 6;
            this.modelName.Name = "modelName";
            this.modelName.Width = 181;
            // 
            // cartablebrand
            // 
            this.cartablebrand.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cartablebrand.DataPropertyName = "pBrand";
            this.cartablebrand.HeaderText = "Brand";
            this.cartablebrand.MinimumWidth = 6;
            this.cartablebrand.Name = "cartablebrand";
            this.cartablebrand.Width = 106;
            // 
            // carType
            // 
            this.carType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.carType.DataPropertyName = "cType";
            this.carType.HeaderText = "Car Type";
            this.carType.MinimumWidth = 6;
            this.carType.Name = "carType";
            this.carType.Width = 141;
            // 
            // cColour
            // 
            this.cColour.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cColour.DataPropertyName = "cColour";
            this.cColour.HeaderText = "Car Colour";
            this.cColour.MinimumWidth = 6;
            this.cColour.Name = "cColour";
            this.cColour.Width = 158;
            // 
            // carBranch
            // 
            this.carBranch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.carBranch.DataPropertyName = "branchName";
            this.carBranch.HeaderText = "Branch ";
            this.carBranch.MinimumWidth = 6;
            this.carBranch.Name = "carBranch";
            this.carBranch.Width = 123;
            // 
            // carplateno
            // 
            this.carplateno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.carplateno.DataPropertyName = "cCarPlateNo";
            this.carplateno.HeaderText = "Car Plate No";
            this.carplateno.MinimumWidth = 6;
            this.carplateno.Name = "carplateno";
            this.carplateno.Width = 179;
            // 
            // ManufactureYear
            // 
            this.ManufactureYear.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ManufactureYear.DataPropertyName = "cManufactureYear";
            this.ManufactureYear.HeaderText = "Production Year";
            this.ManufactureYear.MinimumWidth = 6;
            this.ManufactureYear.Name = "ManufactureYear";
            this.ManufactureYear.Width = 196;
            // 
            // mileage
            // 
            this.mileage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.mileage.DataPropertyName = "cMileage";
            this.mileage.HeaderText = "Mileage";
            this.mileage.MinimumWidth = 6;
            this.mileage.Name = "mileage";
            this.mileage.Width = 129;
            // 
            // quantity
            // 
            this.quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.quantity.DataPropertyName = "quantity";
            this.quantity.HeaderText = "Quantity";
            this.quantity.MinimumWidth = 6;
            this.quantity.Name = "quantity";
            this.quantity.Width = 129;
            // 
            // cdescription
            // 
            this.cdescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cdescription.DataPropertyName = "cDescription";
            this.cdescription.HeaderText = "Description";
            this.cdescription.MinimumWidth = 6;
            this.cdescription.Name = "cdescription";
            this.cdescription.Width = 164;
            // 
            // cPrice
            // 
            this.cPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cPrice.DataPropertyName = "cPrice";
            this.cPrice.HeaderText = "Price";
            this.cPrice.MinimumWidth = 6;
            this.cPrice.Name = "cPrice";
            this.cPrice.Width = 98;
            // 
            // cPicture
            // 
            this.cPicture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cPicture.DataPropertyName = "cPicture";
            this.cPicture.HeaderText = "Picture";
            this.cPicture.MinimumWidth = 6;
            this.cPicture.Name = "cPicture";
            this.cPicture.Width = 117;
            // 
            // status
            // 
            this.status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "Status";
            this.status.MinimumWidth = 6;
            this.status.Name = "status";
            this.status.Width = 108;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 790);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Admin";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Admin_Load);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.acPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accTable)).EndInit();
            this.Car.ResumeLayout(false);
            this.Car.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.carPicture)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customerTable)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transferTable)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabpage6.ResumeLayout(false);
            this.tabpage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.brandPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brandTable)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CarDataTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.PictureBox acPicture;
        private System.Windows.Forms.Button updateAcc;
        private System.Windows.Forms.Button refreshAccBtn;
        private System.Windows.Forms.TextBox accSearchBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button searchAcc;
        private System.Windows.Forms.DataGridView accTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn accId;
        private System.Windows.Forms.DataGridViewTextBoxColumn accName;
        private System.Windows.Forms.DataGridViewTextBoxColumn accPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn accQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn accPicture;
        private System.Windows.Forms.Button addAcc;
        private System.Windows.Forms.TabPage Car;
        private System.Windows.Forms.PictureBox carPicture;
        private System.Windows.Forms.Button updateCarTable;
        private System.Windows.Forms.Button carRefreshBtn;
        private System.Windows.Forms.TextBox carSearchBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button carSearch;
        private System.Windows.Forms.Button addCar;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button searchCustomer;
        private System.Windows.Forms.DataGridView customerTable;
        private System.Windows.Forms.Button updateCustTable;
        private System.Windows.Forms.Button cusRefresh;
        private System.Windows.Forms.TextBox searchDetails;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button addCustomer;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button updateTransferTable;
        private System.Windows.Forms.Button refreshTransferBtn;
        private System.Windows.Forms.TextBox searchTransfer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button searchTransferBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transferid;
        private System.Windows.Forms.Button createTransfer;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button Add_productbtn;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabpage6;
        private System.Windows.Forms.Button sPromoProduBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox afterdiscount;
        private System.Windows.Forms.TextBox discount;
        private System.Windows.Forms.TextBox promoproduType;
        private System.Windows.Forms.TextBox promoproduBrand;
        private System.Windows.Forms.TextBox promoProduPrice;
        private System.Windows.Forms.TextBox promoproduQuantity;
        private System.Windows.Forms.TextBox promoproduid;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox invoiceNum;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox brandPicture;
        private System.Windows.Forms.Button updateBrand;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.TextBox sSearch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button searchBrand;
        private System.Windows.Forms.DataGridView brandTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn productId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn pPicture;
        private System.Windows.Forms.Button addBrand;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label aID;
        private System.Windows.Forms.Label aEmail;
        private System.Windows.Forms.Label aPhone;
        private System.Windows.Forms.Label aName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.PictureBox mPicture;
        private System.Windows.Forms.Button updmodelTable;
        private System.Windows.Forms.Button refreshModelBtn;
        private System.Windows.Forms.TextBox modelSearchBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button searchModel;
        private System.Windows.Forms.DataGridView modelTable;
        private System.Windows.Forms.Button addModel;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button manageBrandBtn;
        private System.Windows.Forms.Button manageCustBtn;
        private System.Windows.Forms.Button manageCarBtn;
        private System.Windows.Forms.Button updateAccBtn;
        private System.Windows.Forms.Button updateModelBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn modId;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn mName;
        private System.Windows.Forms.DataGridViewTextBoxColumn mEngineCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn mQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn mSeats;
        private System.Windows.Forms.DataGridViewTextBoxColumn mDoor;
        private System.Windows.Forms.DataGridViewTextBoxColumn mDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn pic;
        private System.Windows.Forms.DataGridViewTextBoxColumn carId;
        private System.Windows.Forms.DataGridViewTextBoxColumn pBrand;
        private System.Windows.Forms.DataGridView transferTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn transfercarid;
        private System.Windows.Forms.DataGridViewTextBoxColumn transfersID;
        private System.Windows.Forms.DataGridViewTextBoxColumn transferfrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn transferto;
        private System.Windows.Forms.DataGridViewTextBoxColumn tquantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn transferDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn custID;
        private System.Windows.Forms.DataGridViewTextBoxColumn custIC;
        private System.Windows.Forms.DataGridViewTextBoxColumn custName;
        private System.Windows.Forms.DataGridViewTextBoxColumn custAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn custPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn custGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn custAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn cstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Car_Model;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridView CarDataTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn cartableid;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cartablebrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn carType;
        private System.Windows.Forms.DataGridViewTextBoxColumn cColour;
        private System.Windows.Forms.DataGridViewTextBoxColumn carBranch;
        private System.Windows.Forms.DataGridViewTextBoxColumn carplateno;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManufactureYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn mileage;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPicture;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
    }
}