﻿namespace AutoMartSystem
{
    partial class Manager_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Manager_main));
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.mID = new System.Windows.Forms.Label();
            this.mName = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mEmail = new System.Windows.Forms.Label();
            this.mPhone = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SalesRecord = new System.Windows.Forms.RadioButton();
            this.salesReport = new System.Windows.Forms.RadioButton();
            this.empDetails = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.eTable = new System.Windows.Forms.DataGridView();
            this.eId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eIc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ePhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eAge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eEmailAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datejoined = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.searchBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.updateTable = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.staffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1012, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "Logout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.mID);
            this.groupBox1.Controls.Add(this.mName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.mEmail);
            this.groupBox1.Controls.Add(this.mPhone);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(33, 42);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(386, 213);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Manager Details:";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 30);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "Name:";
            // 
            // mID
            // 
            this.mID.AutoSize = true;
            this.mID.Location = new System.Drawing.Point(88, 106);
            this.mID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mID.Name = "mID";
            this.mID.Size = new System.Drawing.Size(60, 24);
            this.mID.TabIndex = 19;
            this.mID.Text = "label4";
            // 
            // mName
            // 
            this.mName.AutoSize = true;
            this.mName.Location = new System.Drawing.Point(91, 30);
            this.mName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mName.Name = "mName";
            this.mName.Size = new System.Drawing.Size(60, 24);
            this.mName.TabIndex = 14;
            this.mName.Text = "label4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 106);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 24);
            this.label7.TabIndex = 18;
            this.label7.Text = "ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 58);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 24);
            this.label8.TabIndex = 13;
            this.label8.Text = "Phone:";
            // 
            // mEmail
            // 
            this.mEmail.AutoSize = true;
            this.mEmail.Location = new System.Drawing.Point(88, 82);
            this.mEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mEmail.Name = "mEmail";
            this.mEmail.Size = new System.Drawing.Size(60, 24);
            this.mEmail.TabIndex = 17;
            this.mEmail.Text = "label4";
            // 
            // mPhone
            // 
            this.mPhone.AutoSize = true;
            this.mPhone.Location = new System.Drawing.Point(91, 57);
            this.mPhone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mPhone.Name = "mPhone";
            this.mPhone.Size = new System.Drawing.Size(60, 24);
            this.mPhone.TabIndex = 15;
            this.mPhone.Text = "label4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 82);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 24);
            this.label9.TabIndex = 16;
            this.label9.Text = "Email:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(29, 106);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1074, 605);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(1066, 572);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Home";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(41, 310);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 31);
            this.textBox1.TabIndex = 22;
            this.textBox1.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SalesRecord);
            this.tabPage2.Controls.Add(this.salesReport);
            this.tabPage2.Controls.Add(this.empDetails);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.dateTimePicker2);
            this.tabPage2.Controls.Add(this.dateTimePicker1);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(1066, 572);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Report";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SalesRecord
            // 
            this.SalesRecord.AutoSize = true;
            this.SalesRecord.Location = new System.Drawing.Point(67, 205);
            this.SalesRecord.Name = "SalesRecord";
            this.SalesRecord.Size = new System.Drawing.Size(152, 24);
            this.SalesRecord.TabIndex = 9;
            this.SalesRecord.TabStop = true;
            this.SalesRecord.Text = "Car Sales Record";
            this.SalesRecord.UseVisualStyleBackColor = true;
            this.SalesRecord.CheckedChanged += new System.EventHandler(this.SalesRecord_CheckedChanged);
            // 
            // salesReport
            // 
            this.salesReport.AutoSize = true;
            this.salesReport.Location = new System.Drawing.Point(67, 149);
            this.salesReport.Name = "salesReport";
            this.salesReport.Size = new System.Drawing.Size(149, 24);
            this.salesReport.TabIndex = 8;
            this.salesReport.TabStop = true;
            this.salesReport.Text = "Car Sales Report";
            this.salesReport.UseVisualStyleBackColor = true;
            this.salesReport.CheckedChanged += new System.EventHandler(this.salesReport_CheckedChanged);
            // 
            // empDetails
            // 
            this.empDetails.AutoSize = true;
            this.empDetails.Location = new System.Drawing.Point(67, 101);
            this.empDetails.Name = "empDetails";
            this.empDetails.Size = new System.Drawing.Size(150, 24);
            this.empDetails.TabIndex = 7;
            this.empDetails.TabStop = true;
            this.empDetails.Text = "Employee Details";
            this.empDetails.UseVisualStyleBackColor = true;
            this.empDetails.CheckedChanged += new System.EventHandler(this.empDetails_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(67, 267);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(183, 29);
            this.button2.TabIndex = 6;
            this.button2.Text = "Generate Report";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(227, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "to";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(286, 36);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(128, 26);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(67, 37);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 26);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.eTable);
            this.tabPage3.Controls.Add(this.searchBtn);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.searchBox);
            this.tabPage3.Controls.Add(this.updateTable);
            this.tabPage3.Controls.Add(this.clear);
            this.tabPage3.Controls.Add(this.add);
            this.tabPage3.Controls.Add(this.update);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(1066, 572);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Employee Tracking";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // eTable
            // 
            this.eTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.eTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.eId,
            this.eName,
            this.eIc,
            this.ePhone,
            this.eAge,
            this.eGender,
            this.eEmailAdd,
            this.datejoined,
            this.salary,
            this.staffstatus,
            this.roles,
            this.password});
            this.eTable.Location = new System.Drawing.Point(2, 228);
            this.eTable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.eTable.Name = "eTable";
            this.eTable.RowHeadersWidth = 51;
            this.eTable.RowTemplate.Height = 24;
            this.eTable.Size = new System.Drawing.Size(1060, 340);
            this.eTable.TabIndex = 35;
            this.eTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.eTable_CellContentClick);
            // 
            // eId
            // 
            this.eId.DataPropertyName = "staffID";
            this.eId.HeaderText = "Staff ID";
            this.eId.MinimumWidth = 6;
            this.eId.Name = "eId";
            this.eId.Width = 125;
            // 
            // eName
            // 
            this.eName.DataPropertyName = "staffName";
            this.eName.HeaderText = "Staff Name";
            this.eName.MinimumWidth = 6;
            this.eName.Name = "eName";
            this.eName.Width = 125;
            // 
            // eIc
            // 
            this.eIc.DataPropertyName = "staffIC";
            this.eIc.HeaderText = "Staff IC";
            this.eIc.MinimumWidth = 6;
            this.eIc.Name = "eIc";
            this.eIc.Width = 125;
            // 
            // ePhone
            // 
            this.ePhone.DataPropertyName = "staffPhone";
            this.ePhone.HeaderText = "Staff Phone";
            this.ePhone.MinimumWidth = 6;
            this.ePhone.Name = "ePhone";
            this.ePhone.Width = 125;
            // 
            // eAge
            // 
            this.eAge.DataPropertyName = "staffAge";
            this.eAge.HeaderText = "Staff Age";
            this.eAge.MinimumWidth = 6;
            this.eAge.Name = "eAge";
            this.eAge.Width = 125;
            // 
            // eGender
            // 
            this.eGender.DataPropertyName = "staffGender";
            this.eGender.HeaderText = "Staff Gender";
            this.eGender.MinimumWidth = 6;
            this.eGender.Name = "eGender";
            this.eGender.Width = 125;
            // 
            // eEmailAdd
            // 
            this.eEmailAdd.DataPropertyName = "staffEmailAdd";
            this.eEmailAdd.HeaderText = "Staff Email Address";
            this.eEmailAdd.MinimumWidth = 6;
            this.eEmailAdd.Name = "eEmailAdd";
            this.eEmailAdd.Width = 125;
            // 
            // datejoined
            // 
            this.datejoined.DataPropertyName = "datejoined";
            this.datejoined.HeaderText = "Date Joined";
            this.datejoined.MinimumWidth = 6;
            this.datejoined.Name = "datejoined";
            this.datejoined.Width = 125;
            // 
            // salary
            // 
            this.salary.DataPropertyName = "salary";
            this.salary.HeaderText = "salary";
            this.salary.MinimumWidth = 6;
            this.salary.Name = "salary";
            this.salary.Width = 125;
            // 
            // staffstatus
            // 
            this.staffstatus.DataPropertyName = "staffstatus";
            this.staffstatus.HeaderText = "Staff Status";
            this.staffstatus.MinimumWidth = 6;
            this.staffstatus.Name = "staffstatus";
            this.staffstatus.Width = 125;
            // 
            // roles
            // 
            this.roles.DataPropertyName = "roles";
            this.roles.HeaderText = "roles";
            this.roles.MinimumWidth = 6;
            this.roles.Name = "roles";
            this.roles.Width = 125;
            // 
            // password
            // 
            this.password.DataPropertyName = "password";
            this.password.HeaderText = "password";
            this.password.MinimumWidth = 6;
            this.password.Name = "password";
            this.password.Width = 125;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(657, 41);
            this.searchBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(104, 29);
            this.searchBtn.TabIndex = 33;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(360, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 32;
            this.label1.Text = "Search:";
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(425, 46);
            this.searchBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(199, 26);
            this.searchBox.TabIndex = 31;
            // 
            // updateTable
            // 
            this.updateTable.Location = new System.Drawing.Point(433, 186);
            this.updateTable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updateTable.Name = "updateTable";
            this.updateTable.Size = new System.Drawing.Size(147, 27);
            this.updateTable.TabIndex = 30;
            this.updateTable.Text = "Update Table";
            this.updateTable.UseVisualStyleBackColor = true;
            this.updateTable.Click += new System.EventHandler(this.updateTable_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(874, 186);
            this.clear.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(147, 27);
            this.clear.TabIndex = 4;
            this.clear.Text = "Refresh";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(584, 186);
            this.add.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(133, 27);
            this.add.TabIndex = 2;
            this.add.Text = "Add Employee";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(728, 186);
            this.update.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(134, 27);
            this.update.TabIndex = 1;
            this.update.Text = "Update Record";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(942, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(173, 42);
            this.label17.TabIndex = 23;
            this.label17.Text = "Manager";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(128, 32);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(148, 37);
            this.label18.TabIndex = 24;
            this.label18.Text = "AutoMart";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(32, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(82, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // staffID
            // 
            this.staffID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.staffID.DataPropertyName = "staffID";
            this.staffID.FillWeight = 20F;
            this.staffID.HeaderText = "No";
            this.staffID.MinimumWidth = 6;
            this.staffID.Name = "staffID";
            // 
            // Name
            // 
            this.Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Name.DataPropertyName = "staffName";
            this.Name.HeaderText = "Name";
            this.Name.MinimumWidth = 6;
            this.Name.Name = "Name";
            // 
            // Manager_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 746);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button1);
          //  this.Name = "Manager_main";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label mID;
        private System.Windows.Forms.Label mName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label mEmail;
        private System.Windows.Forms.Label mPhone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RadioButton SalesRecord;
        private System.Windows.Forms.RadioButton salesReport;
        private System.Windows.Forms.RadioButton empDetails;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button updateTable;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView eTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn eId;
        private System.Windows.Forms.DataGridViewTextBoxColumn eName;
        private System.Windows.Forms.DataGridViewTextBoxColumn eIc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ePhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn eAge;
        private System.Windows.Forms.DataGridViewTextBoxColumn eGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn eEmailAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn datejoined;
        private System.Windows.Forms.DataGridViewTextBoxColumn salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn roles;
        private System.Windows.Forms.DataGridViewTextBoxColumn password;
    }
}