﻿namespace AutoMartSystem
{
    partial class ManageCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.passwordBtn = new System.Windows.Forms.CheckBox();
            this.addPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.addAddress = new System.Windows.Forms.RichTextBox();
            this.addName = new System.Windows.Forms.TextBox();
            this.addIc = new System.Windows.Forms.TextBox();
            this.addGender = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.addId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addAge = new System.Windows.Forms.TextBox();
            this.addPhone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.addCustomerBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // passwordBtn
            // 
            this.passwordBtn.AutoSize = true;
            this.passwordBtn.Location = new System.Drawing.Point(654, 408);
            this.passwordBtn.Name = "passwordBtn";
            this.passwordBtn.Size = new System.Drawing.Size(129, 21);
            this.passwordBtn.TabIndex = 118;
            this.passwordBtn.Text = "Show Password";
            this.passwordBtn.UseVisualStyleBackColor = true;
            this.passwordBtn.CheckedChanged += new System.EventHandler(this.passwordBtn_CheckedChanged);
            // 
            // addPassword
            // 
            this.addPassword.Location = new System.Drawing.Point(293, 406);
            this.addPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addPassword.Name = "addPassword";
            this.addPassword.Size = new System.Drawing.Size(284, 22);
            this.addPassword.TabIndex = 117;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(108, 408);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 20);
            this.label7.TabIndex = 116;
            this.label7.Text = "Password: ";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(246, 267);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(37, 22);
            this.textBox1.TabIndex = 115;
            this.textBox1.Text = "+60";
            // 
            // addAddress
            // 
            this.addAddress.Location = new System.Drawing.Point(289, 168);
            this.addAddress.Name = "addAddress";
            this.addAddress.Size = new System.Drawing.Size(248, 73);
            this.addAddress.TabIndex = 114;
            this.addAddress.Text = "";
            // 
            // addName
            // 
            this.addName.Location = new System.Drawing.Point(289, 111);
            this.addName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addName.Name = "addName";
            this.addName.Size = new System.Drawing.Size(196, 22);
            this.addName.TabIndex = 113;
            // 
            // addIc
            // 
            this.addIc.Location = new System.Drawing.Point(293, 58);
            this.addIc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addIc.Name = "addIc";
            this.addIc.Size = new System.Drawing.Size(196, 22);
            this.addIc.TabIndex = 112;
            this.addIc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addIc_KeyPress);
            // 
            // addGender
            // 
            this.addGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addGender.FormattingEnabled = true;
            this.addGender.Items.AddRange(new object[] {
            "Male\t",
            "Female"});
            this.addGender.Location = new System.Drawing.Point(292, 315);
            this.addGender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addGender.Name = "addGender";
            this.addGender.Size = new System.Drawing.Size(196, 28);
            this.addGender.TabIndex = 111;
            this.addGender.Text = "-Select Gender-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(108, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 20);
            this.label6.TabIndex = 110;
            this.label6.Text = "Customer IC:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(107, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 109;
            this.label5.Text = "Gender: ";
            // 
            // addId
            // 
            this.addId.Enabled = false;
            this.addId.Location = new System.Drawing.Point(293, 9);
            this.addId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addId.Name = "addId";
            this.addId.Size = new System.Drawing.Size(118, 22);
            this.addId.TabIndex = 108;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(107, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 20);
            this.label2.TabIndex = 107;
            this.label2.Text = "Customer Name: ";
            // 
            // addAge
            // 
            this.addAge.Location = new System.Drawing.Point(293, 362);
            this.addAge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addAge.Name = "addAge";
            this.addAge.Size = new System.Drawing.Size(196, 22);
            this.addAge.TabIndex = 106;
            this.addAge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addAge_KeyPress);
            // 
            // addPhone
            // 
            this.addPhone.Location = new System.Drawing.Point(289, 267);
            this.addPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addPhone.Name = "addPhone";
            this.addPhone.Size = new System.Drawing.Size(196, 22);
            this.addPhone.TabIndex = 105;
            this.addPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.addPhone_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(107, 267);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 20);
            this.label11.TabIndex = 104;
            this.label11.Text = "Phone Number: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(108, 364);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 20);
            this.label4.TabIndex = 103;
            this.label4.Text = "Age: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(108, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 20);
            this.label3.TabIndex = 102;
            this.label3.Text = "Customer Address: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(108, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 101;
            this.label1.Text = "Customer ID: ";
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(513, 449);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(172, 31);
            this.exitBtn.TabIndex = 100;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(292, 449);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(172, 31);
            this.clearbtn.TabIndex = 99;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // addCustomerBtn
            // 
            this.addCustomerBtn.Location = new System.Drawing.Point(94, 449);
            this.addCustomerBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addCustomerBtn.Name = "addCustomerBtn";
            this.addCustomerBtn.Size = new System.Drawing.Size(172, 31);
            this.addCustomerBtn.TabIndex = 98;
            this.addCustomerBtn.Text = "Confirm";
            this.addCustomerBtn.UseVisualStyleBackColor = true;
            this.addCustomerBtn.Click += new System.EventHandler(this.addCustomerBtn_Click);
            // 
            // ManageCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 498);
            this.Controls.Add(this.passwordBtn);
            this.Controls.Add(this.addPassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.addAddress);
            this.Controls.Add(this.addName);
            this.Controls.Add(this.addIc);
            this.Controls.Add(this.addGender);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.addId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.addAge);
            this.Controls.Add(this.addPhone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.addCustomerBtn);
            this.Name = "ManageCustomer";
            this.Text = "ManageCustomer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox passwordBtn;
        private System.Windows.Forms.TextBox addPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox addAddress;
        private System.Windows.Forms.TextBox addName;
        private System.Windows.Forms.TextBox addIc;
        private System.Windows.Forms.ComboBox addGender;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox addId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox addAge;
        private System.Windows.Forms.TextBox addPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button addCustomerBtn;
    }
}