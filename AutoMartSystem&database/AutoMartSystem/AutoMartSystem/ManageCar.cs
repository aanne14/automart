﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class ManageCar : Form
    {

        string correctFilename,pictureurl;
        bool checkchangepicture = false;
        public ManageCar(string managecarid)
        {
            InitializeComponent();
            this.carId.Text = managecarid;
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT * FROM cartable INNER JOIN branchdetails ON cLocation = branchId";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while (MyReader.Read())
                {
                    if (this.carId.Text == MyReader.GetString("carId"))
                    {
                        this.modelId.Text = MyReader.GetString("mId");
                        this.cType.Text = MyReader.GetString("cType");
                        this.cColour.Text = MyReader.GetString("cColour");
                        this.cLocation.Text = MyReader.GetString("branchName");
                        this.cCarPlateNo.Text = MyReader.GetString("cCarPlateNo");
                        this.cProductionYear.Text = MyReader.GetString("cManufactureYear");
                        this.cMileage.Text = MyReader.GetString("cMileage");
                        this.quantity.Text = MyReader.GetString("quantity");
                        this.cDescription.Text = MyReader.GetString("cDescription");
                        this.cPrice.Text = MyReader.GetString("cPrice");
                        this.status.Text = MyReader.GetString("status");
                        pictureurl = MyReader.GetString("cPicture");
                        ///show picture in update car page
                        string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                        cPicture.Image = Image.FromFile(paths + "\\pictures\\" + MyReader.GetString("cPicture"));
                    }

                }
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Add_Carbtn_Click(object sender, EventArgs e)
        {
         
            if (searchProduct.BackColor == Color.Green)
            {
                if (cLocation.Text == "Car Dealership Subang Jaya")
                {
                    cLocation.Text = "B2";
                }
                else if (cLocation.Text == "Car Dealership Glenmarie")
                {
                    cLocation.Text = "B1";
                }
                else
                {
                    cLocation.Text = "B3";
                }

                
                try
                {

                    //to connect to phpmyadmin database name cardealership
                    string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";
                    string Query;
                    if (checkchangepicture == true)
                    {
                        //insert from the table of car details
                        Query = "UPDATE cartable set mId= '" + this.modelId.Text + "',cType='" + this.cType.Text + "',cColour='" + this.cColour.Text + "',cLocation='" + this.cLocation.Text +
                            "',cCarPlateNo='" + this.cCarPlateNo.Text + "',cManufactureYear='" + int.Parse(this.cProductionYear.Text) + "',cMileage='" + int.Parse(this.cMileage.Text) + "',quantity='" + int.Parse(this.quantity.Text)
                            + "',cDescription='" + this.cDescription.Text + "',cPrice='" + this.cPrice.Text + "',cPicture='" + correctFilename + "',status='" + this.status.Text + "' where carId='" + this.carId.Text + "';";

                    }
                    else
                    {
                        //insert from the table of car details
                        Query = "UPDATE cartable set mId= '" + this.modelId.Text + "',cType='" + this.cType.Text + "',cColour='" + this.cColour.Text + "',cLocation='" + this.cLocation.Text +
                            "',cCarPlateNo='" + this.cCarPlateNo.Text + "',cManufactureYear='" + int.Parse(this.cProductionYear.Text) + "',cMileage='" + int.Parse(this.cMileage.Text) + "',quantity='" + int.Parse(this.quantity.Text)
                            + "',cDescription='" + this.cDescription.Text + "',cPrice='" + this.cPrice.Text + "',cPicture='" + pictureurl + "',status='" + this.status.Text + "' where carId='" + this.carId.Text + "';";

                    }
                    MySqlConnection MyConn = new MySqlConnection(Conn);
                    MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MessageBox.Show("Record Updated", "Update Record");
                    MyConn.Close();
                  
                    this.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

               
            }
            else
            {
                MessageBox.Show("Please check the search box for checking process", "Error");
            }

        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            modelId.Text = "";
            cType.Text = "";
            cColour.Text = "";
            cLocation.Text = "";
            cCarPlateNo.Text = "";
            cProductionYear.Text = "";
            cMileage.Text = "";
            quantity.Text = "";
            cDescription.Text = "";
            cPrice.Text = "";
            cPicture.Image = null;
            searchProduct.BackColor = Color.Gray;
            status.Text = "";
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            checkchangepicture = true;
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    cPicture.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void cProductionYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void cMileage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void cPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void searchProduct_Click(object sender, EventArgs e)
        {
            //search for branch name and status from the pid entered
            try
            {
                string checkproductid;
                bool checkproduct = false;
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string query = "SELECT * FROM modeldetails WHERE modelId='" + this.modelId.Text + "';";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while(MyReader.Read())
                {
                    checkproductid = MyReader.GetString("modelId");
                  
                    if (checkproductid == modelId.Text)
                    {
                        checkproduct = true;
                        MessageBox.Show(MyReader.GetString("mName"), "Model Name Selected");
                    }

                }
                MyConn.Close();

                //check for product id existence
                if (checkproduct == true)
                {
                    searchProduct.BackColor = Color.Green;
                }
                else
                {
                    searchProduct.BackColor = Color.Red;
                    MessageBox.Show("Model ID doesn't exist. ", "Please try again.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
