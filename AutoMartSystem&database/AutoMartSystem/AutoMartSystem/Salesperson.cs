﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class Salesperson : Form
    {
        
        public Salesperson(string name, string email, string phone, string staffid)
        {
            InitializeComponent();
            ///display name, email, phone, staffid
            sName.Text = name;
            sEmail.Text = email;
            sPhone.Text = phone;
            sID.Text = staffid;

            try
            {
                /* carId,mEngineCC,cType,branchName,cColour,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription" +
                    ",cPrice,cPicture,status,pBrand,mName,status*/
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                ///display car details in datagridview
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT carId,mEngineCC,branchName,cType,cColour,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription" +
                    ",cPrice,cPicture,status,pBrand,mName FROM cartable INNER JOIN branchdetails ON cLocation = branchId INNER JOIN modeldetails ON mId=modelId " +
                    "INNER JOIN producttable ON pId = brandId ", MyConn);
                DataTable table = new DataTable();

                sqldata.Fill(table);

                productTable.DataSource = table;
                MyConn.Close();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void Salesperson_Load(object sender, EventArgs e)
        {
         
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 form = new Form1();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //open to view salary details and maybe save
           // this.Hide();

            //open another window to view salary
            EmployeeDetails emp = new EmployeeDetails();
            emp.Show();

        }

        public void searchData(string valueToSearch)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT carId,cType,cColour,branchName,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription" +
                    ",cPrice,cPicture,status,pBrand,mName,mEngineCC FROM cartable INNER JOIN modeldetails ON mId=modelId INNER JOIN producttable ON pId = brandId INNER JOIN branchdetails ON cLocation = branchId" +
                    " WHERE CONCAT(`carId`, `mName`, `pBrand`,`mEngineCC`,`cType`, `cColour`, `cLocation`, `cCarPlateNo`, `cManufactureYear`, `cMileage`, `quantity`, `cDescription`," +
                    " `cPrice`, `cPicture`, `status`)" +
                    " like '%" + this.searchButton.Text + "%'";
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            productTable.DataSource = table;
        }

        private void search_Click(object sender, EventArgs e)
        {
            string valueToSearch = searchButton.Text.ToString();
            searchData(valueToSearch);
        }

        private void productTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.productTable.Rows[e.RowIndex];

                this.details.Text = "Car ID: " + row.Cells["carId"].Value.ToString() + "\n" +
                                     "Car Brand: " + row.Cells["pBrand"].Value.ToString() + "\n" +
                                     "Car Model: " + row.Cells["mName"].Value.ToString() + "\n" +
                                    "Type: " + row.Cells["cType"].Value.ToString() + "\n" +
                                   "Branch Name: " + row.Cells["branchName"].Value.ToString() + "\n" +
                                    "Colour: " + row.Cells["cColour"].Value.ToString() + "\n" +
                                    "Car Plate No: " + row.Cells["cCarPlateNo"].Value.ToString() + "\n" +
                                    "Production Year: " + row.Cells["cManufactureYear"].Value.ToString() + "\n" +
                                    "Engine CC: " + row.Cells["mEngineCC"].Value.ToString() + "\n" +
                                    "Mileage: " + row.Cells["cMileage"].Value.ToString() + "\n" +
                                    "Quantity: " + row.Cells["quantity"].Value.ToString() + "\n" +
                                    "Description: " + row.Cells["cDescription"].Value.ToString() + "\n" +
                                    "Price: " + row.Cells["cPrice"].Value.ToString() + "\n" +
                                    "Status: " + row.Cells["status"].Value.ToString();

                string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                pProductPic.Image = Image.FromFile(paths + "\\pictures\\" + row.Cells["cPicture"].Value.ToString());
                
            }
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            details.Text = "";
            searchButton.Text = "";
        }
    }
}
