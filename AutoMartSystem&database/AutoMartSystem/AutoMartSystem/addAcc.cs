﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class addAcc : Form
    {
        string correctFilename;
        public addAcc()
        {
            InitializeComponent();
            
            //auto-generate accessories id
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT MAX(accId) FROM accessoriesdetails";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {

                    accId.Text = "A" + (int.Parse(MyReader.GetString("MAX(accId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            accPicture.Image = null;
       
            accName.Text = "";
            accPrice.Text = "";
         
            accQuantity.Text = "";
        
        }

        private void Add_productbtn_Click(object sender, EventArgs e)
        {
            try
            {

                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";



                //insert from the table of car details
                string Query = "INSERT INTO accessoriesdetails (accId,accName,accPrice,accQuantity,accPicture) " +
                    "VALUES ('" + this.accId.Text + "','" + this.accName.Text + "','" + int.Parse(this.accPrice.Text) + "','" + int.Parse(this.accQuantity.Text) + "','" + correctFilename + "');";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                MessageBox.Show("Record Saved", "Insert Record");
                MyConn.Close();

                Query = "SELECT MAX(accId) FROM accessoriesdetails";

                MyConn = new MySqlConnection(Conn);
                MyComd = new MySqlCommand(Query, MyConn);

                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    accId.Text = "A" + (int.Parse(MyReader.GetString("MAX(accId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

                accPicture.Image = null;

                accName.Text = "";
                accPrice.Text = "";

                accQuantity.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

         
           
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    accPicture.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void accPrice_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void accQuantity_TextChanged(object sender, EventArgs e)
        {

        }

        private void accPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void accQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
