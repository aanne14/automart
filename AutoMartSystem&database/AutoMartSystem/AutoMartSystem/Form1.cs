﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AutoMartSystem
{
    public partial class Form1 : Form
    {
        static int attempt = 0;
       // int Session[];
        public Form1()
        {
            InitializeComponent();
            password.UseSystemPasswordChar = true;
        }

 

        //when user presses the login button
        private void loginBtn_Click(object sender, EventArgs e)
        {


            //declare singleton object
            Employee login = Employee.GetInstance();

            if ((this.userID.Text == null) || (this.password.Text == null))
            {

                MessageBox.Show("Please ensure password and username are entered");
            }
            else
            {
                string getresult = login.getLoginResult(this.userID.Text, this.password.Text);


                if (getresult != "false")
                {
                    try
                    {
                        //to connect to phpmyadmin database name cardealership
                        string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                        //select from the table of staff details
                        string Query = "SELECT * FROM staffdetails WHERE staffIC =" + this.userID.Text;

                        MySqlConnection MyConn = new MySqlConnection(Conn);
                        MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                        MySqlDataReader MyReader;
                        MyConn.Open();
                        MyReader = MyComd.ExecuteReader();

                        if (MyReader.Read())
                        {
                            login.eName = MyReader.GetString("staffName");
                            login.eEmail = MyReader.GetString("staffEmailAdd");
                            login.ePhone = MyReader.GetString("staffPhone");
                            login.estaffId = MyReader.GetString("staffID");
                        }
                        else
                        {
                            MessageBox.Show("No record found", "Please try again");
                        }

                        //Store username in session
                       // Session["staffID"] = userID.Text;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    if (getresult == "manager")
                    {
                        this.Hide();
                        Manager_main manager = new Manager_main(login.eName, login.eEmail, login.ePhone, login.estaffId);
                        manager.Show();

                    }
                    else if (getresult == "admin")
                    {
                        this.Hide();
                        Admin admin = new Admin(login.eName, login.eEmail, login.ePhone, login.estaffId);
                        admin.Show();

                    }
                    else if (getresult == "salesperson")
                    {
                        this.Hide();
                        Salesperson salesperson = new Salesperson(login.eName, login.eEmail, login.ePhone, login.estaffId);
                        salesperson.Show();

                    }
                    else if (getresult == "false")
                    {
                        MessageBox.Show("Please try again");
                    }
                }

            }


        }

        //when user presses the clear button
        private void Clearbtn_Click(object sender, EventArgs e)
        {
            userID.Clear();
            password.Clear();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void passwordBtn_CheckedChanged(object sender, EventArgs e)
        {
            if(passwordBtn.Checked)
            {
                password.UseSystemPasswordChar = false;
            }
            else
            password.UseSystemPasswordChar = true;
        }
    }
}
