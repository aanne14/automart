﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Timers;
using AutoMartSystem.Subject;
using AutoMartSystem.Observer;

namespace AutoMartSystem
{


    public partial class Manager_main : Form
    {

        
            private Adapter Adapter1;

            public void Outputmessage(string message)
            {
                 this.textBox1.Text= message.ToString();
            }
        
        MySqlDataAdapter sqldata;
        DataTable employeetable;
        string role="";
        string manageemployeeid="";
        string ic="";
        public Manager_main(string name, string email, string phone, string staffid)
        {
            InitializeComponent();

            ///display name, email, phone, staffid
            mName.Text = name;
            mEmail.Text = email;
            mPhone.Text = phone;
            mID.Text = staffid;

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM staffdetails INNER JOIN logindetails ON loginID = staffIC", MyConn);
                employeetable = new DataTable();

                sqldata.Fill(employeetable);

                eTable.DataSource = employeetable;
                MyConn.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1 form = new Form1();
            form.Show();
        }

        private void employeeTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        //when the manager selects an option to generate report
        private void button2_Click(object sender, EventArgs e)
        {
            if (empDetails.Checked == true)
            {
               EmployeeHistory emp = new EmployeeHistory();
               emp.Show();

            }
            else if (salesReport.Checked == true)
            {
                CarSalesReport car = new CarSalesReport();
                car.Show(); //viewing report

            }
            else if (SalesRecord.Checked == true)
            {
                CarSalesRecord sales = new CarSalesRecord();
                sales.Show(); //viewing sales record
            }
            else
            {
                MessageBox.Show("Please select an option before generating a report");
            }

        }

        private void update_Click(object sender, EventArgs e)
        {
            ManageEmployee manage = new ManageEmployee(manageemployeeid);
            manage.Show();

        }


        private void add_Click(object sender, EventArgs e)
        {
            //go to add employee page
            AddEmployee employee = new AddEmployee();
            employee.Show();

        }


        private void delete_Click(object sender, EventArgs e)
        {
           
                try
                {    //to connect to phpmyadmin database name cardealership
                    string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                    string Query = "DELETE FROM staffdetails WHERE staffID =" + manageemployeeid;

                    MySqlConnection MyConn = new MySqlConnection(Conn);
                    MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MyConn.Close();

                    Query = "DELETE FROM logindetails WHERE loginID =" + ic;
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MyConn.Close();
                    MessageBox.Show("Record Deleted", "Records");


                    //Observer Design Pattern implementation to remove employee
                    Employed employ = new Employed();


                    if (role == "Admin")
                    {
                        Observer.Staff admin = new ObserverAdmin();
                        employ.Remove_Employee(admin);
                    }
                    else
                    {
                        Observer.Staff salesperson = new ObserverSalesperson();
                        employ.Remove_Employee(salesperson);
                    }

                    employ.NotifyManager(); //notify the removal of employees
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            
        }

        private void clear_Click(object sender, EventArgs e)
        {
            //Just Refresh the Form
            searchBox.Text = "";
        }

        private void updateTable_Click(object sender, EventArgs e)
        {

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of staff details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM staffdetails INNER JOIN logindetails ON staffIC = loginID", MyConn);
                employeetable = new DataTable();

                sqldata.Fill(employeetable);

                eTable.DataSource = employeetable;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void eno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void eic_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }



        private void eage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void esalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void ephone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void employeeTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private void salesReport_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void SalesRecord_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void empDetails_CheckedChanged(object sender, EventArgs e)
        {

        }

        //search for employee function
        public void searchData(string valueToSearch)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT * FROM staffdetails WHERE CONCAT(`staffID`, `staffName`, `staffIC`, `staffPhone`, `staffAge`, " +
                "`staffGender`, `staffEmailAdd`, `datejoined`, `salary`) like '%" + valueToSearch + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            eTable.DataSource = table;
        }
        private void searchBtn_Click(object sender, EventArgs e)
        {
            string valueToSearch = searchBox.Text.ToString();
            searchData(valueToSearch);
        }

        private void employeeTable_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void eTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.eTable.Rows[e.RowIndex];
                manageemployeeid = row.Cells["eId"].Value.ToString();
                role = row.Cells["roles"].Value.ToString();
                ic = row.Cells["eIc"].Value.ToString();
            }
        }
    }

   

}
