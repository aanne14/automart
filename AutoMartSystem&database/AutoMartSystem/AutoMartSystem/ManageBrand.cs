﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class ManageBrand : Form
    {
        string correctFilename,pictureurl;
        bool checkchangepicture = false;
        public ManageBrand(string pid)
        {
            InitializeComponent();
            this.pid.Text = pid;

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT * FROM producttable";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while(MyReader.Read())
                {
                    if(this.pid.Text == MyReader.GetString("pId"))
                    {
                        this.pBrand.Text = MyReader.GetString("pBrand");
                        pictureurl = MyReader.GetString("pPicture");
                        ///show picture in update brand page
                        string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                        pProductPic.Image = Image.FromFile(paths + "\\pictures\\" + MyReader.GetString("pPicture"));
                        
                    }
                   
                }
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            checkchangepicture = true;
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    pProductPic.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void Add_productbtn_Click(object sender, EventArgs e)
        {
            try
            {

                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";
                string Query;
                if(checkchangepicture == true)
                {
                    //update from the table of car details
                    Query = "UPDATE producttable set pBrand ='" + this.pBrand.Text + "',pPicture= '" 
                        + correctFilename + "' where pId='" + this.pid.Text + "';";

                }
                else
                {
                    //update from the table of car details
                    Query = "UPDATE producttable set pBrand ='" + this.pBrand.Text + "',pPicture= '" 
                        + pictureurl + "' where pId='" + this.pid.Text + "';";

                }
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                MessageBox.Show("Record Updated", "Update Record");
                MyConn.Close();

                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            pProductPic.Image = null;
            pBrand.Text = "";
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT MAX(pId) FROM producttable";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    pid.Text = "P" + (int.Parse(MyReader.GetString("MAX(pId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
            pProductPic.Image = null;
            pBrand.Text = "";
            this.Close();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
