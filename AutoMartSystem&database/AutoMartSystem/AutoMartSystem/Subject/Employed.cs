﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMartSystem.Subject
{
    public class Employed
    {
            private IList<Observer.Staff> employeed = new List<Observer.Staff>();
            public void Add_Employee(Observer.Staff e)
            {
                employeed.Add(e);
            }

            public void Remove_Employee(Observer.Staff e)
            {
                employeed.Remove(e);
            }

            public void NotifyManager()
            {
                foreach (Observer.Staff e in employeed)
                {
                    e.NotifyEmployeeChange();
                }
            }
        
    }
}
