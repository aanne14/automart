﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class ManageAcc : Form
    {
        string correctFilename;
        bool checkchangefilename = false;
        public ManageAcc(string accId)
        {
            InitializeComponent();
            this.accId.Text = accId;
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT * FROM accessoriesdetails";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                while (MyReader.Read())
                {
                    if (this.accId.Text == MyReader.GetString("accId"))
                    {
                        this.accName.Text = MyReader.GetString("accName");
                        this.accPrice.Text = MyReader.GetString("accPrice");
                        this.accQuantity.Text = MyReader.GetString("accQuantity");
                        this.accPicture.Text = MyReader.GetString("accPicture");

                        ///show picture in update car page
                        string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                        accPicture.Image = Image.FromFile(paths + "\\pictures\\" + MyReader.GetString("accPicture"));
                    }

                }
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Add_productbtn_Click(object sender, EventArgs e)
        {
            try
            {

                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";
                string Query;

                if (checkchangefilename == true)
                {
                    //insert from the table of accessories details
                    Query = "UPDATE accessoriesdetails set accName ='" + this.accName.Text + "',accPrice='" + int.Parse(this.accPrice.Text) + "',accQuantity='"
                        + int.Parse(this.accQuantity.Text) + "',accPicture='" + correctFilename + "' WHERE accId='" + this.accId.Text + "';";

                }
                else
                {
                    //insert from the table of accessories details
                    Query = "UPDATE accessoriesdetails set accName ='" + this.accName.Text + "',accPrice='" + int.Parse(this.accPrice.Text) + "',accQuantity='"
                        + int.Parse(this.accQuantity.Text) + "',accPicture='" + this.accPicture.Text + "' WHERE accId='" + this.accId.Text + "';";

                }
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                MessageBox.Show("Record Saved", "Insert Record");
                MyConn.Close();

                this.Close();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            accPicture.Image = null;

            accName.Text = "";
            accPrice.Text = "";

            accQuantity.Text = "";
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            accPicture.Image = null;

            accName.Text = "";
            accPrice.Text = "";

            accQuantity.Text = "";
        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            checkchangefilename = true;
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    accPicture.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void accPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void accQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
