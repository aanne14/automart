﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class addTransfer : Form
    {
        public addTransfer()
        {
            InitializeComponent();

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                //auto generate transfer id 
                string Query = "SELECT MAX(transferid) FROM transferdetails";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {

                    transferid.Text = "T" + (int.Parse(MyReader.GetString("MAX(transferid)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void searchTransfer_Click(object sender, EventArgs e)
        {

            //search for branch name and status from the pid entered
            try
            {
                string checkproductid;
                bool checkproduct = false;
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string query = "SELECT * FROM cartable INNER JOIN branchdetails ON cLocation = branchId WHERE carId='" + this.transferproduct.Text + "'";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    checkproductid = MyReader.GetString("carId");
                    transferStatus.Text = MyReader.GetString("status");
                    transferFrom.Text = MyReader.GetString("branchName");
                    if (checkproductid == transferproduct.Text)
                    {
                        checkproduct = true;
                    }

                }
                MyConn.Close();

                //check for product id existence
                if (checkproduct == true)
                {
                    searchTransfer.BackColor = Color.Green;
                }
                else
                {
                    searchTransfer.BackColor = Color.Red;
                    MessageBox.Show("Product ID doesn't exist. ", "Please try again.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            //to connect to phpmyadmin database name cardealership
            string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";
            int quantity=0;
            string checkproductid;
            bool checkproduct = false,checkquantity = true,checkvalidation = true;

            string query = "SELECT carId,branchName,cLocation,status,quantity FROM cartable INNER JOIN branchdetails ON cLocation = branchId WHERE carId='" + this.transferproduct.Text + "';";

            MySqlConnection MyConn = new MySqlConnection(Conn);
            MySqlCommand MyComd = new MySqlCommand(query, MyConn);
            MySqlDataReader MyReader;
            MyConn.Open();
            MyReader = MyComd.ExecuteReader();

            if (MyReader.Read())
            {
                quantity = int.Parse(MyReader.GetString("quantity"));
                checkproductid = MyReader.GetString("carId");
                transferStatus.Text = MyReader.GetString("status");
                transferFrom.Text = MyReader.GetString("branchName");
                if (checkproductid == transferproduct.Text)
                {
                    checkproduct = true;
                }

            }
            MyConn.Close();

            if(this.quantity.Text != "")
            {
                if (quantity < int.Parse(this.quantity.Text))
                {
                    checkquantity = false;
                    MessageBox.Show("The quantity of this car in that location is " + quantity, "Please try again.");

                }
                else
                {
                    checkquantity = true;
                }
            }
          
            //check for product id existence
            if (checkproduct == true)
            {
                searchTransfer.BackColor = Color.Green;
            }
            else
            {
                searchTransfer.BackColor = Color.Red;
                MessageBox.Show("Product ID doesn't exist. ", "Please try again.");

            }

            if(this.quantity.Text == "" || this.transferTo.Text == "-Select-")
            {
                checkvalidation = false;
                MessageBox.Show("Please insert value for quantity and choose a location to transfer to. ", "Please try again.");
            }
            else
            {
                checkvalidation = true;
            }

            if (searchTransfer.BackColor == Color.Green && this.transferStatus.Text == "available" && checkquantity == true && checkvalidation == true)
            {
                try
                {
                  
                    //insert from the table of transfer details
                    string Query = "INSERT INTO transferdetails (transferId,productid,transferfrom,transferto,tquantity,tdescription) " +
                        "VALUES ('" + this.transferid.Text + "','" + this.transferproduct.Text + "','" + this.transferFrom.Text +
                        "','" + this.transferTo.Text + "','" + this.quantity.Text + "','" + this.transferDescription.Text + "');";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                  
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MessageBox.Show("Record Saved", "Insert Record");
                    MyConn.Close();
                    string modelid="", carid="";
                    int mquantity = 0;
                    int carquantity = 0;
                    Query = "SELECT * FROM cartable INNER JOIN modeldetails ON mId = modelId";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {
                        carid = MyReader.GetString("carId");
                        modelid = MyReader.GetString("modelId");
                        carquantity = int.Parse(MyReader.GetString("quantity")) - int.Parse(this.quantity.Text);
                        mquantity = int.Parse(MyReader.GetString("mQuantity")) - int.Parse(this.quantity.Text);
                    }

                    MyConn.Close();

                 /*   Query = "UPDATE modeldetails set mQuantity = '" + mquantity + "' WHERE modelId='" + modelid + "';";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MyConn.Close();

                    Query = "UPDATE cartable set quantity = '" + carquantity + "' WHERE carId='" + carid + "';";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MyConn.Close();*/

                    //auto generate new transferid
                    Query = "SELECT MAX(transferid) FROM transferdetails";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {

                        transferid.Text = "T" + (int.Parse(MyReader.GetString("MAX(transferid)").Remove(0, 1)) + 1).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Please try again", "Please try again");
                    }

                    //refresh form
                    transferproduct.Text = "";
                    transferFrom.Text = "";
                    this.quantity.Text = "";
                    transferTo.Text = "-Select Location-";
                    transferDescription.Text = "";
                    searchTransfer.BackColor = Color.Gray;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select another product ID that is in the system. ", "Please try again.");
            }

        }

        //refresh form
        private void btnClear_Click(object sender, EventArgs e)
        {
            transferproduct.Text = "";
            transferFrom.Text = "";
            transferTo.Text = "-Select Location-";
            transferDescription.Text = "";
            searchTransfer.BackColor = Color.Gray;
            this.quantity.Text = "";
        }

        //exit this page
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void quantity_TextChanged(object sender, EventArgs e)
        {

        }

        private void quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
