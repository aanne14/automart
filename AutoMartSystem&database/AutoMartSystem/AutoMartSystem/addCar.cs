﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class addCar : Form
    {
        string correctFilename;
        public addCar()
        {
            InitializeComponent();

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                string Query = "SELECT MAX(carId) FROM cartable";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {

                    carId.Text = "C" + (int.Parse(MyReader.GetString("MAX(carId)").Remove(0, 1)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
            modelId.Text = "";
            cType.Text = "";
            cColour.Text = "";
            cLocation.Text = "";
            cCarPlateNo.Text = "";
            cProductionYear.Text = "";
            cMileage.Text = "";
            quantity.Text = "";
            cDescription.Text = "";
            cPrice.Text = "";
            cPicture.Image = null;
            searchProduct.BackColor = Color.Gray;

        }

        private void btnOpenPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.InitialDirectory = "C:\\";
            open.Filter = "Image files (*.JPG)|*.jpg|All files(*.*)|*.*";
            open.FilterIndex = 1;


            if (open.ShowDialog() == DialogResult.OK)
            {
                if (open.CheckFileExists)
                {
                    string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                    correctFilename = System.IO.Path.GetFileName(open.FileName);
                    System.IO.File.Copy(open.FileName, paths + "\\pictures\\" + correctFilename);
                    cPicture.Image = Image.FromFile(paths + "\\pictures\\" + correctFilename);

                }
            }
        }

        private void Add_Carbtn_Click(object sender, EventArgs e)
        {
            if(searchProduct.BackColor == Color.Green)
            {
                if(cLocation.Text == "Car Dealership Subang Jaya")
                {
                    cLocation.Text = "B2";
                }
                else if(cLocation.Text == "Car Dealership Glenmarie")
                {
                    cLocation.Text = "B1";
                }
                else
                {
                    cLocation.Text = "B3";
                }
                try
                {

                    //to connect to phpmyadmin database name cardealership
                    string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";



                    //insert from the table of car details
                    string Query = "INSERT INTO cartable (carId,mId,cType,cColour,cLocation,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription,cPrice,cPicture,status) " +
                        "VALUES ('" + this.carId.Text + "','" + this.modelId.Text + "','" + this.cType.Text + "','" + this.cColour.Text + "','" + this.cLocation.Text +
                        "','" + this.cCarPlateNo.Text + "','" + this.cProductionYear.Text + "','" + this.cMileage.Text + "','" + this.quantity.Text + "','" + this.cDescription.Text +
                        "','" + this.cPrice.Text + "','" + correctFilename + "','" + "available" + "');";

                    MySqlConnection MyConn = new MySqlConnection(Conn);
                    MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                    MySqlDataReader MyReader;
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MessageBox.Show("Record Saved", "Insert Record");
                    MyConn.Close();

                    int mquantity=0;
                    Query = "SELECT * FROM modeldetails";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                 
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {

                        mquantity = int.Parse(MyReader.GetString("mQuantity")) + int.Parse(this.quantity.Text);
                    }
                
                    MyConn.Close();

                    Query = "UPDATE modeldetails set mQuantity = '" + mquantity + "' WHERE modelId='" + this.modelId.Text + "';";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    MyConn.Close();
                    Query = "SELECT MAX(carId) FROM cartable";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {
                        carId.Text = "C" + (int.Parse(MyReader.GetString("MAX(carId)").Remove(0, 1)) + 1).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Please try again", "Please try again");
                    }

                    modelId.Text = "";
                    cType.Text = "";
                    cColour.Text = "";
                    cLocation.Text = "";
                    cCarPlateNo.Text = "";
                    cProductionYear.Text = "";
                    cMileage.Text = "";
                    quantity.Text = "";
                    cDescription.Text = "";
                    cPrice.Text = "";
                    cPicture.Image = null;
                    searchProduct.BackColor = Color.Gray;


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
              
            }
         
        }

        private void searchProduct_Click(object sender, EventArgs e)
        {
            //search for branch name and status from the pid entered
            try
            {
                string checkproductid;
                bool checkproduct = false;
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string query = "SELECT modelId FROM modeldetails WHERE modelId='" + this.modelId.Text + "'";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    checkproductid = MyReader.GetString("modelId");
                   
                    if (checkproductid == modelId.Text)
                    {
                        checkproduct = true;
                    }

                }
                MyConn.Close();

                //check for product id existence
                if (checkproduct == true)
                {
                    searchProduct.BackColor = Color.Green;
                }
                else
                {
                    searchProduct.BackColor = Color.Red;
                    MessageBox.Show("Model ID doesn't exist. ", "Please try again.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void carId_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void cType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void cColour_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cPicture_Click(object sender, EventArgs e)
        {

        }

        private void quantity_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void selectPic_Click(object sender, EventArgs e)
        {

        }

        private void cDescription_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pId_TextChanged(object sender, EventArgs e)
        {

        }

        private void cPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void cLocation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cProductionYear_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void cCarPlateNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void cMileage_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cProductionYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void cMileage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void cPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
