﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class Admin : Form
    {
        MySqlDataAdapter sqldata;
        DataTable table;
        MySqlConnection MyConn;
        string managepid = "";
        string managecustid = "";
        string managecarid = "";
        string accessoriesid = "";
        string modelid = "";
        string managetransferid = "";
        public Admin(string name, string email, string phone, string staffid)
        {
            InitializeComponent();
            ///display name, email, phone, staffid
            aName.Text = name;
            aEmail.Text = email;
            aPhone.Text = phone;
            aID.Text = staffid;

            searchData("");
            searchDataTransfer("");
            searchDataCustomer("");

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                //auto generate into datagridview of product table
                MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM producttable", MyConn);
                table = new DataTable();

                sqldata.Fill(table);

                brandTable.DataSource = table;
                MyConn.Close();

                ///display transfer details in datagridview
                MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM transferdetails", MyConn);
                table = new DataTable();

                sqldata.Fill(table);

                transferTable.DataSource = table;
                MyConn.Close();

                //display customer details in datagridview
                MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM customerdetails", MyConn);
                table = new DataTable();

                sqldata.Fill(table);

                customerTable.DataSource = table;
                MyConn.Close();

                ///display car details in datagridview
                MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT carId,cType,cColour,branchName,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription" +
                    ",cPrice,cPicture,status,pBrand,mName FROM cartable INNER JOIN modeldetails ON mId=modelId INNER JOIN producttable ON pId = brandId INNER JOIN branchdetails ON cLocation = branchId", MyConn);
                table = new DataTable();

                sqldata.Fill(table);

                CarDataTable.DataSource = table;
                MyConn.Close();

                ///display accessories details in datagridview
                MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT * FROM accessoriesdetails", MyConn);
                table = new DataTable();

                sqldata.Fill(table);

                accTable.DataSource = table;
                MyConn.Close();

                ///display model details in datagridview
                MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                sqldata = new MySqlDataAdapter("SELECT modelId,pBrand,mName,mEngineCC,mQuantity,mSeats,mDoor,mDescription,mPicture FROM modeldetails INNER JOIN producttable ON brandId=pId", MyConn);
                table = new DataTable();

                sqldata.Fill(table);

                modelTable.DataSource = table;
                MyConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public Admin()
        {
        }

        //log out from admin page go to main page
        private void logoutBtn_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            form.Show();
            this.Close();
          
        }

        ///refresh product page
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            sSearch.Text = "";
            brandPicture.Image = null;
        }

        //search for product function
        public void searchData(string valueToSearch)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT * FROM producttable WHERE CONCAT(`pId`, `pBrand`, `pPicture`)" +
                    " like '%" + this.sSearch.Text + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            brandTable.DataSource = table;
        }

        //search product button
        private void searchProduct_Click(object sender, EventArgs e)
        {
            string valueToSearch = sSearch.Text.ToString();
            searchData(valueToSearch);
        }
      
        //admin page load
        private void Admin_Load(object sender, EventArgs e)
        {
            searchData("");
            searchDataTransfer("");
            searchDataCustomer("");

        }

        private void deleteProduct_Click(object sender, EventArgs e)
        {
          /*  int rowIndex = productTable.CurrentCell.RowIndex;
            productTable.Rows.RemoveAt(rowIndex);

            try
            {    //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string Query = "DELETE FROM cartable WHERE pId =" + int.Parse(dProductId.Text);

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();
                MyConn.Close();


                MessageBox.Show("Record Deleted", "Records");

                //Just Refresh the Form
                sSearch.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }*/

        }

      

        private void productTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           /* if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.productTable.Rows[e.RowIndex];

                this.dProductId.Text = row.Cells["productId"].Value.ToString();

            }*/
        }

        ///why not just write a discount in invoice ?
        private void sPromoProduBtn_Click(object sender, EventArgs e)
        {
            /*try
            {
                string checkproductid;
                bool checkproduct = false;
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                string query = "SELECT branchName,pId,status FROM cartable INNER JOIN branchdetails ON pLocation = branchId WHERE pId='" + this.transferproduct.Text + "'";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {
                    checkproductid = MyReader.GetString("pId");
                    this.transferFrom.Text = MyReader.GetString("branchName");
                    this.transferStatus.Text = MyReader.GetString("status");
                    if (checkproductid == transferproduct.Text)
                    {
                        checkproduct = true;
                    }

                }
                MyConn.Close();

                if (checkproduct == true)
                {
                    sPromoProduBtn.BackColor = Color.Green;
                }
                else
                {
                    sPromoProduBtn.BackColor = Color.Red;
                    MessageBox.Show("Product ID doesn't exist. ", "Please try again.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }*/
        }

        //search for transfer table
        public void searchDataTransfer(string valueToSearch)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT * FROM transferdetails WHERE CONCAT(`transferId`, `productid`, `transferfrom`, `transferto`, `tdescription`)" +
                    " like '%" + this.searchTransfer.Text + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            transferTable.DataSource = table;
        }

        //search for transfer table 
        private void searchTransferBtn_Click(object sender, EventArgs e)
        {
            string valueToSearch = searchTransfer.Text.ToString();
            searchDataTransfer(valueToSearch);
        }

        //goto transfer page
        private void createTransfer_Click(object sender, EventArgs e)
        {
            addTransfer transfer = new addTransfer();
            transfer.Show();
        }

        //refresh button in tranfer tabpage
        private void refreshTransferBtn_Click(object sender, EventArgs e)
        {
            searchTransfer.Text = "";
        }

        //update transfer table
        private void updateTransferTable_Click(object sender, EventArgs e)
        {
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of car details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT * FROM transferdetails", MyConn);
                DataTable producttable = new DataTable();

                sqldata.Fill(producttable);

                transferTable.DataSource = producttable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //go to add customer page
        private void addCustomer_Click(object sender, EventArgs e)
        {
            addCustomer customer = new addCustomer();
            customer.Show();
        }

        //search for customer table
        public void searchDataCustomer(string customerValue)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT * FROM customerdetails WHERE CONCAT(`custID`, `custIC`, `custName`, `custAdd`, `custPhone`, `custGender`, `custAge`)" +
                    " like '%" + this.searchDetails.Text + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            customerTable.DataSource = table;
        }

     

        private void updateCustTable_Click(object sender, EventArgs e)
        {
            //update customer table
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of customer details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT * FROM customerdetails", MyConn);
                DataTable producttable = new DataTable();

                sqldata.Fill(producttable);

                customerTable.DataSource = producttable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cusRefresh_Click(object sender, EventArgs e)
        {
            searchDetails.Text = "";
        }

        private void searchCustomer_Click(object sender, EventArgs e)
        {
            string customerValue = searchDetails.Text.ToString();
            searchDataCustomer(customerValue);
        }


        //refresh datagridview brand
        private void updateBrand_Click(object sender, EventArgs e)
        {
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of car details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT * FROM producttable", MyConn);
                DataTable producttable = new DataTable();

                sqldata.Fill(producttable);

                brandTable.DataSource = producttable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        ///click on cell content display brand picture
        private void brandTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.brandTable.Rows[e.RowIndex];

                ///show picture in brand tabpage
                   string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                   brandPicture.Image = Image.FromFile(paths + "\\pictures\\" + row.Cells["pPicture"].Value.ToString());
                   managepid = row.Cells["productId"].Value.ToString();
            }
        }

        //go to addbrand page
        private void addBrand_Click_1(object sender, EventArgs e)
        {
            Addproduct product = new Addproduct();
            product.Show();
        }

        //go to addcar page
        private void addCar_Click(object sender, EventArgs e)
        {
            addCar car = new addCar();
            car.Show();
        }

        //update car datagridview
        private void updateCarTable_Click(object sender, EventArgs e)
        {
            //update customer table
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of car details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT  carId,cType,cColour,branchName,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription" +
                    ",cPrice,cPicture,status,pBrand,mName FROM cartable INNER JOIN modeldetails ON mId=modelId INNER JOIN producttable ON pId = brandId INNER JOIN branchdetails ON cLocation = branchId", MyConn);
                DataTable producttable = new DataTable();

                sqldata.Fill(producttable);

                CarDataTable.DataSource = producttable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //refresh car tabpage
        private void carRefreshBtn_Click(object sender, EventArgs e)
        {
            carSearchBox.Text = "";
            carPicture.Image = null;
        }

        //search for car tabpage
        public void searchDataCar(string customerValue)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT carId,cType,cColour,cLocation,cCarPlateNo,cManufactureYear,cMileage,quantity,cDescription" +
                    ",cPrice,cPicture,status,pBrand,mName FROM cartable INNER JOIN modeldetails ON mId=modelId INNER JOIN producttable ON pId = brandId INNER JOIN branchdetails ON cLocation = branchId" +
                    " WHERE CONCAT(`carId`, `mName`, `pBrand`,`cType`, `cColour`, `cLocation`, `branchName`, `cCarPlateNo`, `cManufactureYear`, `cMileage`, `quantity`, `cDescription`," +
                    " `cPrice`, `cPicture`, `status`)" +
                    " like '%" + this.carSearchBox.Text + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            CarDataTable.DataSource = table;
        }

        //search for car
        private void carSearch_Click(object sender, EventArgs e)
        {
            string customerValue = carSearchBox.Text.ToString();
            searchDataCar(customerValue);
        }

 

        private void addAcc_Click(object sender, EventArgs e)
        {
            addAcc acc = new addAcc();
            acc.Show();
        }

        private void updateAcc_Click(object sender, EventArgs e)
        {
            //update accessories table
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of accessories details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT * FROM accessoriesdetails", MyConn);
                DataTable producttable = new DataTable();

                sqldata.Fill(producttable);

                accTable.DataSource = producttable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void refreshAccBtn_Click(object sender, EventArgs e)
        {
            accSearchBox.Text = "";
            acPicture.Image = null;
        }

        private void accTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.accTable.Rows[e.RowIndex];

                string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                acPicture.Image = Image.FromFile(paths + "\\pictures\\" + row.Cells["accPicture"].Value.ToString());
                accessoriesid = row.Cells["accId"].Value.ToString();
            }
        }

        //search for accessories tabpage
        public void searchDataAcc(string customerValue)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT * FROM accessoriesdetails WHERE CONCAT(`accId`, `accName`, `accPrice`, `accQuantity`, `accPicture`)" +
                    " like '%" + this.accSearchBox.Text + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            accTable.DataSource = table;
        }

        //search accessories function
        private void searchAcc_Click(object sender, EventArgs e)
        {
            string customerValue = accSearchBox.Text.ToString();
            searchDataAcc(customerValue);
        }

        private void addModel_Click(object sender, EventArgs e)
        {
            addModel model = new addModel();
            model.Show();
        }

        private void updmodelTable_Click(object sender, EventArgs e)
        {
            //update accessories table
            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


                //select from the table of accessories details
                MySqlConnection MyConn = new MySqlConnection(Conn);
                MyConn.Open();
                MySqlDataAdapter sqldata = new MySqlDataAdapter("SELECT * FROM modeldetails INNER JOIN producttable ON brandId = pId", MyConn);
                DataTable producttable = new DataTable();

                sqldata.Fill(producttable);

                modelTable.DataSource = producttable;
                MyConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void refreshModelBtn_Click(object sender, EventArgs e)
        {
            modelSearchBox.Text = "";
            mPicture.Image = null;
        }

        private void modelTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.modelTable.Rows[e.RowIndex];

                string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                mPicture.Image = Image.FromFile(paths + "\\pictures\\" + row.Cells["pic"].Value.ToString());
                modelid = row.Cells["modId"].Value.ToString();
            }
        }

        //search for model tabpage
        public void searchModelCar(string customerValue)
        {
            MySqlConnection connection = new MySqlConnection("datasource=localhost;port=3306;Initial Catalog ='cardealership';username=root;password=");

            string query = "SELECT * FROM modeldetails INNER JOIN producttable ON brandId = pId WHERE CONCAT(`modelId`, `brandId`, `mName`, `mEngineCC`, `mQuantity`, `mSeats`, `mDoor`, `mDescription`, `mPicture`)" +
                    " like '%" + this.modelSearchBox.Text + "%'";

            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataAdapter adapter = new MySqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            modelTable.DataSource = table;
        }
        private void searchModel_Click(object sender, EventArgs e)
        {
            string customerValue = modelSearchBox.Text.ToString();
            searchModelCar(customerValue);
        }

        private void manageBrandBtn_Click(object sender, EventArgs e)
        {
          
            ManageBrand manage = new ManageBrand(managepid);
            manage.Show();
        }

        private void customerTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.customerTable.Rows[e.RowIndex];
                managecustid = row.Cells["custID"].Value.ToString();
            }
        }

        private void manageCustBtn_Click(object sender, EventArgs e)
        {
            ManageCustomer manage = new ManageCustomer(managecustid);
            manage.Show();
        }

        private void manageCarBtn_Click(object sender, EventArgs e)
        {
            ManageCar manage = new ManageCar(managecarid);
                manage.Show();
        }

        private void updateAccBtn_Click(object sender, EventArgs e)
        {
            ManageAcc manage = new ManageAcc(accessoriesid);
            manage.Show();
        }

        private void updateModelBtn_Click(object sender, EventArgs e)
        {
            ManageModel manage =new ManageModel(modelid);
            manage.Show();
        }

      

        private void transferTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.transferTable.Rows[e.RowIndex];
              
            }
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void promoproduQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void promoProduPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void CarDataTable_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.CarDataTable.Rows[e.RowIndex];

                string paths = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                carPicture.Image = Image.FromFile(paths + "\\pictures\\" + row.Cells["cPicture"].Value.ToString());
                managecarid = row.Cells["cartableid"].Value.ToString();
            }
        }
    }
}
