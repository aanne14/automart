﻿namespace AutoMartSystem
{
    partial class addTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.transferTo = new System.Windows.Forms.ComboBox();
            this.transferDescription = new System.Windows.Forms.TextBox();
            this.transferid = new System.Windows.Forms.TextBox();
            this.transferFrom = new System.Windows.Forms.TextBox();
            this.searchTransfer = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.transferproduct = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.quantity = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.transferStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // transferTo
            // 
            this.transferTo.FormattingEnabled = true;
            this.transferTo.Items.AddRange(new object[] {
            "Car Dealership Glenmarie",
            "Car Dealership Subang Jaya",
            "Car Dealership KL"});
            this.transferTo.Location = new System.Drawing.Point(744, 185);
            this.transferTo.Margin = new System.Windows.Forms.Padding(4);
            this.transferTo.Name = "transferTo";
            this.transferTo.Size = new System.Drawing.Size(480, 28);
            this.transferTo.TabIndex = 34;
            this.transferTo.Text = "-Select-";
            // 
            // transferDescription
            // 
            this.transferDescription.Location = new System.Drawing.Point(217, 244);
            this.transferDescription.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.transferDescription.Multiline = true;
            this.transferDescription.Name = "transferDescription";
            this.transferDescription.Size = new System.Drawing.Size(432, 142);
            this.transferDescription.TabIndex = 33;
            // 
            // transferid
            // 
            this.transferid.Enabled = false;
            this.transferid.Location = new System.Drawing.Point(321, 45);
            this.transferid.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.transferid.Name = "transferid";
            this.transferid.Size = new System.Drawing.Size(232, 27);
            this.transferid.TabIndex = 32;
            // 
            // transferFrom
            // 
            this.transferFrom.Location = new System.Drawing.Point(111, 186);
            this.transferFrom.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.transferFrom.Name = "transferFrom";
            this.transferFrom.Size = new System.Drawing.Size(442, 27);
            this.transferFrom.TabIndex = 31;
            // 
            // searchTransfer
            // 
            this.searchTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTransfer.ForeColor = System.Drawing.Color.Black;
            this.searchTransfer.Location = new System.Drawing.Point(578, 91);
            this.searchTransfer.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.searchTransfer.Name = "searchTransfer";
            this.searchTransfer.Size = new System.Drawing.Size(118, 38);
            this.searchTransfer.TabIndex = 30;
            this.searchTransfer.Text = "Search";
            this.searchTransfer.UseVisualStyleBackColor = true;
            this.searchTransfer.Click += new System.EventHandler(this.searchTransfer_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(321, 424);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(192, 62);
            this.btnClear.TabIndex = 29;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(90, 424);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(192, 62);
            this.btnConfirm.TabIndex = 28;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(96, 244);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(100, 20);
            this.label36.TabIndex = 25;
            this.label36.Text = "Description:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(738, 145);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(33, 20);
            this.label33.TabIndex = 24;
            this.label33.Text = "To:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(104, 146);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 20);
            this.label15.TabIndex = 23;
            this.label15.Text = "From:";
            // 
            // transferproduct
            // 
            this.transferproduct.Location = new System.Drawing.Point(321, 102);
            this.transferproduct.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.transferproduct.Name = "transferproduct";
            this.transferproduct.Size = new System.Drawing.Size(232, 27);
            this.transferproduct.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(104, 45);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 20);
            this.label14.TabIndex = 21;
            this.label14.Text = "Transfer#:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(104, 102);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 20);
            this.label13.TabIndex = 20;
            this.label13.Text = "Car ID:";
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(542, 424);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(192, 62);
            this.exitBtn.TabIndex = 35;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(740, 100);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 36;
            this.label1.Text = "Quantity:";
            // 
            // quantity
            // 
            this.quantity.Location = new System.Drawing.Point(901, 97);
            this.quantity.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(77, 27);
            this.quantity.TabIndex = 37;
            this.quantity.TextChanged += new System.EventHandler(this.quantity_TextChanged);
            this.quantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantity_KeyPress);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(740, 52);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(62, 20);
            this.label38.TabIndex = 26;
            this.label38.Text = "Status:";
            // 
            // transferStatus
            // 
            this.transferStatus.AutoSize = true;
            this.transferStatus.Location = new System.Drawing.Point(897, 52);
            this.transferStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.transferStatus.Name = "transferStatus";
            this.transferStatus.Size = new System.Drawing.Size(113, 20);
            this.transferStatus.TabIndex = 27;
            this.transferStatus.Text = "transferstatus";
            // 
            // addTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1289, 628);
            this.Controls.Add(this.quantity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.transferTo);
            this.Controls.Add(this.transferDescription);
            this.Controls.Add(this.transferid);
            this.Controls.Add(this.transferFrom);
            this.Controls.Add(this.searchTransfer);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.transferStatus);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.transferproduct);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "addTransfer";
            this.Text = "addTransfer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox transferTo;
        private System.Windows.Forms.TextBox transferDescription;
        private System.Windows.Forms.TextBox transferid;
        private System.Windows.Forms.TextBox transferFrom;
        private System.Windows.Forms.Button searchTransfer;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox transferproduct;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox quantity;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label transferStatus;
    }
}