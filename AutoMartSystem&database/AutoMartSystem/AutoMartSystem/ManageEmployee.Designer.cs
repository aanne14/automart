﻿namespace AutoMartSystem
{
    partial class ManageEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.edatejoined = new System.Windows.Forms.DateTimePicker();
            this.egender = new System.Windows.Forms.ComboBox();
            this.epassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.eno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.erole = new System.Windows.Forms.ComboBox();
            this.esalary = new System.Windows.Forms.TextBox();
            this.eemailadd = new System.Windows.Forms.TextBox();
            this.eage = new System.Windows.Forms.TextBox();
            this.ephone = new System.Windows.Forms.TextBox();
            this.eic = new System.Windows.Forms.TextBox();
            this.ename = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.update = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.staffStatus = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(112, 180);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(47, 22);
            this.textBox1.TabIndex = 56;
            this.textBox1.Text = "+60";
            // 
            // edatejoined
            // 
            this.edatejoined.Location = new System.Drawing.Point(627, 84);
            this.edatejoined.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.edatejoined.Name = "edatejoined";
            this.edatejoined.Size = new System.Drawing.Size(295, 22);
            this.edatejoined.TabIndex = 55;
            // 
            // egender
            // 
            this.egender.FormattingEnabled = true;
            this.egender.Items.AddRange(new object[] {
            "Male\t",
            "Female"});
            this.egender.Location = new System.Drawing.Point(112, 269);
            this.egender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.egender.Name = "egender";
            this.egender.Size = new System.Drawing.Size(121, 24);
            this.egender.TabIndex = 54;
            this.egender.Text = "-Select-";
            // 
            // epassword
            // 
            this.epassword.Location = new System.Drawing.Point(627, 126);
            this.epassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.epassword.Name = "epassword";
            this.epassword.Size = new System.Drawing.Size(565, 22);
            this.epassword.TabIndex = 53;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(476, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 17);
            this.label14.TabIndex = 52;
            this.label14.Text = "Password:";
            // 
            // eno
            // 
            this.eno.Enabled = false;
            this.eno.Location = new System.Drawing.Point(112, 44);
            this.eno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eno.Name = "eno";
            this.eno.Size = new System.Drawing.Size(125, 22);
            this.eno.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 50;
            this.label1.Text = "No:";
            // 
            // erole
            // 
            this.erole.FormattingEnabled = true;
            this.erole.Items.AddRange(new object[] {
            "manager\t",
            "salesperson",
            "admin"});
            this.erole.Location = new System.Drawing.Point(627, 219);
            this.erole.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.erole.Name = "erole";
            this.erole.Size = new System.Drawing.Size(121, 24);
            this.erole.TabIndex = 49;
            this.erole.Text = "-Select-";
            // 
            // esalary
            // 
            this.esalary.Location = new System.Drawing.Point(627, 175);
            this.esalary.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.esalary.Name = "esalary";
            this.esalary.Size = new System.Drawing.Size(151, 22);
            this.esalary.TabIndex = 48;
            this.esalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.esalary_KeyPress);
            // 
            // eemailadd
            // 
            this.eemailadd.Location = new System.Drawing.Point(627, 39);
            this.eemailadd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eemailadd.Name = "eemailadd";
            this.eemailadd.Size = new System.Drawing.Size(291, 22);
            this.eemailadd.TabIndex = 47;
            // 
            // eage
            // 
            this.eage.Location = new System.Drawing.Point(112, 221);
            this.eage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eage.Name = "eage";
            this.eage.Size = new System.Drawing.Size(79, 22);
            this.eage.TabIndex = 46;
            this.eage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.eage_KeyPress);
            // 
            // ephone
            // 
            this.ephone.Location = new System.Drawing.Point(164, 180);
            this.ephone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ephone.Name = "ephone";
            this.ephone.Size = new System.Drawing.Size(171, 22);
            this.ephone.TabIndex = 45;
            this.ephone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ephone_KeyPress);
            // 
            // eic
            // 
            this.eic.Location = new System.Drawing.Point(112, 133);
            this.eic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eic.Name = "eic";
            this.eic.Size = new System.Drawing.Size(229, 22);
            this.eic.TabIndex = 44;
            this.eic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.eic_KeyPress);
            // 
            // ename
            // 
            this.ename.Location = new System.Drawing.Point(112, 90);
            this.ename.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ename.Name = "ename";
            this.ename.Size = new System.Drawing.Size(229, 22);
            this.ename.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(26, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 17);
            this.label16.TabIndex = 42;
            this.label16.Text = "IC:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(476, 225);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 17);
            this.label15.TabIndex = 41;
            this.label15.Text = "Roles:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(476, 180);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 17);
            this.label13.TabIndex = 40;
            this.label13.Text = "Salary:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(476, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 17);
            this.label12.TabIndex = 39;
            this.label12.Text = "Date Joined:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(476, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 17);
            this.label11.TabIndex = 38;
            this.label11.Text = "Email Address:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 17);
            this.label10.TabIndex = 37;
            this.label10.Text = "Gender:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Age:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 34;
            this.label2.Text = "Phone:";
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(242, 440);
            this.update.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(179, 33);
            this.update.TabIndex = 58;
            this.update.Text = "Update Record";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(443, 440);
            this.clear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(135, 33);
            this.clear.TabIndex = 59;
            this.clear.Text = "Clear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(599, 440);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(179, 33);
            this.exitBtn.TabIndex = 60;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(479, 269);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 61;
            this.label3.Text = "Status:";
            // 
            // staffStatus
            // 
            this.staffStatus.FormattingEnabled = true;
            this.staffStatus.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.staffStatus.Location = new System.Drawing.Point(627, 262);
            this.staffStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.staffStatus.Name = "staffStatus";
            this.staffStatus.Size = new System.Drawing.Size(121, 24);
            this.staffStatus.TabIndex = 62;
            this.staffStatus.Text = "-Select-";
            // 
            // ManageEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 494);
            this.Controls.Add(this.staffStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.update);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.edatejoined);
            this.Controls.Add(this.egender);
            this.Controls.Add(this.epassword);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.eno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.erole);
            this.Controls.Add(this.esalary);
            this.Controls.Add(this.eemailadd);
            this.Controls.Add(this.eage);
            this.Controls.Add(this.ephone);
            this.Controls.Add(this.eic);
            this.Controls.Add(this.ename);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Name = "ManageEmployee";
            this.Text = "ManageEmployee";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DateTimePicker edatejoined;
        private System.Windows.Forms.ComboBox egender;
        private System.Windows.Forms.TextBox epassword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox eno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox erole;
        private System.Windows.Forms.TextBox esalary;
        private System.Windows.Forms.TextBox eemailadd;
        private System.Windows.Forms.TextBox eage;
        private System.Windows.Forms.TextBox ephone;
        private System.Windows.Forms.TextBox eic;
        private System.Windows.Forms.TextBox ename;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox staffStatus;
    }
}