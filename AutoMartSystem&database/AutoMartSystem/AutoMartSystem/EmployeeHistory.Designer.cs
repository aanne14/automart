﻿namespace AutoMartSystem
{
    partial class EmployeeHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.employeeDetails = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BestSales = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BestSalary = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BestName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.employeeDetails)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Employee Details ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(679, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Return";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // employeeDetails
            // 
            this.employeeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employeeDetails.Location = new System.Drawing.Point(36, 122);
            this.employeeDetails.Name = "employeeDetails";
            this.employeeDetails.Size = new System.Drawing.Size(718, 113);
            this.employeeDetails.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BestSales);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.BestSalary);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.BestName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(36, 286);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 115);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Best Employee: ";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // BestSales
            // 
            this.BestSales.AutoSize = true;
            this.BestSales.Location = new System.Drawing.Point(68, 84);
            this.BestSales.Name = "BestSales";
            this.BestSales.Size = new System.Drawing.Size(13, 13);
            this.BestSales.TabIndex = 5;
            this.BestSales.Text = "7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Total Sales:";
            // 
            // BestSalary
            // 
            this.BestSalary.AutoSize = true;
            this.BestSalary.Location = new System.Drawing.Point(101, 59);
            this.BestSalary.Name = "BestSalary";
            this.BestSalary.Size = new System.Drawing.Size(31, 13);
            this.BestSalary.TabIndex = 3;
            this.BestSalary.Text = "5500";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Salary :          RM";
            // 
            // BestName
            // 
            this.BestName.AutoSize = true;
            this.BestName.Location = new System.Drawing.Point(68, 29);
            this.BestName.Name = "BestName";
            this.BestName.Size = new System.Drawing.Size(74, 13);
            this.BestName.TabIndex = 1;
            this.BestName.Text = "Tang Xue Jun";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name :";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(405, 325);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 33);
            this.button2.TabIndex = 4;
            this.button2.Text = "Calculate Commision";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // EmployeeHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.employeeDetails);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "EmployeeHistory";
            this.Text = "EmployeeHistory";
            ((System.ComponentModel.ISupportInitialize)(this.employeeDetails)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView employeeDetails;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label BestSales;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label BestSalary;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label BestName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
    }
}