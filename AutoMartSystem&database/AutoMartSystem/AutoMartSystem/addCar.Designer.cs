﻿namespace AutoMartSystem
{
    partial class addCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnOpenPic = new System.Windows.Forms.Button();
            this.cColour = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cPicture = new System.Windows.Forms.PictureBox();
            this.quantity = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cDescription = new System.Windows.Forms.ComboBox();
            this.modelId = new System.Windows.Forms.TextBox();
            this.cPrice = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cLocation = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cProductionYear = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cCarPlateNo = new System.Windows.Forms.TextBox();
            this.cMileage = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.Add_Carbtn = new System.Windows.Forms.Button();
            this.carId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.searchProduct = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // cType
            // 
            this.cType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cType.FormattingEnabled = true;
            this.cType.Items.AddRange(new object[] {
            "Used Car",
            "New Car"});
            this.cType.Location = new System.Drawing.Point(244, 87);
            this.cType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cType.Name = "cType";
            this.cType.Size = new System.Drawing.Size(196, 28);
            this.cType.TabIndex = 90;
            this.cType.Text = "-Select Type-";
            this.cType.SelectedIndexChanged += new System.EventHandler(this.cType_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(53, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 20);
            this.label6.TabIndex = 89;
            this.label6.Text = "Product Type:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // btnOpenPic
            // 
            this.btnOpenPic.Location = new System.Drawing.Point(603, 314);
            this.btnOpenPic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOpenPic.Name = "btnOpenPic";
            this.btnOpenPic.Size = new System.Drawing.Size(152, 30);
            this.btnOpenPic.TabIndex = 88;
            this.btnOpenPic.Text = "Open Picture";
            this.btnOpenPic.UseVisualStyleBackColor = true;
            this.btnOpenPic.Click += new System.EventHandler(this.btnOpenPic_Click);
            // 
            // cColour
            // 
            this.cColour.Location = new System.Drawing.Point(243, 128);
            this.cColour.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cColour.Name = "cColour";
            this.cColour.Size = new System.Drawing.Size(196, 22);
            this.cColour.TabIndex = 87;
            this.cColour.TextChanged += new System.EventHandler(this.cColour_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(53, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 86;
            this.label5.Text = "Car Colour: ";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // cPicture
            // 
            this.cPicture.Location = new System.Drawing.Point(480, 55);
            this.cPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cPicture.Name = "cPicture";
            this.cPicture.Size = new System.Drawing.Size(396, 242);
            this.cPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cPicture.TabIndex = 85;
            this.cPicture.TabStop = false;
            this.cPicture.Click += new System.EventHandler(this.cPicture_Click);
            // 
            // quantity
            // 
            this.quantity.Location = new System.Drawing.Point(244, 319);
            this.quantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(87, 22);
            this.quantity.TabIndex = 84;
            this.quantity.TextChanged += new System.EventHandler(this.quantity_TextChanged);
            this.quantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantity_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(53, 314);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 20);
            this.label14.TabIndex = 83;
            this.label14.Text = "Quantity: ";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // cDescription
            // 
            this.cDescription.FormattingEnabled = true;
            this.cDescription.Items.AddRange(new object[] {
            "Sports Car",
            "Recreational Car",
            "Family Car"});
            this.cDescription.Location = new System.Drawing.Point(244, 350);
            this.cDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cDescription.Name = "cDescription";
            this.cDescription.Size = new System.Drawing.Size(197, 24);
            this.cDescription.TabIndex = 81;
            this.cDescription.Text = "-Select-";
            this.cDescription.SelectedIndexChanged += new System.EventHandler(this.cDescription_SelectedIndexChanged);
            // 
            // modelId
            // 
            this.modelId.Location = new System.Drawing.Point(243, 55);
            this.modelId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.modelId.Name = "modelId";
            this.modelId.Size = new System.Drawing.Size(119, 22);
            this.modelId.TabIndex = 80;
            this.modelId.TextChanged += new System.EventHandler(this.pId_TextChanged);
            // 
            // cPrice
            // 
            this.cPrice.Location = new System.Drawing.Point(243, 390);
            this.cPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cPrice.Name = "cPrice";
            this.cPrice.Size = new System.Drawing.Size(196, 22);
            this.cPrice.TabIndex = 79;
            this.cPrice.TextChanged += new System.EventHandler(this.cPrice_TextChanged);
            this.cPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cPrice_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(59, 391);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 20);
            this.label15.TabIndex = 78;
            this.label15.Text = "Price(RM): ";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // cLocation
            // 
            this.cLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cLocation.FormattingEnabled = true;
            this.cLocation.Items.AddRange(new object[] {
            "Car Dealership Subang Jaya",
            "Car Dealership Glenmarie",
            "Car Dealership KL"});
            this.cLocation.Location = new System.Drawing.Point(243, 166);
            this.cLocation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cLocation.Name = "cLocation";
            this.cLocation.Size = new System.Drawing.Size(231, 28);
            this.cLocation.TabIndex = 77;
            this.cLocation.Text = "-Select Location-";
            this.cLocation.SelectedIndexChanged += new System.EventHandler(this.cLocation_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(53, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 20);
            this.label2.TabIndex = 76;
            this.label2.Text = "Branch Location: ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // cProductionYear
            // 
            this.cProductionYear.Location = new System.Drawing.Point(243, 245);
            this.cProductionYear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cProductionYear.Name = "cProductionYear";
            this.cProductionYear.Size = new System.Drawing.Size(196, 22);
            this.cProductionYear.TabIndex = 73;
            this.cProductionYear.TextChanged += new System.EventHandler(this.cProductionYear_TextChanged);
            this.cProductionYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cProductionYear_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(53, 245);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 20);
            this.label12.TabIndex = 72;
            this.label12.Text = "Production Year:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // cCarPlateNo
            // 
            this.cCarPlateNo.Location = new System.Drawing.Point(243, 208);
            this.cCarPlateNo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cCarPlateNo.Name = "cCarPlateNo";
            this.cCarPlateNo.Size = new System.Drawing.Size(196, 22);
            this.cCarPlateNo.TabIndex = 71;
            this.cCarPlateNo.TextChanged += new System.EventHandler(this.cCarPlateNo_TextChanged);
            // 
            // cMileage
            // 
            this.cMileage.Location = new System.Drawing.Point(244, 277);
            this.cMileage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cMileage.Name = "cMileage";
            this.cMileage.Size = new System.Drawing.Size(196, 22);
            this.cMileage.TabIndex = 67;
            this.cMileage.TextChanged += new System.EventHandler(this.cMileage_TextChanged);
            this.cMileage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cMileage_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(53, 354);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 66;
            this.label9.Text = "Description: ";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(53, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 20);
            this.label8.TabIndex = 65;
            this.label8.Text = "Current Mileage: ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(53, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 20);
            this.label4.TabIndex = 64;
            this.label4.Text = "Car Plate No: ";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 62;
            this.label1.Text = "Model ID: ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(480, 479);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(172, 31);
            this.exitBtn.TabIndex = 61;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(269, 479);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(172, 31);
            this.clearbtn.TabIndex = 60;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // Add_Carbtn
            // 
            this.Add_Carbtn.Location = new System.Drawing.Point(59, 479);
            this.Add_Carbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_Carbtn.Name = "Add_Carbtn";
            this.Add_Carbtn.Size = new System.Drawing.Size(172, 31);
            this.Add_Carbtn.TabIndex = 59;
            this.Add_Carbtn.Text = "Confirm";
            this.Add_Carbtn.UseVisualStyleBackColor = true;
            this.Add_Carbtn.Click += new System.EventHandler(this.Add_Carbtn_Click);
            // 
            // carId
            // 
            this.carId.Enabled = false;
            this.carId.Location = new System.Drawing.Point(243, 22);
            this.carId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.carId.Name = "carId";
            this.carId.Size = new System.Drawing.Size(119, 22);
            this.carId.TabIndex = 92;
            this.carId.TextChanged += new System.EventHandler(this.carId_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(59, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.TabIndex = 91;
            this.label3.Text = "Car ID: ";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // searchProduct
            // 
            this.searchProduct.Location = new System.Drawing.Point(367, 50);
            this.searchProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchProduct.Name = "searchProduct";
            this.searchProduct.Size = new System.Drawing.Size(75, 27);
            this.searchProduct.TabIndex = 93;
            this.searchProduct.Text = "Search";
            this.searchProduct.UseVisualStyleBackColor = true;
            this.searchProduct.Click += new System.EventHandler(this.searchProduct_Click);
            // 
            // addCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 596);
            this.Controls.Add(this.searchProduct);
            this.Controls.Add(this.carId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnOpenPic);
            this.Controls.Add(this.cColour);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cPicture);
            this.Controls.Add(this.quantity);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cDescription);
            this.Controls.Add(this.modelId);
            this.Controls.Add(this.cPrice);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cLocation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cProductionYear);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cCarPlateNo);
            this.Controls.Add(this.cMileage);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.Add_Carbtn);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "addCar";
            this.Text = "addCar";
            ((System.ComponentModel.ISupportInitialize)(this.cPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnOpenPic;
        private System.Windows.Forms.TextBox cColour;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox cPicture;
        private System.Windows.Forms.TextBox quantity;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cDescription;
        private System.Windows.Forms.TextBox modelId;
        private System.Windows.Forms.TextBox cPrice;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cProductionYear;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox cCarPlateNo;
        private System.Windows.Forms.TextBox cMileage;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button Add_Carbtn;
        private System.Windows.Forms.TextBox carId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button searchProduct;
    }
}