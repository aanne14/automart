﻿namespace AutoMartSystem
{
    partial class ManageModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modelName = new System.Windows.Forms.TextBox();
            this.searchProduct = new System.Windows.Forms.Button();
            this.modelId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnOpenPic = new System.Windows.Forms.Button();
            this.engineCC = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.mPicture = new System.Windows.Forms.PictureBox();
            this.quantity = new System.Windows.Forms.TextBox();
            this.mDescription = new System.Windows.Forms.ComboBox();
            this.brandId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Door = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.seatCapacity = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.Add_Carbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // modelName
            // 
            this.modelName.Location = new System.Drawing.Point(275, 125);
            this.modelName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.modelName.Name = "modelName";
            this.modelName.Size = new System.Drawing.Size(196, 22);
            this.modelName.TabIndex = 145;
            // 
            // searchProduct
            // 
            this.searchProduct.Location = new System.Drawing.Point(402, 84);
            this.searchProduct.Name = "searchProduct";
            this.searchProduct.Size = new System.Drawing.Size(75, 27);
            this.searchProduct.TabIndex = 144;
            this.searchProduct.Text = "Search";
            this.searchProduct.UseVisualStyleBackColor = true;
            this.searchProduct.Click += new System.EventHandler(this.searchProduct_Click);
            // 
            // modelId
            // 
            this.modelId.Enabled = false;
            this.modelId.Location = new System.Drawing.Point(278, 56);
            this.modelId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.modelId.Name = "modelId";
            this.modelId.Size = new System.Drawing.Size(118, 22);
            this.modelId.TabIndex = 143;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(93, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 142;
            this.label3.Text = "Model ID: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(89, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 20);
            this.label6.TabIndex = 141;
            this.label6.Text = "Model Name:";
            // 
            // btnOpenPic
            // 
            this.btnOpenPic.Location = new System.Drawing.Point(651, 347);
            this.btnOpenPic.Name = "btnOpenPic";
            this.btnOpenPic.Size = new System.Drawing.Size(137, 30);
            this.btnOpenPic.TabIndex = 140;
            this.btnOpenPic.Text = "Open Picture";
            this.btnOpenPic.UseVisualStyleBackColor = true;
            this.btnOpenPic.Click += new System.EventHandler(this.btnOpenPic_Click);
            // 
            // engineCC
            // 
            this.engineCC.Location = new System.Drawing.Point(275, 163);
            this.engineCC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.engineCC.Name = "engineCC";
            this.engineCC.Size = new System.Drawing.Size(196, 22);
            this.engineCC.TabIndex = 139;
            this.engineCC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.engineCC_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(89, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 20);
            this.label5.TabIndex = 138;
            this.label5.Text = "Engine CC: ";
            // 
            // mPicture
            // 
            this.mPicture.Location = new System.Drawing.Point(515, 89);
            this.mPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mPicture.Name = "mPicture";
            this.mPicture.Size = new System.Drawing.Size(396, 242);
            this.mPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.mPicture.TabIndex = 137;
            this.mPicture.TabStop = false;
            // 
            // quantity
            // 
            this.quantity.Location = new System.Drawing.Point(275, 204);
            this.quantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(86, 22);
            this.quantity.TabIndex = 136;
            this.quantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantity_KeyPress);
            // 
            // mDescription
            // 
            this.mDescription.FormattingEnabled = true;
            this.mDescription.Items.AddRange(new object[] {
            "Sports Car",
            "Recreational Car",
            "Family Car"});
            this.mDescription.Location = new System.Drawing.Point(273, 307);
            this.mDescription.Name = "mDescription";
            this.mDescription.Size = new System.Drawing.Size(197, 24);
            this.mDescription.TabIndex = 135;
            this.mDescription.Text = "-Select-";
            // 
            // brandId
            // 
            this.brandId.Location = new System.Drawing.Point(278, 89);
            this.brandId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.brandId.Name = "brandId";
            this.brandId.Size = new System.Drawing.Size(118, 22);
            this.brandId.TabIndex = 134;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(89, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 133;
            this.label2.Text = "Quantity: ";
            // 
            // Door
            // 
            this.Door.Location = new System.Drawing.Point(274, 277);
            this.Door.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Door.Name = "Door";
            this.Door.Size = new System.Drawing.Size(196, 22);
            this.Door.TabIndex = 132;
            this.Door.TextChanged += new System.EventHandler(this.Door_TextChanged);
            this.Door.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Door_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(89, 279);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 20);
            this.label12.TabIndex = 131;
            this.label12.Text = "No. of Door:";
            // 
            // seatCapacity
            // 
            this.seatCapacity.Location = new System.Drawing.Point(274, 244);
            this.seatCapacity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.seatCapacity.Name = "seatCapacity";
            this.seatCapacity.Size = new System.Drawing.Size(196, 22);
            this.seatCapacity.TabIndex = 130;
            this.seatCapacity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.seatCapacity_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(89, 311);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 129;
            this.label9.Text = "Description: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(89, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 20);
            this.label4.TabIndex = 128;
            this.label4.Text = "Seat Capacity: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(93, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 20);
            this.label1.TabIndex = 127;
            this.label1.Text = "Brand ID: ";
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(528, 433);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(172, 31);
            this.exitBtn.TabIndex = 126;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(318, 433);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(172, 31);
            this.clearbtn.TabIndex = 125;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // Add_Carbtn
            // 
            this.Add_Carbtn.Location = new System.Drawing.Point(106, 433);
            this.Add_Carbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_Carbtn.Name = "Add_Carbtn";
            this.Add_Carbtn.Size = new System.Drawing.Size(172, 31);
            this.Add_Carbtn.TabIndex = 124;
            this.Add_Carbtn.Text = "Confirm";
            this.Add_Carbtn.UseVisualStyleBackColor = true;
            this.Add_Carbtn.Click += new System.EventHandler(this.Add_Carbtn_Click);
            // 
            // ManageModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 520);
            this.Controls.Add(this.modelName);
            this.Controls.Add(this.searchProduct);
            this.Controls.Add(this.modelId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnOpenPic);
            this.Controls.Add(this.engineCC);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.mPicture);
            this.Controls.Add(this.quantity);
            this.Controls.Add(this.mDescription);
            this.Controls.Add(this.brandId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Door);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.seatCapacity);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.Add_Carbtn);
            this.Name = "ManageModel";
            this.Text = "ManageModel";
            ((System.ComponentModel.ISupportInitialize)(this.mPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox modelName;
        private System.Windows.Forms.Button searchProduct;
        private System.Windows.Forms.TextBox modelId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnOpenPic;
        private System.Windows.Forms.TextBox engineCC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox mPicture;
        private System.Windows.Forms.TextBox quantity;
        private System.Windows.Forms.ComboBox mDescription;
        private System.Windows.Forms.TextBox brandId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Door;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox seatCapacity;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button Add_Carbtn;
    }
}