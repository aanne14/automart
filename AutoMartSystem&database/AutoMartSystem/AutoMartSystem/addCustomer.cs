﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoMartSystem
{
    public partial class addCustomer : Form
    {
        //code reuse
        private string EncryptString(string text)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            string encryptedPassword = Convert.ToBase64String(b);
            return encryptedPassword;
            throw new NotImplementedException();
        }

        public addCustomer()
        {
            InitializeComponent();
            addPassword.UseSystemPasswordChar = true;

            try
            {
                //to connect to phpmyadmin database name cardealership
                string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";

                //auto generate transfer id 
                string Query = "SELECT MAX(custID) FROM customerdetails";

                MySqlConnection MyConn = new MySqlConnection(Conn);
                MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
                MySqlDataReader MyReader;
                MyConn.Open();
                MyReader = MyComd.ExecuteReader();

                if (MyReader.Read())
                {

                    this.addId.Text = "CU" + (int.Parse(MyReader.GetString("MAX(custID)").Remove(0, 2)) + 1).ToString();
                }
                else
                {
                    MessageBox.Show("Please try again", "Please try again");
                }

                addIc.Text = "";
                addGender.Text = "-Select Gender-";
                addName.Text = "";
                addPhone.Text = "";
                addAddress.Text = "";
                addAge.Text = "";
                addPassword.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearbtn_Click(object sender, EventArgs e)
        {
          
            addIc.Text = "";
            addGender.Text = "-Select Gender-";
            addName.Text = "";
            addPhone.Text = "";
            addAddress.Text = "";
            addAge.Text = "";
            addPassword.Text = "";
        }

        private void addCustomerBtn_Click(object sender, EventArgs e)
        {

         
            bool check = true,validateic = false;
            

         
            //to connect to phpmyadmin database name cardealership
            string Conn = "datasource=localhost;port=3306;username=root;password=;database=cardealership;sslMode=none;";


            //select from the table of staff details
            MySqlConnection MyConn = new MySqlConnection(Conn);

            string Query = "SELECT * FROM logindetails";

            MyConn = new MySqlConnection(Conn);
            MySqlCommand MyComd = new MySqlCommand(Query, MyConn);
            MySqlDataReader MyReader;
            MyConn.Open();
            MyReader = MyComd.ExecuteReader();

            while (MyReader.Read())
            {
                if (addIc.Text == MyReader.GetString("loginID"))
                {
                    validateic = true;
                    MessageBox.Show("The IC has been registered.", "Please try again.");
                }
            }

            MyConn.Close();

            //validate phone length
            if (addPhone.TextLength > 10)
            {
                MessageBox.Show("Exceeded the numbers in a phone", "Error Message");
                addPhone.Text = "";
                check = false;
            }

           

            if (check == false || validateic == true)
            {
                MessageBox.Show("No record to add", "Please try again");
            }
            else
            {
                try
                {
                    //create login account for customer
                    Query = "INSERT INTO logindetails (loginID,password,roles) VALUES ('" + this.addIc.Text + "','" + EncryptString(this.addPassword.Text) + "','" + "customer" + "');";
                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MessageBox.Show("Record Saved", "Insert Record");
                    MyConn.Close();

                    //insert from the table of customer details
                    Query = "INSERT INTO customerdetails (custID,custIC,custName,custAdd,custPhone,custGender,custAge) " +
                        "VALUES ('" + this.addId.Text + "','" + this.addIc.Text + "','" + this.addName.Text + "','" + this.addAddress.Text + "','" + 
                        "60" + this.addPhone.Text + "','" + this.addGender.Text + "','" + int.Parse(this.addAge.Text) + "');";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);
              
                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();
                    MyConn.Close();

                 

                    Query = "SELECT MAX(custID) FROM customerdetails";

                    MyConn = new MySqlConnection(Conn);
                    MyComd = new MySqlCommand(Query, MyConn);

                    MyConn.Open();
                    MyReader = MyComd.ExecuteReader();

                    if (MyReader.Read())
                    {
                        this.addId.Text = "CU" + (int.Parse(MyReader.GetString("MAX(custID)").Remove(0, 2)) + 1).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Please try again", "Please try again");
                    }
                    MyConn.Close();

                    addIc.Text = "";
                    addGender.Text = "-Select Gender-";
                    addName.Text = "";
                    addPhone.Text = "";
                    addAddress.Text = "";
                    addAge.Text = "";
                    addPassword.Text = "";

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


             
            }
         
        }

        private void passwordBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (passwordBtn.Checked)
            {
                addPassword.UseSystemPasswordChar = false;

            }
            else
            {
                addPassword.UseSystemPasswordChar = true;

            }
        }

        private void addIc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }

        private void addAge_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                MessageBox.Show("Only input integer", "Error Message");
                e.Handled = true;
            }
        }
    }
}
