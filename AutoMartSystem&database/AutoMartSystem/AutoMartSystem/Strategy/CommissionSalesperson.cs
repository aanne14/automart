﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMartSystem
{
    public class CommissionSalesperson
    {
        private CommissionType comm;

        public void setAmount(CommissionType commtype)
        {
            comm = commtype;
        }

        public void Calculate()
        {
            comm.useAmount();
        }
    }
}
