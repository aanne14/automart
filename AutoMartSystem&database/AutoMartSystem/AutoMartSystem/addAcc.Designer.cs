﻿namespace AutoMartSystem
{
    partial class addAcc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accQuantity = new System.Windows.Forms.TextBox();
            this.btnOpenPic = new System.Windows.Forms.Button();
            this.accPicture = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.accId = new System.Windows.Forms.TextBox();
            this.accPrice = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.accName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.clearbtn = new System.Windows.Forms.Button();
            this.Add_productbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.accPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // accQuantity
            // 
            this.accQuantity.Location = new System.Drawing.Point(306, 283);
            this.accQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accQuantity.Name = "accQuantity";
            this.accQuantity.Size = new System.Drawing.Size(126, 22);
            this.accQuantity.TabIndex = 80;
            this.accQuantity.TextChanged += new System.EventHandler(this.accQuantity_TextChanged);
            this.accQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.accQuantity_KeyPress);
            // 
            // btnOpenPic
            // 
            this.btnOpenPic.Location = new System.Drawing.Point(670, 347);
            this.btnOpenPic.Name = "btnOpenPic";
            this.btnOpenPic.Size = new System.Drawing.Size(139, 30);
            this.btnOpenPic.TabIndex = 78;
            this.btnOpenPic.Text = "Open Picture";
            this.btnOpenPic.UseVisualStyleBackColor = true;
            this.btnOpenPic.Click += new System.EventHandler(this.btnOpenPic_Click);
            // 
            // accPicture
            // 
            this.accPicture.Location = new System.Drawing.Point(544, 86);
            this.accPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accPicture.Name = "accPicture";
            this.accPicture.Size = new System.Drawing.Size(396, 242);
            this.accPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.accPicture.TabIndex = 77;
            this.accPicture.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(125, 285);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 20);
            this.label14.TabIndex = 75;
            this.label14.Text = "Quantity: ";
            // 
            // accId
            // 
            this.accId.Enabled = false;
            this.accId.Location = new System.Drawing.Point(307, 86);
            this.accId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accId.Name = "accId";
            this.accId.Size = new System.Drawing.Size(118, 22);
            this.accId.TabIndex = 72;
            // 
            // accPrice
            // 
            this.accPrice.Location = new System.Drawing.Point(307, 214);
            this.accPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accPrice.Name = "accPrice";
            this.accPrice.Size = new System.Drawing.Size(196, 22);
            this.accPrice.TabIndex = 71;
            this.accPrice.TextChanged += new System.EventHandler(this.accPrice_TextChanged);
            this.accPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.accPrice_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(125, 214);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 20);
            this.label11.TabIndex = 70;
            this.label11.Text = "Price(RM):";
            // 
            // accName
            // 
            this.accName.Location = new System.Drawing.Point(307, 151);
            this.accName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accName.Name = "accName";
            this.accName.Size = new System.Drawing.Size(196, 22);
            this.accName.TabIndex = 69;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(125, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 67;
            this.label3.Text = "Name: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(122, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 20);
            this.label1.TabIndex = 66;
            this.label1.Text = "Accessories ID: ";
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(536, 411);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(172, 31);
            this.exitBtn.TabIndex = 65;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // clearbtn
            // 
            this.clearbtn.Location = new System.Drawing.Point(334, 411);
            this.clearbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.clearbtn.Name = "clearbtn";
            this.clearbtn.Size = new System.Drawing.Size(172, 31);
            this.clearbtn.TabIndex = 64;
            this.clearbtn.Text = "Clear";
            this.clearbtn.UseVisualStyleBackColor = true;
            this.clearbtn.Click += new System.EventHandler(this.clearbtn_Click);
            // 
            // Add_productbtn
            // 
            this.Add_productbtn.Location = new System.Drawing.Point(122, 411);
            this.Add_productbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Add_productbtn.Name = "Add_productbtn";
            this.Add_productbtn.Size = new System.Drawing.Size(172, 31);
            this.Add_productbtn.TabIndex = 63;
            this.Add_productbtn.Text = "Confirm";
            this.Add_productbtn.UseVisualStyleBackColor = true;
            this.Add_productbtn.Click += new System.EventHandler(this.Add_productbtn_Click);
            // 
            // addAcc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 599);
            this.Controls.Add(this.accQuantity);
            this.Controls.Add(this.btnOpenPic);
            this.Controls.Add(this.accPicture);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.accId);
            this.Controls.Add(this.accPrice);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.accName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.clearbtn);
            this.Controls.Add(this.Add_productbtn);
            this.Name = "addAcc";
            this.Text = "addAcc";
            ((System.ComponentModel.ISupportInitialize)(this.accPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox accQuantity;
        private System.Windows.Forms.Button btnOpenPic;
        private System.Windows.Forms.PictureBox accPicture;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox accId;
        private System.Windows.Forms.TextBox accPrice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox accName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button clearbtn;
        private System.Windows.Forms.Button Add_productbtn;
    }
}