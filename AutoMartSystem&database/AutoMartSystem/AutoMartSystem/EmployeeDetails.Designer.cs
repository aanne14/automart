﻿namespace AutoMartSystem
{
    partial class EmployeeDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.eName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.eID = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.eStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.eDate = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.eSalary = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.eCommision = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.eTotalCarsSold = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.eAddComment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(574, 29);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 0;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 31);
            this.label1.TabIndex = 1;
            this.label1.Text = "Employee Details";
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(63, 85);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(586, 338);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name: ";
            // 
            // eName
            // 
            this.eName.AutoSize = true;
            this.eName.Location = new System.Drawing.Point(175, 123);
            this.eName.Name = "eName";
            this.eName.Size = new System.Drawing.Size(35, 13);
            this.eName.TabIndex = 4;
            this.eName.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(97, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Staff ID :";
            // 
            // eID
            // 
            this.eID.AutoSize = true;
            this.eID.Location = new System.Drawing.Point(178, 152);
            this.eID.Name = "eID";
            this.eID.Size = new System.Drawing.Size(35, 13);
            this.eID.TabIndex = 6;
            this.eID.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(94, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Status";
            // 
            // eStatus
            // 
            this.eStatus.AutoSize = true;
            this.eStatus.Location = new System.Drawing.Point(178, 184);
            this.eStatus.Name = "eStatus";
            this.eStatus.Size = new System.Drawing.Size(35, 13);
            this.eStatus.TabIndex = 8;
            this.eStatus.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(94, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Date Joined :";
            // 
            // eDate
            // 
            this.eDate.AutoSize = true;
            this.eDate.Location = new System.Drawing.Point(178, 215);
            this.eDate.Name = "eDate";
            this.eDate.Size = new System.Drawing.Size(35, 13);
            this.eDate.TabIndex = 10;
            this.eDate.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(97, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Salary :";
            // 
            // eSalary
            // 
            this.eSalary.AutoSize = true;
            this.eSalary.Location = new System.Drawing.Point(175, 244);
            this.eSalary.Name = "eSalary";
            this.eSalary.Size = new System.Drawing.Size(41, 13);
            this.eSalary.TabIndex = 12;
            this.eSalary.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(94, 272);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Commission";
            // 
            // eCommision
            // 
            this.eCommision.AutoSize = true;
            this.eCommision.Location = new System.Drawing.Point(175, 272);
            this.eCommision.Name = "eCommision";
            this.eCommision.Size = new System.Drawing.Size(41, 13);
            this.eCommision.TabIndex = 14;
            this.eCommision.Text = "label13";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(97, 307);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Total Cars Sold This Month :";
            // 
            // eTotalCarsSold
            // 
            this.eTotalCarsSold.AutoSize = true;
            this.eTotalCarsSold.Location = new System.Drawing.Point(253, 307);
            this.eTotalCarsSold.Name = "eTotalCarsSold";
            this.eTotalCarsSold.Size = new System.Drawing.Size(41, 13);
            this.eTotalCarsSold.TabIndex = 16;
            this.eTotalCarsSold.Text = "label15";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(97, 344);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Additional Comments :";
            // 
            // eAddComment
            // 
            this.eAddComment.AutoSize = true;
            this.eAddComment.Location = new System.Drawing.Point(214, 344);
            this.eAddComment.Name = "eAddComment";
            this.eAddComment.Size = new System.Drawing.Size(41, 13);
            this.eAddComment.TabIndex = 18;
            this.eAddComment.Text = "label17";
            // 
            // EmployeeDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 500);
            this.Controls.Add(this.eAddComment);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.eTotalCarsSold);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.eCommision);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.eSalary);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.eDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.eStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.eID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.eName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Close);
            this.Name = "EmployeeDetails";
            this.Text = "EmployeeDetails";
            this.Load += new System.EventHandler(this.EmployeeDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label eName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label eID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label eStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label eDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label eSalary;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label eCommision;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label eTotalCarsSold;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label eAddComment;
    }
}