-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2019 at 06:36 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardealership`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessoriesdetails`
--

CREATE TABLE `accessoriesdetails` (
  `accId` varchar(5) NOT NULL,
  `accName` varchar(50) NOT NULL,
  `accPrice` int(6) NOT NULL,
  `accQuantity` int(2) NOT NULL,
  `accPicture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accessoriesdetails`
--

INSERT INTO `accessoriesdetails` (`accId`, `accName`, `accPrice`, `accQuantity`, `accPicture`) VALUES
('A1', 'Total Quartz 5W40 9000', 100, 10, 'totalquartz.jpg'),
('A2', 'Auto Oil Honda CVT 3.5L', 50, 10, 'autooil.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bookdetails`
--

CREATE TABLE `bookdetails` (
  `timeId` varchar(5) NOT NULL,
  `custId` varchar(5) NOT NULL,
  `carTableId` varchar(5) NOT NULL,
  `bDate` varchar(100) NOT NULL,
  `bTime` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookdetails`
--

INSERT INTO `bookdetails` (`timeId`, `custId`, `carTableId`, `bDate`, `bTime`, `status`) VALUES
('BT1', 'CU1', 'C1', '2019-11-13', '11:00', 'booked');

-- --------------------------------------------------------

--
-- Table structure for table `branchdetails`
--

CREATE TABLE `branchdetails` (
  `branchId` varchar(10) NOT NULL,
  `branchName` varchar(50) NOT NULL,
  `branchAddress` varchar(100) NOT NULL,
  `numofStaff` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branchdetails`
--

INSERT INTO `branchdetails` (`branchId`, `branchName`, `branchAddress`, `numofStaff`) VALUES
('B1', 'Car Dealership Glenmarie', '3, Jalan Usahawan U1/8, Glenmarie Golf And Country Club, 40150 Shah Alam, Selangor', 10),
('B2', 'Car Dealership Subang jaya', 'Ss 15\r\n47500 Subang Jaya, Selangor\r\n', 13),
('B3', 'Car Dealership KL', 'Kuala Lumpur\r\nFederal Territory of Kuala Lumpur\r\n', 20);

-- --------------------------------------------------------

--
-- Table structure for table `cartable`
--

CREATE TABLE `cartable` (
  `carId` varchar(5) NOT NULL,
  `mId` varchar(20) NOT NULL,
  `cType` varchar(50) NOT NULL,
  `cColour` varchar(50) NOT NULL,
  `cLocation` varchar(50) NOT NULL,
  `cCarPlateNo` varchar(15) NOT NULL,
  `cManufactureYear` int(4) NOT NULL,
  `cMileage` int(7) NOT NULL,
  `quantity` int(3) NOT NULL,
  `cDescription` varchar(50) NOT NULL,
  `cPrice` varchar(7) NOT NULL,
  `cPicture` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartable`
--

INSERT INTO `cartable` (`carId`, `mId`, `cType`, `cColour`, `cLocation`, `cCarPlateNo`, `cManufactureYear`, `cMileage`, `quantity`, `cDescription`, `cPrice`, `cPicture`, `status`) VALUES
('C1', 'M1', 'Used Car', 'white', 'B3', 'LAMBO1', 2016, 50000, 1, 'Sports Car', '1000000', 'pirelliy.jpg', 'available'),
('C2', 'M2', 'Used Car', 'White', 'B3', 'ROLLS123', 2017, 50000, 1, 'Family Car', '1500000', 'rollsphantom.jpg', 'available');

-- --------------------------------------------------------

--
-- Table structure for table `commissiondetails`
--

CREATE TABLE `commissiondetails` (
  `No` int(11) DEFAULT NULL,
  `salespersonIC` bigint(11) NOT NULL,
  `initialSalary` int(11) NOT NULL,
  `commision` int(11) NOT NULL,
  `finalSalary` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commissiondetails`
--

INSERT INTO `commissiondetails` (`No`, `salespersonIC`, `initialSalary`, `commision`, `finalSalary`) VALUES
(1, 980619435144, 3000, 2000, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `customerdetails`
--

CREATE TABLE `customerdetails` (
  `custID` varchar(15) NOT NULL,
  `custIC` varchar(15) NOT NULL,
  `custName` varchar(50) NOT NULL,
  `custAdd` varchar(100) NOT NULL,
  `custPhone` varchar(13) NOT NULL,
  `custGender` varchar(10) NOT NULL,
  `custAge` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customerdetails`
--

INSERT INTO `customerdetails` (`custID`, `custIC`, `custName`, `custAdd`, `custPhone`, `custGender`, `custAge`) VALUES
('CU1', '980619435146', 'Alicia Morris', 'No.3 jasdasdgrewq', '601111331193', 'Male', 25),
('CU2', '980619435148', 'Roronoa', 'No.2 Jalan Uranus U5/129 Seksyen U5 Mahsing 40150 Shah Alam Selangor', '601111331194', 'Male	', 25);

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetails`
--

CREATE TABLE `invoicedetails` (
  `invoiceID` int(10) NOT NULL,
  `productID` varchar(20) NOT NULL,
  `TransferID` varchar(20) NOT NULL,
  `SalespersonID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicedetails`
--

INSERT INTO `invoicedetails` (`invoiceID`, `productID`, `TransferID`, `SalespersonID`) VALUES
(1, 'P1', '', 'E1');

-- --------------------------------------------------------

--
-- Table structure for table `logindetails`
--

CREATE TABLE `logindetails` (
  `loginID` varchar(12) NOT NULL,
  `password` varchar(50) NOT NULL,
  `roles` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logindetails`
--

INSERT INTO `logindetails` (`loginID`, `password`, `roles`) VALUES
('123451234522', 'MTIzNDU=', 'salesperson'),
('980619435141', 'MTIzNDU=', 'salesperson'),
('980619435142', 'MTIzNDU=', 'manager'),
('980619435143', 'MTIzNDU=', 'salesperson'),
('980619435144', 'MTIzNDU=', 'salesperson'),
('980619435145', 'MTIzNDU=', 'admin'),
('980619435146', 'MTIzNDU=', 'customer'),
('980619435148', 'MTIzNDU2', 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `modeldetails`
--

CREATE TABLE `modeldetails` (
  `modelId` varchar(5) NOT NULL,
  `brandId` varchar(5) NOT NULL,
  `mName` varchar(30) NOT NULL,
  `mEngineCC` int(6) NOT NULL,
  `mQuantity` int(2) NOT NULL,
  `mSeats` int(2) NOT NULL,
  `mDoor` int(2) NOT NULL,
  `mDescription` varchar(50) NOT NULL,
  `mPicture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modeldetails`
--

INSERT INTO `modeldetails` (`modelId`, `brandId`, `mName`, `mEngineCC`, `mQuantity`, `mSeats`, `mDoor`, `mDescription`, `mPicture`) VALUES
('M1', 'P1', 'Aventador', 6498, 1, 2, 2, 'Sports Car', 'lambopirelli.jpg'),
('M2', 'P2', 'Phantom', 6498, 1, 5, 4, 'Recreational Car', 'phantom.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `producttable`
--

CREATE TABLE `producttable` (
  `pId` varchar(5) NOT NULL,
  `pBrand` varchar(20) NOT NULL,
  `pPicture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producttable`
--

INSERT INTO `producttable` (`pId`, `pBrand`, `pPicture`) VALUES
('P1', 'Lamborghini', 'lambbo.jpg'),
('P2', 'Rolls Royce', 'rollsroyce.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `reportdetails`
--

CREATE TABLE `reportdetails` (
  `reportID` int(11) NOT NULL,
  `year` date NOT NULL,
  `noSold` int(11) NOT NULL,
  `totalSales` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reportdetails`
--

INSERT INTO `reportdetails` (`reportID`, `year`, `noSold`, `totalSales`) VALUES
(12, '2019-11-05', 15, 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `staffdetails`
--

CREATE TABLE `staffdetails` (
  `staffID` varchar(5) NOT NULL,
  `staffName` varchar(50) NOT NULL,
  `staffIC` varchar(12) NOT NULL,
  `staffPhone` varchar(13) NOT NULL,
  `staffAge` int(2) NOT NULL,
  `staffGender` varchar(10) NOT NULL,
  `staffEmailAdd` varchar(50) NOT NULL,
  `datejoined` varchar(30) NOT NULL,
  `salary` int(6) NOT NULL,
  `staffstatus` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffdetails`
--

INSERT INTO `staffdetails` (`staffID`, `staffName`, `staffIC`, `staffPhone`, `staffAge`, `staffGender`, `staffEmailAdd`, `datejoined`, `salary`, `staffstatus`) VALUES
('E1', 'Tay Jie Ying', '980619435144', '60123435234', 21, 'Female', 'jieying@gmail.com', 'Wednesday, June 13, 2018', 5000, 'Active'),
('E2', 'Yap Chen Ling', '980619435142', '60321456756', 24, 'Female', 'hahaha@gmail.com', 'Wednesday, July 19, 2017', 6000, 'Active'),
('E3', 'Tang Xue Jun', '980619435143', '601435839453', 20, 'Female', 'dafdsa@gmail.com', 'Wednesday, July 16, 2014', 7000, 'Active'),
('E4', 'Tan Min Lee', '980619435141', '60123456789', 23, 'Female', 'minlee@gmail.com', 'Tuesday, May 15, 2018', 5000, 'Active'),
('E5', 'Alicia Morris', '980619435145', '60123412321', 23, 'Female', 'alicia@gmail.com', 'Tuesday, February 13, 2018', 5000, 'Active'),
('E6', 'fdsagas', '123451234522', '60123456789', 21, 'Female', 'tjy190698@hotmail.com', 'Friday, November 1, 2019', 5000, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transferdetails`
--

CREATE TABLE `transferdetails` (
  `transferId` varchar(5) NOT NULL,
  `productid` varchar(5) NOT NULL,
  `transferfrom` varchar(50) NOT NULL,
  `transferto` varchar(50) NOT NULL,
  `tquantity` int(2) NOT NULL,
  `tdescription` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transferdetails`
--

INSERT INTO `transferdetails` (`transferId`, `productid`, `transferfrom`, `transferto`, `tquantity`, `tdescription`) VALUES
('T1', 'C1', 'Car Dealership KL', 'Car Dealership Glenmarie', 1, 'Car exterior colour change to red'),
('T2', 'C2', 'Car Dealership Glenmarie', 'Car Dealership Subang Jaya', 0, 'Car Model');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessoriesdetails`
--
ALTER TABLE `accessoriesdetails`
  ADD PRIMARY KEY (`accId`);

--
-- Indexes for table `bookdetails`
--
ALTER TABLE `bookdetails`
  ADD PRIMARY KEY (`timeId`);

--
-- Indexes for table `branchdetails`
--
ALTER TABLE `branchdetails`
  ADD PRIMARY KEY (`branchId`);

--
-- Indexes for table `cartable`
--
ALTER TABLE `cartable`
  ADD PRIMARY KEY (`carId`);

--
-- Indexes for table `commissiondetails`
--
ALTER TABLE `commissiondetails`
  ADD UNIQUE KEY `No` (`No`);

--
-- Indexes for table `customerdetails`
--
ALTER TABLE `customerdetails`
  ADD PRIMARY KEY (`custID`);

--
-- Indexes for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  ADD PRIMARY KEY (`invoiceID`);

--
-- Indexes for table `logindetails`
--
ALTER TABLE `logindetails`
  ADD PRIMARY KEY (`loginID`);

--
-- Indexes for table `modeldetails`
--
ALTER TABLE `modeldetails`
  ADD PRIMARY KEY (`modelId`);

--
-- Indexes for table `producttable`
--
ALTER TABLE `producttable`
  ADD PRIMARY KEY (`pId`);

--
-- Indexes for table `reportdetails`
--
ALTER TABLE `reportdetails`
  ADD PRIMARY KEY (`reportID`);

--
-- Indexes for table `staffdetails`
--
ALTER TABLE `staffdetails`
  ADD PRIMARY KEY (`staffID`);

--
-- Indexes for table `transferdetails`
--
ALTER TABLE `transferdetails`
  ADD PRIMARY KEY (`transferId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
